/* FragmentConcatenerImageInputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageInputStreamImpl;

class FragmentConcatenerImageInputStream extends ImageInputStreamImpl
{
    private final ArrayList fragments = new ArrayList();
    private final ImageInputStream source;
    private int current_fragment;
    private long current_fragment_end = 0L;
    private final byte[] one_byte_buf = new byte[1];
    
    private final class Fragment
    {
	long this_offset;
	long source_offset;
	long length;
	
	private Fragment() {
	    super();
	}
    }
    
    FragmentConcatenerImageInputStream(ImageInputStream imageinputstream) {
	source = imageinputstream;
    }
    
    private boolean addFragment() throws IOException {
	Fragment fragment = new Fragment();
	do {
	    int i = Tag.create(source.readUnsignedShort(),
			       source.readUnsignedShort());
	    if (i == -73507) {
		current_fragment = -1;
		return true;
	    }
	    if (i != -73728)
		throw new DicomException("ProtocoleViolation",
					 "Item expected");
	    fragment.length = (long) source.readInt() & 0xffffffffL;
	} while (fragment.length == 0L);
	fragment.source_offset = source.getStreamPosition();
	fragment.this_offset = streamPos;
	current_fragment = fragments.size();
	fragments.add(fragment);
	current_fragment_end = streamPos + fragment.length;
	return false;
    }
    
    public int read(byte[] is, int i, int i_0_) throws IOException {
	if (current_fragment == -1)
	    return -1;
	if (streamPos == current_fragment_end) {
	    if (current_fragment < fragments.size() - 1) {
		current_fragment++;
		Fragment fragment = (Fragment) fragments.get(current_fragment);
		source.seek(fragment.source_offset);
		current_fragment_end = streamPos + fragment.length;
	    } else if (addFragment())
		return -1;
	}
	if (streamPos + (long) i_0_ > current_fragment_end)
	    i_0_ = (int) (current_fragment_end - streamPos);
	source.readFully(is, i, i_0_);
	streamPos += (long) i_0_;
	return i_0_;
    }
    
    public int read() throws IOException {
	int i = read(one_byte_buf, 0, 1);
	return i < 0 ? i : one_byte_buf[0] & 0xff;
    }
    
    public void seek(long l) throws IOException {
	if (l < 0L)
	    throw new IndexOutOfBoundsException("negative seek position");
	if (current_fragment_end == 0L && addFragment()) {
	    streamPos = l;
	    current_fragment = -1;
	} else {
	    for (int i = 0; i < fragments.size(); i++) {
		Fragment fragment = (Fragment) fragments.get(i);
		if (l >= fragment.this_offset
		    && l < fragment.this_offset + fragment.length) {
		    source.seek(fragment.source_offset
				+ (l - fragment.this_offset));
		    streamPos = l;
		    current_fragment = i;
		    current_fragment_end
			= fragment.this_offset + fragment.length;
		    return;
		}
	    }
	    for (;;) {
		Fragment fragment
		    = (Fragment) fragments.get(fragments.size() - 1);
		source.seek(fragment.source_offset + fragment.length);
		streamPos = fragment.this_offset + fragment.length;
		if (addFragment()) {
		    current_fragment = -1;
		    break;
		}
		if (l < current_fragment_end) {
		    source.seek(fragment.source_offset
				+ (l - fragment.this_offset));
		    streamPos = l;
		    break;
		}
	    }
	}
    }
}
