/* ExceptionListener - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

public interface ExceptionListener
{
    public void exceptionOccured(Exception exception);
}
