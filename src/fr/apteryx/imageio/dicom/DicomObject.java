/*
 * DicomObject - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

import java.awt.Point;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteOrder;
import java.util.Collection;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

final class DicomObject implements Constants, Serializable {
    String sopInstance;

    String sopClass;

    final DataSet ds;

    final TransferSyntax transferSyntax;

    private static byte[] zero = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0 };

    private static byte[] version = { 0, 1 };

    /* synthetic */static Class array$Ljava$lang$Number;

    static class ApplyLUTTarget {
        BufferedImage bi;

        SampleModel sm;

        ColorModel cm;

        ApplyLUTTarget(BufferedImage bufferedimage) {
            bi = bufferedimage;
        }

        ApplyLUTTarget(SampleModel samplemodel, ColorModel colormodel) {
            cm = colormodel;
            sm = samplemodel;
        }
    }

    DicomObject(DataSet dataset, String string, String string_0_,
            TransferSyntax transfersyntax) {
        ds = dataset;
        sopClass = string;
        sopInstance = string_0_;
        transferSyntax = transfersyntax;
    }

    public String toString() {
        return ("DICOM object (class " + sopClass + ", instance " + sopInstance
                + ")\n" + (ds == null ? "No dataset" : ds.toString()));
    }

    DicomObject(ImageInputStream imageinputstream,
            DicomWarningListener dicomwarninglistener, boolean bool)
            throws IOException {
        imageinputstream.mark();
        if (DicomReaderSpi.readHeader(imageinputstream)) {
            DataSetReader datasetreader = new DataSetReader(imageinputstream,
                    TransferSyntax.EXPLICIT_LITTLEENDIAN, dicomwarninglistener,
                    false);
            DataSet dataset = datasetreader.readGroup();
            DataElement dataelement = dataset.find(131074);
            if (dataelement == null || dataelement.value == null)
                dicomwarninglistener.warning("ProtocoleViolation",
                        "MediaStorageSOPClassUID has no value");
            else
                sopClass = (String) dataelement.value;
            dataelement = dataset.find(131075);
            if (dataelement == null || dataelement.value == null)
                dicomwarninglistener.warning("ProtocoleViolation",
                        "MediaStorageSOPInstanceUID has no value");
            else
                sopInstance = (String) dataelement.value;
            dataelement = dataset.find(131088);
            if (!(dataelement.value instanceof String))
                throw new DicomException("ProtocoleViolation",
                        "TransferSyntaxUID VR is not UI");
            TransferSyntax transfersyntax = TransferSyntax
                    .getInstance((String) dataelement.value);
            transferSyntax = bool ? transfersyntax : null;
            datasetreader = new DataSetReader(imageinputstream, transfersyntax,
                    dicomwarninglistener, bool);
            ds = datasetreader.readDataSet();
        } else {
            imageinputstream.reset();
            dicomwarninglistener
                    .warning("ProtocoleViolation",
                            "Not a part 10 DICOM file, trying implicit little endian transfer syntax");
            DataSetReader datasetreader = new DataSetReader(imageinputstream,
                    dicomwarninglistener, bool);
            ds = datasetreader.readDataSet();
            transferSyntax = bool ? TransferSyntax.IMPLICIT_LITTLEENDIAN : null;
        }
        if (sopClass == null) {
            DataElement dataelement = ds.find(524310);
            if (dataelement == null)
                throw new DicomException("ProtocoleViolation",
                        "SOPClassUID not present");
            if (dataelement.value instanceof String)
                sopClass = (String) dataelement.value;
            else if (dataelement.value instanceof byte[])
                sopClass = new String((byte[]) dataelement.value);
            else
                throw new DicomException("ProtocoleViolation",
                        "Invalid VR for SOPClassUID");
        }
        if (sopInstance == null) {
            DataElement dataelement = ds.find(524312);
            if (dataelement.value instanceof String)
                sopInstance = (String) dataelement.value;
            else if (dataelement.value instanceof byte[])
                sopInstance = new String((byte[]) dataelement.value);
            else {
                dicomwarninglistener.warning("ProtocoleViolation",
                        "SOPInstanceUID not present or wrong VR");
                sopInstance = "";
            }
        }
    }

    void write(ImageOutputStream imageoutputstream,
            TransferSyntax transfersyntax, DicomWriter dicomwriter)
            throws IOException {
        imageoutputstream.setByteOrder(ByteOrder.LITTLE_ENDIAN);
        for (int i = 128 / zero.length; i > 0; i--)
            imageoutputstream.write(zero);
        imageoutputstream.write(68);
        imageoutputstream.write(73);
        imageoutputstream.write(67);
        imageoutputstream.write(77);
        if (transferSyntax != null)
            transfersyntax = transferSyntax;
        DataSet dataset = new DataSet();
        dataset
                .add(
                        131072,
                        new Long(
                                (long) (12
                                        + (version.length + 1 & ~0x1)
                                        + 8
                                        + (sopClass.length() + 1 & ~0x1)
                                        + 8
                                        + (sopInstance.length() + 1 & ~0x1)
                                        + 8
                                        + (transfersyntax.uid.length() + 1 & ~0x1)
                                        + 8 + ("1.2.826.0.1.3680043.2.228.0.2"
                                        .length() + 1 & ~0x1))));
        dataset.add(131073, version);
        dataset.add(131074, sopClass);
        dataset.add(131075, sopInstance);
        dataset.add(131088, transfersyntax.uid);
        dataset.add(131090, "1.2.826.0.1.3680043.2.228.0.2");
        dataset.write(imageoutputstream, TransferSyntax.EXPLICIT_LITTLEENDIAN,
                true, dicomwriter);
        ds.write(imageoutputstream, transfersyntax, true, dicomwriter);
    }

    void reset() {
        ds.clear();
        sopClass = sopInstance = null;
    }

    synchronized void normalize(DicomWarningListener dicomwarninglistener)
            throws DicomException {
        new Normalizer(dicomwarninglistener).normalize(this);
    }

    void updateFromICM(IndexColorModel indexcolormodel) {
        int i = indexcolormodel.getMapSize();
        int[] is = { i & 0xffff, 0, 8 };
        short[] is_1_ = new short[i];
        short[] is_2_ = new short[i];
        short[] is_3_ = new short[i];
        for (int i_4_ = i - 1; i_4_ >= 0; i_4_--) {
            is_1_[i_4_] = (short) indexcolormodel.getRed(i_4_);
            is_2_[i_4_] = (short) indexcolormodel.getGreen(i_4_);
            is_3_[i_4_] = (short) indexcolormodel.getBlue(i_4_);
        }
        ds.add(2625793, is);
        ds.add(2625794, is);
        ds.add(2625795, is);
        ds.add(2626049, is_1_);
        ds.add(2626050, is_2_);
        ds.add(2626051, is_3_);
    }

    static DataSet findMacro(int i, DataSet dataset, DataSet dataset_5_) {
        DataSet dataset_6_ = dataset_5_ == null ? null : dataset_5_
                .findDataSet(i);
        if (dataset_6_ != null)
            return dataset_6_;
        if (dataset != null) {
            dataset_6_ = dataset.findDataSet(i);
            if (dataset_6_ != null)
                return dataset_6_;
        }
        return null;
    }

    TransferSyntax selectSuitableTS(TransferSyntax[] transfersyntaxes) {
        for (int i = 0; i < transfersyntaxes.length; i++) {
            if (transfersyntaxes[i] == TransferSyntax.JPEG_BASELINE_1) {
                if (ds.findInt(2621697) > 8)
                    continue;
            } else if (transfersyntaxes[i] == TransferSyntax.JPEG_EXTENDED_2_4
                    && ds.findInt(2621697) > 12)
                continue;
            if (transfersyntaxes[i].encapsulated) {
                Object object = ds.findValue(2145386512);
                if (object != null) {
                    ImageTypeSpecifier imagetypespecifier = new ImageTypeSpecifier(
                            Util.getColorModel(object), Util
                                    .getSampleModel(object));
                    if (imagetypespecifier.getSampleModel().getDataType() == 2
                            && ((transfersyntaxes[i] == TransferSyntax.JPEG_BASELINE_1)
                                    || (transfersyntaxes[i] == TransferSyntax.JPEG_EXTENDED_2_4)
                                    || (transfersyntaxes[i] == TransferSyntax.JPEG_LOSSLESS_14SV1) || (transfersyntaxes[i] == TransferSyntax.JPEG_LS_LOSSLESS)))
                        imagetypespecifier = (new ImageTypeSpecifier(
                                (ShortToUShortRenderedImage
                                        .getCompatibleColorModel(imagetypespecifier
                                                .getColorModel())), Util
                                        .convertSampleModel(imagetypespecifier
                                                .getSampleModel(), 1)));
                    if (!transfersyntaxes[i].getWriterSpi().canEncodeImage(
                            imagetypespecifier))
                        continue;
                }
            }
            return transfersyntaxes[i];
        }
        return null;
    }

    void updateFromTransferSyntax(TransferSyntax transfersyntax)
            throws DicomException {
        if (transfersyntax == TransferSyntax.JPEG_BASELINE_1
                || transfersyntax == TransferSyntax.JPEG_EXTENDED_2_4
                || transfersyntax == TransferSyntax.JPEG_LOSSLESS_14SV1
                || transfersyntax == TransferSyntax.JPEG_LS_LOSSLESS) {
            if (ds.findInt(2621442) == 3)
                ds.add(2621444, "YBR_FULL_422");
            Object object = ds.findValue(2145386512);
            if (object instanceof Collection) {
                Iterator iterator = ((Collection) object).iterator();
                while (iterator.hasNext())
                    forceUnsigned((IIOImage) iterator.next());
            } else if (object instanceof IIOImage)
                forceUnsigned((IIOImage) object);
        } else if (transfersyntax == TransferSyntax.JPEG2000
                || transfersyntax == TransferSyntax.JPEG2000_LOSSLESS) {
            if (ds.findInt(2621442) == 3)
                ds.add(2621444,
                        (transfersyntax == TransferSyntax.JPEG2000 ? "YBR_ICT"
                                : "YBR_RCT"));
            ds.add(2621446, 0);
        }
    }

    private static void forceUnsigned(IIOImage iioimage) throws DicomException {
        if (iioimage.hasRaster()) {
            Raster raster = iioimage.getRaster();
            SampleModel samplemodel = raster.getSampleModel();
            if (samplemodel.getDataType() == 2) {
                raster = Util.convertRaster(raster, 1);
                if (raster == null)
                    throw new DicomException("ApplicationError",
                            "Cannot convert non standard sample models");
                iioimage.setRaster(raster);
            }
        } else {
            RenderedImage renderedimage = iioimage.getRenderedImage();
            if (renderedimage.getSampleModel().getDataType() == 2)
                iioimage.setRenderedImage(new ShortToUShortRenderedImage(
                        renderedimage));
        }
    }

    boolean applyLUT(ApplyLUTTarget applyluttarget, int i,
            DicomWarningListener dicomwarninglistener) {
        boolean bool = false;
        boolean bool_7_ = false;
        boolean bool_8_ = false;
        WritableRaster writableraster = applyluttarget.bi == null ? null
                : applyluttarget.bi.getRaster();
        ColorModel colormodel = (applyluttarget.cm == null ? applyluttarget.bi
                .getColorModel() : applyluttarget.cm);
        ColorSpace colorspace = colormodel.getColorSpace();
        SampleModel samplemodel = (applyluttarget.sm == null ? writableraster
                .getSampleModel() : applyluttarget.sm);
        if (samplemodel.getNumBands() > 1
                || colormodel instanceof IndexColorModel)
            return true;
        DataSet dataset = null;
        Object object = ds.findValue(1375769136);
        if (object instanceof Collection && ((Collection) object).size() > i) {
            Iterator iterator = ((Collection) object).iterator();
            for (int i_9_ = 0; i_9_ < i; i_9_++)
                iterator.next();
            dataset = (DataSet) iterator.next();
        }
        DataSet dataset_10_ = ds.findDataSet(1375769129);
        DataSet dataset_11_ = findMacro(2658629, dataset_10_, dataset);
        DataSet dataset_12_ = findMacro(2658610, dataset_10_, dataset);
        if (dataset_11_ == null)
            dataset_11_ = ds;
        if (dataset_12_ == null)
            dataset_12_ = ds;
        float f = dataset_11_.findFloat(2625618);
        float f_13_ = dataset_11_.findFloat(2625619);
        if (f_13_ == 0.0F)
            f_13_ = 1.0F;
        if (f_13_ != 1.0F || f != 0.0F) {
            int i_14_;
            int i_15_;
            switch (samplemodel.getTransferType()) {
            case 0:
            case 1:
                i_14_ = 0;
                i_15_ = (1 << samplemodel.getSampleSize(0) - 1) - 1;
                break;
            default:
                i_14_ = -(1 << samplemodel.getSampleSize(0) - 2);
                i_15_ = (1 << samplemodel.getSampleSize(0) - 2) - 1;
            }
            int i_16_;
            int i_17_;
            if (f_13_ > 0.0F) {
                i_16_ = (int) ((float) i_14_ * f_13_ + f);
                i_17_ = (int) ((float) i_15_ * f_13_ + f);
            } else {
                i_16_ = (int) ((float) i_15_ * f_13_ + f);
                i_17_ = (int) ((float) i_14_ * f_13_ + f);
            }
            int i_18_;
            if (i_16_ >= 0)
                i_18_ = i_17_ <= 255 ? 0 : i_17_ <= 65535 ? 1 : 3;
            else
                i_18_ = i_16_ >= -32768 && i_17_ <= 32767 ? 2 : 3;
            samplemodel = new PixelInterleavedSampleModel(i_18_, samplemodel
                    .getWidth(), samplemodel.getHeight(), 1, samplemodel
                    .getWidth(), new int[] { 0 });
            if (writableraster != null) {
                WritableRaster writableraster_19_ = (Raster
                        .createWritableRaster(samplemodel, samplemodel
                                .createDataBuffer(), new Point(writableraster
                                .getMinX(), writableraster.getMinY())));
                Operations
                        .rescale(writableraster, writableraster_19_, f_13_, f);
                writableraster = writableraster_19_;
                bool = true;
            }
        } else {
            DataSet dataset_20_ = dataset_11_.findDataSet(2633728);
            if (dataset_20_ != null) {
                Number[] numbers = ((Number[]) (dataset_20_.findValue(2633730,
                        java.lang.Number.class)));
                if (numbers == null || numbers.length != 3) {
                    if (dicomwarninglistener != null)
                        dicomwarninglistener.warning("ProtocoleViolation",
                                "LUT Descriptor has invalid value");
                } else {
                    int i_21_ = ds.findInt(2621699);
                    int i_22_ = (i_21_ == 0 ? (int) (numbers[1].shortValue() & 0xffff)
                            : numbers[1].shortValue());
                    Object object_23_ = dataset_20_.findValue(2633734);
                    if (writableraster == null)
                        samplemodel = (new PixelInterleavedSampleModel(
                                object_23_ instanceof byte[] ? 0 : 1,
                                samplemodel.getWidth(),
                                samplemodel.getHeight(), 1, samplemodel
                                        .getWidth(), new int[] { 0 }));
                    else {
                        writableraster = Operations.lookup(writableraster,
                                i_22_, object_23_);
                        samplemodel = writableraster.getSampleModel();
                        bool = true;
                    }
                }
            }
        }
        String string = ds.findString(542113824);
        if (string != null) {
            colorspace = DicomColorSpace
                    .getInstanceFromPresentationLUTShape(string);
            bool_8_ = true;
        } else {
            DataSet dataset_24_ = ds.findDataSet(542113808);
            if (dataset_24_ != null) {
                ColorSpace colorspace_25_ = DicomColorSpace
                        .getInstanceFromPresentationLUTSequence(dataset_24_);
                if (colorspace_25_ != null) {
                    colorspace = colorspace_25_;
                    bool_8_ = true;
                }
            }
        }
        float f_26_ = 0.0F;
        float f_27_ = 0.0F;
        object = dataset_12_.findValue(2625616);
        if (object instanceof Number[]) {
            Number[] numbers = (Number[]) object;
            if (numbers.length > 0)
                f_26_ = numbers[0].floatValue();
        } else if (object instanceof Number)
            f_26_ = ((Number) object).floatValue();
        object = dataset_12_.findValue(2625617);
        if (object instanceof Number[]) {
            Number[] numbers = (Number[]) object;
            if (numbers.length > 0)
                f_27_ = numbers[0].floatValue();
        } else if (object instanceof Number)
            f_27_ = ((Number) object).floatValue();
        if (f_27_ >= 1.0F) {
            colormodel = new VOIWindowColorModel(samplemodel.getTransferType(),
                    samplemodel.getSampleSize(0), f_26_, f_27_, colorspace);
            bool_7_ = true;
        } else if (ds.has(2621703)) {
            float f_28_ = ds.findFloat(2621703);
            float f_29_ = ds.findFloat(2621702);
            colormodel = new VOIWindowColorModel(samplemodel.getTransferType(),
                    samplemodel.getSampleSize(0), (f_28_ + f_29_) / 2.0F, f_28_
                            - f_29_, colorspace);
            bool_7_ = true;
        } else {
            Object object_30_ = dataset_12_.findValue(2633744);
            DataSet dataset_31_ = null;
            if (object_30_ instanceof Collection)
                dataset_31_ = (DataSet) ((Collection) object_30_).iterator()
                        .next();
            else if (object_30_ instanceof DataSet)
                dataset_31_ = (DataSet) object_30_;
            Number[] numbers = (dataset_31_ == null ? null
                    : (Number[]) (dataset_31_.findValue(2633730,
                            java.lang.Number.class)));
            if (numbers != null && numbers.length > 1) {
                int i_32_ = dataset_31_.findInt(2621699);
                int i_33_ = (i_32_ == 0 ? (int) (numbers[1].shortValue() & 0xffff)
                        : numbers[1].shortValue());
                Object object_34_ = dataset_31_.findValue(2633734);
                colormodel = new VOILUTColorModel(
                        samplemodel.getTransferType(), numbers[2].intValue(),
                        i_33_, object_34_, colorspace);
                bool_7_ = true;
            } else {
                if (writableraster == null)
                    return false;
                int[] is = Operations.minmax(writableraster);
                int i_35_ = is[0];
                int i_36_ = is[1];
                int i_37_ = samplemodel.getDataType();
                switch (i_37_) {
                case 0:
                    if (i_35_ == 0 && i_36_ == 255) {
                        if (bool || bool_8_) {
                            colormodel = new ComponentColorModel(colorspace,
                                    new int[] { 8 }, false, false, 1, i_37_);
                            bool_7_ = true;
                        }
                    } else {
                        colormodel = new VOIWindowColorModel(0, 8,
                                (float) ((i_35_ + i_36_) / 2),
                                (float) (i_36_ - i_35_), colorspace);
                        bool_7_ = true;
                    }
                    break;
                case 2:
                    colormodel = new VOIWindowColorModel(2, 16,
                            (float) ((i_35_ + i_36_) / 2),
                            (float) (i_36_ - i_35_), colorspace);
                    bool_7_ = true;
                    break;
                case 1:
                    colormodel = new VOIWindowColorModel(1, 16,
                            (float) ((i_35_ + i_36_) / 2),
                            (float) (i_36_ - i_35_), colorspace);
                    bool_7_ = true;
                    break;
                case 3:
                    colormodel = new VOIWindowColorModel(3, 32,
                            (float) ((i_35_ + i_36_) / 2),
                            (float) (i_36_ - i_35_), colorspace);
                    bool_7_ = true;
                    break;
                }
            }
        }
        applyluttarget.cm = colormodel;
        applyluttarget.sm = samplemodel;
        if (writableraster != null && (bool_7_ || bool))
            applyluttarget.bi = new BufferedImage(colormodel, writableraster,
                    false, null);
        return true;
    }

}
