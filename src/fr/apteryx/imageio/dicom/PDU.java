/* PDU - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

abstract class PDU
{
    static final byte ASSOCIATE_RQ = 1;
    static final byte ASSOCIATE_AC = 2;
    static final byte ASSOCIATE_RJ = 3;
    static final byte DATA_TF = 4;
    static final byte RELEASE_RQ = 5;
    static final byte RELEASE_RP = 6;
    static final byte ABORT = 7;
    final byte type;
    
    static class DataTf extends PDU
    {
	final byte[] buf;
	final int len;
	
	DataTf(byte[] is, int i) {
	    super((byte) 4);
	    buf = is;
	    len = i;
	}
	
	void send(OutputStream outputstream) {
	    throw new Error("NOT USED");
	}
    }
    
    static class AssociateRjReleaseRqReleaseRpAbort extends PDU
    {
	final byte result;
	final byte source;
	final byte reason;
	private static byte[] head = { 0, 0, 0, 0, 4, 0 };
	
	AssociateRjReleaseRqReleaseRpAbort(byte i, int i_0_, int i_1_,
					   int i_2_) {
	    super(i);
	    result = (byte) i_0_;
	    source = (byte) i_1_;
	    reason = (byte) i_2_;
	}
	
	AssociateRjReleaseRqReleaseRpAbort(byte i, byte[] is, int i_3_)
	    throws DicomException {
	    super(i);
	    if (i_3_ != 4)
		throw new DicomException("ProtocoleViolation",
					 "PDU length is incorrect");
	    result = is[1];
	    source = is[2];
	    reason = is[3];
	}
	
	void send(OutputStream outputstream) throws IOException {
	    outputstream.write(this.type);
	    outputstream.write(head);
	    outputstream.write(result);
	    outputstream.write(source);
	    outputstream.write(reason);
	}
    }
    
    static class AssociateRqAc extends PDU
    {
	final AbstractSyntax[] syntaxes;
	final long maxLength;
	final String calledAE;
	final String callingAE;
	
	AssociateRqAc(byte i, String string, String string_4_,
		      AbstractSyntax[] abstractsyntaxes, long l) {
	    super(i);
	    calledAE = string;
	    callingAE = string_4_;
	    syntaxes = abstractsyntaxes;
	    maxLength = l;
	}
	
	AssociateRqAc(byte i, byte[] is, int i_5_) throws DicomException {
	    super(i);
	    int i_6_ = readUnsignedShort(is, 0);
	    if ((i_6_ & 0x1) != 1)
		throw new DicomException("Unsupported",
					 "Dicom protocol version");
	    calledAE = new String(is, 4, 16).trim();
	    callingAE = new String(is, 20, 16).trim();
	    int i_7_ = 68;
	    long l = 0L;
	    ArrayList arraylist = new ArrayList();
	    HashMap hashmap = new HashMap();
	    while (i_7_ < i_5_) {
		int i_8_ = is[i_7_] & 0xff;
		switch (i_8_) {
		case 16: {
		    int i_9_ = readUnsignedShort(is, i_7_ + 2);
		    String string = new String(is, i_7_ + 4, i_9_);
		    if (!string.equals("1.2.840.10008.3.1.1.1"))
			throw new DicomException
				  ("ProtocoleViolation",
				   "Application context item value in A-ASSOCIATE-RQ/AC PDU is not Dicom Application Context");
		    i_7_ += 4 + i_9_;
		    break;
		}
		case 32:
		case 33: {
		    int i_10_ = readUnsignedShort(is, i_7_ + 2);
		    int i_11_ = is[i_7_ + 4] & 0xff;
		    if (i_11_ < 1 || (i_11_ & 0x1) != 1)
			throw new DicomException
				  ("ProtocoleViolation",
				   ("Presentation context ID in A-ASSOCIATE-RQ/AC PDU is not odd and positive ("
				    + i_11_ + ")"));
		    int i_12_ = is[i_7_ + 6] & 0xff;
		    i_10_ -= 4;
		    i_7_ += 8;
		    String string = null;
		    ArrayList arraylist_13_ = new ArrayList();
		    while (i_10_ > 0) {
			int i_14_ = is[i_7_] & 0xff;
			int i_15_ = readUnsignedShort(is, i_7_ + 2);
			String string_16_ = new String(is, i_7_ + 4, i_15_);
			i_10_ -= 4 + i_15_;
			i_7_ += 4 + i_15_;
			switch (i_14_) {
			case 48:
			    string = string_16_;
			    break;
			case 64:
			    if (i_8_ == 32 || i_12_ == 0)
				arraylist_13_.add(string_16_);
			    break;
			}
		    }
		    AbstractSyntax abstractsyntax
			= new AbstractSyntax(i_11_, string, arraylist_13_);
		    abstractsyntax.reason
			= (i_12_ < 0 || i_12_ >= AbstractSyntax.REASONS.length
			   ? 2 : i_12_);
		    arraylist.add(abstractsyntax);
		    break;
		}
		case 80: {
		    int i_17_ = readUnsignedShort(is, i_7_ + 2);
		    i_7_ += 4;
		    while (i_17_ > 0) {
			int i_18_ = is[i_7_] & 0xff;
			int i_19_ = readUnsignedShort(is, i_7_ + 2);
			i_17_ -= 4 + i_19_;
			if (i_18_ == 81 && i_19_ == 4)
			    l = readUnsignedInt(is, i_7_ + 4);
			else if (i_18_ == 84) {
			    int i_20_ = readUnsignedShort(is, i_7_ + 4);
			    String string = new String(is, i_7_ + 6, i_20_);
			    int i_21_ = is[i_7_ + 6 + i_20_];
			    int i_22_ = is[i_7_ + 7 + i_20_];
			    int i_23_;
			    if (i_21_ == 1 && i_22_ == 1)
				i_23_ = 3;
			    else if (i_21_ == 1)
				i_23_ = 1;
			    else if (i_22_ == 1)
				i_23_ = 2;
			    else
				i_23_ = 0;
			    hashmap.put(string, new Integer(i_23_));
			}
			i_7_ += 4 + i_19_;
		    }
		    break;
		}
		default:
		    throw new DicomException("ProtocoleViolation",
					     ("Unknown item " + i_8_
					      + " in A-ASSOCIATE-RQ/AC PDU"));
		}
	    }
	    syntaxes = new AbstractSyntax[arraylist.size()];
	    arraylist.toArray(syntaxes);
	    if (hashmap.size() > 0) {
		for (int i_24_ = 0; i_24_ < syntaxes.length; i_24_++) {
		    Integer integer
			= (Integer) hashmap.get(syntaxes[i_24_].name);
		    if (integer != null)
			syntaxes[i_24_].role = integer.intValue();
		}
	    }
	    maxLength = l;
	}
	
	void send(OutputStream outputstream) throws IOException {
	    PDU.Item item = new PDU.Item(this.type, true);
	    item.addShort(1);
	    item.addShort(0);
	    item.addAE(calledAE);
	    item.addAE(callingAE);
	    for (int i = 0; i < 32; i++)
		item.addByte(0);
	    PDU.Item item_25_ = new PDU.Item(16);
	    item_25_.add("1.2.840.10008.3.1.1.1");
	    item.add(item_25_);
	    for (int i = 0; i < syntaxes.length; i++) {
		PDU.Item item_26_ = new PDU.Item(this.type == 1 ? 32 : 33);
		item_26_.addByte(syntaxes[i].id);
		item_26_.addByte(0);
		item_26_.addByte((this.type == 1
				  || syntaxes[i].selected_ts != null)
				 ? 0 : syntaxes[i].reason);
		item_26_.addByte(0);
		if (this.type == 1) {
		    PDU.Item item_27_ = new PDU.Item(48);
		    item_27_.add(syntaxes[i].name);
		    item_26_.add(item_27_);
		    for (int i_28_ = 0; i_28_ < syntaxes[i].ts.size();
			 i_28_++) {
			PDU.Item item_29_ = new PDU.Item(64);
			item_29_.add((String) syntaxes[i].ts.get(i_28_));
			item_26_.add(item_29_);
		    }
		} else {
		    PDU.Item item_30_ = new PDU.Item(64);
		    TransferSyntax transfersyntax = syntaxes[i].selected_ts;
		    item_30_
			.add(transfersyntax == null ? "" : transfersyntax.uid);
		    item_26_.add(item_30_);
		}
		item.add(item_26_);
	    }
	    PDU.Item item_31_ = new PDU.Item(80);
	    PDU.Item item_32_ = new PDU.Item(81);
	    item_32_.addInt((int) maxLength);
	    item_31_.add(item_32_);
	    item_32_ = new PDU.Item(82);
	    item_32_.add("1.2.826.0.1.3680043.2.228.0.2");
	    item_31_.add(item_32_);
	    for (int i = 0; i < syntaxes.length; i++) {
		if (syntaxes[i].role != 0
		    && (this.type == 1 || syntaxes[i].selected_ts != null)) {
		    item_32_ = new PDU.Item(84);
		    item_32_.addShort(syntaxes[i].name.length());
		    item_32_.add(syntaxes[i].name);
		    item_32_.addByte(syntaxes[i].role == 2 ? 0 : 1);
		    item_32_.addByte(syntaxes[i].role == 1 ? 0 : 1);
		    item_31_.add(item_32_);
		}
	    }
	    item_32_ = new PDU.Item(85);
	    item_32_.add("1.08pre");
	    item_31_.add(item_32_);
	    item.add(item_31_);
	    item.writeTo(outputstream);
	}
    }
    
    static final class Item extends ByteArrayOutputStream
    {
	private boolean large;
	
	Item(int i) {
	    this(i, false);
	}
	
	void addByte(int i) {
	    write(i);
	}
	
	void addShort(int i) {
	    write(i >> 8 & 0xff);
	    write(i & 0xff);
	}
	
	void addInt(int i) {
	    write(i >> 24 & 0xff);
	    write(i >> 16 & 0xff);
	    write(i >> 8 & 0xff);
	    write(i & 0xff);
	}
	
	void add(String string) {
	    for (int i = 0; i < string.length(); i++)
		write(string.charAt(i));
	}
	
	void addAE(String string) {
	    if (string.length() <= 16) {
		for (int i = 0; i < string.length(); i++)
		    write(string.charAt(i));
		for (int i = string.length(); i < 16; i++)
		    write(32);
	    } else {
		for (int i = 0; i < 16; i++)
		    write(string.charAt(i));
	    }
	}
	
	void add(Item item_33_) {
	    try {
		item_33_.writeTo(this);
	    } catch (IOException ioexception) {
		/* empty */
	    }
	}
	
	Item(int i, boolean bool) {
	    super(32);
	    large = bool;
	    write(i);
	    write(0);
	    if (bool)
		addInt(0);
	    else
		addShort(0);
	}
	
	private void seal() {
	    if (large) {
		int i = count - 6;
		buf[2] = (byte) (i >> 24);
		buf[3] = (byte) (i >> 16);
		buf[4] = (byte) (i >> 8);
		buf[5] = (byte) i;
	    } else {
		buf[2] = (byte) (count - 4 >> 8);
		buf[3] = (byte) (count - 4);
	    }
	}
	
	public void writeTo(OutputStream outputstream) throws IOException {
	    seal();
	    super.writeTo(outputstream);
	}
    }
    
    static final int readUnsignedShort(byte[] is, int i) {
	return ((is[i] & 0xff) << 8) + (is[i + 1] & 0xff);
    }
    
    static final long readUnsignedInt(byte[] is, int i) {
	return ((((long) is[i] & 0xffL) << 24)
		+ (((long) is[i + 1] & 0xffL) << 16)
		+ (((long) is[i + 2] & 0xffL) << 8)
		+ ((long) is[i + 3] & 0xffL));
    }
    
    static final PDU create(byte i, byte[] is, int i_34_)
	throws DicomException {
	switch (i) {
	case 1:
	case 2:
	    return new AssociateRqAc(i, is, i_34_);
	case 3:
	case 5:
	case 6:
	case 7:
	    return new AssociateRjReleaseRqReleaseRpAbort(i, is, i_34_);
	case 4:
	    return new DataTf(is, i_34_);
	default:
	    return null;
	}
    }
    
    PDU(byte i) {
	type = i;
    }
    
    abstract void send(OutputStream outputstream) throws IOException;
}
