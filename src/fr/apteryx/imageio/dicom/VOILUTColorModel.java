/* VOILUTColorModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.color.ColorSpace;
import java.lang.reflect.Array;

public final class VOILUTColorModel extends VOIColorModel
{
    private final int first;
    private final int last;
    private float[] flut;
    private byte[] blut;
    private final Object lut;
    
    public VOILUTColorModel(int i, int i_0_, int i_1_, Object object,
			    ColorSpace colorspace) {
	super(i, i_0_, 8, colorspace);
	lut = object;
	first = i_1_;
	last = Array.getLength(object) - 1;
    }
    
    public int getFirst() {
	return first;
    }
    
    public Object getLUT() {
	return lut;
    }
    
    private synchronized void make_flut() {
	if (flut == null) {
	    float f = getColorSpace().getMinValue(0);
	    float f_2_ = getColorSpace().getMaxValue(0);
	    if (lut instanceof byte[]) {
		double d = (double) ((f_2_ - f) / 255.0F);
		blut = (byte[]) lut;
		flut = new float[blut.length];
		for (int i = 0; i < blut.length; i++) {
		    flut[i]
			= (float) ((double) f + (double) (blut[i] & 0xff) * d);
		    if (flut[i] > f_2_)
			flut[i] = f_2_;
		}
	    } else {
		double d = (double) ((f_2_ - f)
				     / (float) ((1 << getPixelSize()) - 1));
		short[] is = Util.toShortArray(lut);
		flut = new float[is.length];
		for (int i = 0; i < is.length; i++) {
		    flut[i]
			= (float) ((double) f + (double) (is[i] & 0xffff) * d);
		    if (flut[i] > f_2_)
			flut[i] = f_2_;
		}
	    }
	}
    }
    
    private synchronized void make_blut() {
	if (blut == null) {
	    if (lut instanceof byte[])
		blut = (byte[]) lut;
	    else {
		short[] is = Util.toShortArray(lut);
		int i = getPixelSize() - 8;
		blut = new byte[is.length];
		for (int i_3_ = 0; i_3_ < is.length; i_3_++)
		    blut[i_3_] = (byte) (is[i_3_] >> i);
	    }
	}
    }
    
    int getComp(int i) {
	if (blut == null)
	    make_blut();
	i -= first;
	if (i < 0)
	    i = 0;
	if (i > last)
	    i = last;
	return blut[i] & 0xff;
    }
    
    int get255(int i) {
	return getComp(i);
    }
    
    float getNorm(int i) {
	if (flut == null)
	    make_flut();
	i -= first;
	if (i < 0)
	    i = 0;
	if (i > last)
	    i = last;
	return flut[i];
    }
    
    int getDataElement(float f) {
	return last;
    }
}
