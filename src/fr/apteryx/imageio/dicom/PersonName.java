/* PersonName - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

public class PersonName
{
    final String[][] groups;
    private int nb_groups;
    
    public PersonName() {
	groups = new String[3][];
	nb_groups = 0;
    }
    
    public PersonName(String string) {
	groups = new String[3][];
	nb_groups = 0;
	groups[0] = new String[] { string, null, null, null, null };
	nb_groups = 1;
    }
    
    public boolean equals(Object object) {
	if (!(object instanceof PersonName))
	    return false;
	PersonName personname_0_ = (PersonName) object;
	if (nb_groups != personname_0_.nb_groups)
	    return false;
	for (int i = 0; i < nb_groups; i++) {
	    for (int i_1_ = 0; i_1_ < 5; i_1_++) {
		String string = groups[i][i_1_];
		if (string == null) {
		    if (personname_0_.groups[i][i_1_] != null)
			return false;
		} else if (!string.equalsIgnoreCase(personname_0_.groups[i]
						    [i_1_]))
		    return false;
	    }
	}
	return true;
    }
    
    public synchronized String toString() {
	if (nb_groups == 0)
	    return "";
	String[] strings = groups[0];
	StringBuffer stringbuffer = new StringBuffer();
	if (strings != null) {
	    if (strings[3] != null)
		stringbuffer.append(strings[3]);
	    if (strings[1] != null) {
		if (stringbuffer.length() > 0)
		    stringbuffer.append(' ');
		stringbuffer.append(strings[1]);
	    }
	    if (strings[2] != null) {
		if (stringbuffer.length() > 0)
		    stringbuffer.append(' ');
		stringbuffer.append(strings[2]);
	    }
	    if (strings[0] != null) {
		if (stringbuffer.length() > 0)
		    stringbuffer.append(' ');
		stringbuffer.append(strings[0].toUpperCase());
	    }
	    if (strings[4] != null) {
		if (stringbuffer.length() > 0)
		    stringbuffer.append(", ");
		stringbuffer.append(strings[4]);
	    }
	}
	return new String(stringbuffer);
    }
    
    synchronized void encode(StringBuffer stringbuffer) {
	if (nb_groups >= 1) {
	    stringbuffer.append(componentGroup(0));
	    for (int i = 1; i < nb_groups; i++) {
		stringbuffer.append('=');
		stringbuffer.append(componentGroup(i));
	    }
	}
    }
    
    private synchronized StringBuffer componentGroup(int i) {
	StringBuffer stringbuffer = new StringBuffer();
	String[] strings = groups[i];
	int i_2_;
	for (i_2_ = strings.length - 1; i_2_ >= 0 && strings[i_2_] == null;
	     i_2_--) {
	    /* empty */
	}
	for (int i_3_ = 0; i_3_ <= i_2_; i_3_++) {
	    if (i_3_ > 0)
		stringbuffer.append('^');
	    stringbuffer.append(strings[i_3_]);
	}
	if (stringbuffer.length() > 64)
	    stringbuffer.setLength(64);
	return stringbuffer;
    }
    
    public void add(String[] strings) {
	groups[nb_groups++] = strings;
    }
    
    public String[] get(int i) {
	return groups[i];
    }
    
    public void remove(int i) {
	if (i < nb_groups) {
	    groups[i] = null;
	    for (int i_4_ = i + 1; i_4_ < nb_groups; i_4_++)
		groups[i_4_ - 1] = groups[i_4_];
	    groups[--nb_groups] = null;
	}
    }
    
    public int size() {
	return nb_groups;
    }
    
    public void set(int i, String[] strings) {
	groups[i] = strings;
    }
}
