/* UnknownValue - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.Serializable;

public final class UnknownValue implements Serializable
{
    static UnknownValue instance = new UnknownValue();
    
    public String toString() {
	return "<?>";
    }
}
