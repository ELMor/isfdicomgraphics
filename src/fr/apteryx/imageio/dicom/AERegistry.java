/* AERegistry - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.net.InetAddress;
import java.util.Hashtable;

public class AERegistry
{
    private Hashtable reg = new Hashtable();
    
    private static class Addr
    {
	InetAddress ia;
	int port;
	
	private Addr() {
	    /* empty */
	}
    }
    
    public void registerAE(String string, InetAddress inetaddress) {
	registerAE(string, inetaddress, 104);
    }
    
    public void deregisterAE(String string) {
	reg.remove(string);
    }
    
    public void registerAE(String string, InetAddress inetaddress, int i) {
	Addr addr = new Addr();
	addr.ia = inetaddress;
	addr.port = i;
	reg.put(string, addr);
    }
    
    public PeerAE getPeerAE(String string) {
	Addr addr = (Addr) reg.get(string);
	if (addr == null)
	    return null;
	return new PeerAE(addr.ia, addr.port, Plugin.getApplicationTitle(),
			  string);
    }
}
