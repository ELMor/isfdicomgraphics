/* ZipImageInputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageInputStreamImpl;

final class ZipImageInputStream extends ImageInputStreamImpl
{
    final ImageInputStream iis;
    private byte[] singleByteBuf = new byte[1];
    private Inflater inf = new Inflater(false);
    private byte[] buf = new byte[512];
    
    ZipImageInputStream(ImageInputStream imageinputstream) {
	iis = imageinputstream;
	setByteOrder(ByteOrder.BIG_ENDIAN);
    }
    
    public int read() throws IOException {
	return read(singleByteBuf, 0, 1) == -1 ? -1 : singleByteBuf[0] & 0xff;
    }
    
    public int read(byte abyte0[], int i, int j)
    throws IOException
{
    if((i | j | i + j | abyte0.length - (i + j)) < 0)
        throw new IndexOutOfBoundsException();
    if(j == 0)
        return 0;
    int k;
	while(true){
	    try {
            if((k = inf.inflate(abyte0, i, j)) != 0)
                break; /* Loop/switch isn't completed */
        } catch (DataFormatException e) {
            throw new IOException("Invalid zlib file");
        }
	    if(inf.finished() || inf.needsDictionary())
	        return -1;
	    if(inf.needsInput())
	        fill();
	}
    return k;
}
    
    private void fill() throws IOException {
	int i = iis.read(buf, 0, buf.length);
	iis.flush();
	if (i == -1)
	    throw new EOFException("Unexpected end of ZLIB input stream");
	inf.setInput(buf, 0, i);
    }
}
