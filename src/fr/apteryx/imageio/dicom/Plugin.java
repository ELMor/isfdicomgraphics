/* Plugin - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.math.BigInteger;

public abstract class Plugin
{
    static final boolean demo = true;
    static final boolean debug = false;
    static final int WELLKNOWN_PORT = 104;
    private static long serial = 0L;
    private static int subserial = 0;
    private static int max_user;
    private static boolean evaluation;
    private static String appli = "ImageIO Plugin";
    static final String version = "1.08pre";
    static int ARTIM = 30000;
    static boolean serverDaemon = true;
    private static final BigInteger modulus;
    private static final BigInteger pub;
    
    public static synchronized void setLicenseKey(String string) {
	/* empty */
    }
    
    public static synchronized void setSubSerial(int i) {
	throw new Error("Insufficient License");
    }
    
    public static synchronized void setApplicationTitle(String string) {
	appli = string;
    }
    
    static synchronized long getSerial() {
	return serial;
    }
    
    static synchronized String getSerialString() {
	getSerial();
	return (subserial == 0 ? String.valueOf(serial)
		: serial + "." + subserial);
    }
    
    public static synchronized String getApplicationTitle() {
	return appli;
    }
    
    public static void setARTIM(int i) {
	ARTIM = i;
    }
    
    public static boolean serversAreDaemons() {
	return serverDaemon;
    }
    
    public static void setServersAreDaemons(boolean bool) {
	serverDaemon = bool;
    }
    
    Plugin() {
	/* empty */
    }
    
    public static void addSupportedSOPClass(String string) {
	Modality.sop2mod.put(string, null);
    }
    
    public static void removeSupportedSOPClass(String string) {
	Modality.sop2mod.remove(string);
    }
    
    static {
	modulus = pub = null;
    }
}
