/* TempFileImageInputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.File;
import java.io.IOException;

import javax.imageio.stream.FileImageInputStream;

final class TempFileImageInputStream extends FileImageInputStream
{
    private File f;
    
    TempFileImageInputStream(File file) throws IOException {
	super(file);
	f = file;
    }
    
    protected void finalize() throws Throwable {
	super.finalize();
	f.delete();
    }
}
