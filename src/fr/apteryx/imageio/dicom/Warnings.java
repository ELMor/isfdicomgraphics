/* Warnings - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.util.ArrayList;

final class Warnings
{
    private final ArrayList al = new ArrayList();
    
    private static class Warning
    {
	String s;
	String d;
	
	Warning(String string, String string_0_) {
	    s = string;
	    d = string_0_;
	}
    }
    
    void add(String string, String string_1_) {
	al.add(new Warning(string, string_1_));
    }
    
    boolean hasSome() {
	return al.size() != 0;
    }
    
    void dump(DicomWarningListener dicomwarninglistener) {
	for (int i = 0; i < al.size(); i++) {
	    Warning warning = (Warning) al.get(i);
	    dicomwarninglistener.warning(warning.s, warning.d);
	}
    }
}
