/* FileSet - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.imageio.stream.FileImageInputStream;

public class FileSet
{
    private final DataSet ds;
    private final Directory root;
    private final HashMap hm = new HashMap();
    /*synthetic*/ static Class class$java$util$Collection;
    
    public class Record
    {
	final DataSet ds;
	final long lowerlevel;
	
	Record(DataSet dataset) {
	    super();
	    ds = dataset;
	    lowerlevel = dataset.findLong(267296);
	}
	
	public String getType() {
	    return ds.findString(267312);
	}
	
	public Directory getLowerLevelDirectory() throws DicomException {
	    if (lowerlevel == 0L)
		return null;
	    return new Directory(lowerlevel);
	}
	
	public File getFile() throws DicomException {
	    Object object = ds.findValue(267520);
	    if (object != null)
		return objectToFile(object);
	    long l = ds.findLong(267524);
	    if (l == 0L)
		return null;
	    return FileSet.this.getRecordByOff(l).getFile();
	}
	
	public String getSOPInstance() {
	    return ds.findString(267537);
	}
	
	public String getSOPClass() {
	    return ds.findString(267536);
	}
	
	public Object getAttribute(int i) {
	    return ds.findValue(i);
	}
	
	public DataSet getAttributes() {
	    return ds;
	}
	
	public String toString() {
	    return toString("");
	}
	
	private String toString(String string) {
	    StringBuffer stringbuffer = new StringBuffer("Record type:");
	    stringbuffer.append(getType());
	    try {
		File file = getFile();
		if (file != null) {
		    stringbuffer.append(", file:");
		    stringbuffer.append(file);
		}
	    } catch (DicomException dicomexception) {
		stringbuffer.append(", file: error");
	    }
	    try {
		Directory directory = getLowerLevelDirectory();
		if (directory != null && directory.getNumRecords() > 0) {
		    stringbuffer.append("\n");
		    stringbuffer.append(directory.toString(string + ' '));
		}
	    } catch (DicomException dicomexception) {
		stringbuffer.append("\nLower level: error");
	    }
	    return stringbuffer.toString();
	}
    }
    
    public class Directory
    {
	private final ArrayList records;
	
	Directory(long l) throws DicomException {
	    super();
	    records = new ArrayList();
	    Record record;
	    for (/**/; l > 0L; l = record.ds.findLong(267264)) {
		record = FileSet.this.getRecordByOff(l);
		records.add(record);
	    }
	}
	
	public Record getRecord(int i) {
	    return (Record) records.get(i);
	}
	
	public int getNumRecords() {
	    return records.size();
	}
	
	public String toString() {
	    return toString("");
	}
	
	private String toString(String string) {
	    StringBuffer stringbuffer = new StringBuffer();
	    for (int i = 0; i < records.size(); i++) {
		stringbuffer.append(string);
		stringbuffer
		    .append(((Record) records.get(i)).toString(string));
		stringbuffer.append("\n");
	    }
	    return new String(stringbuffer);
	}
    }
    
    public FileSet(File file, DicomReader dicomreader) throws IOException {
	DicomObject dicomobject
	    = new DicomObject(new FileImageInputStream(file),
			      dicomreader == null ? null : dicomreader.wl,
			      false);
	if (!dicomobject.sopClass.equals("1.2.840.10008.1.3.10"))
	    throw new DicomException("ProtocoleViolation",
				     ("Object in file " + file
				      + " is not a directory"));
	ds = dicomobject.ds;
	Collection collection
	    = ((Collection)
	       ds.findValue(266784, Collection.class));
	if (collection == null)
	    root = null;
	else {
	    Iterator iterator = collection.iterator();
	    while (iterator.hasNext()) {
		DataSet dataset = (DataSet) iterator.next();
		hm.put(new Long(dataset.offset), new Record(dataset));
	    }
	    root = new Directory(ds.findLong(266752));
	}
    }
    
    private static File objectToFile(Object object) {
	if (object instanceof String)
	    return new File((String) object);
	if (object instanceof String[]) {
	    String[] strings = (String[]) object;
	    if (strings.length < 1)
		return null;
	    StringBuffer stringbuffer = new StringBuffer(strings[0]);
	    for (int i = 1; i < strings.length; i++) {
		stringbuffer.append(File.separator);
		stringbuffer.append(strings[i]);
	    }
	    return new File(stringbuffer.toString());
	}
	return null;
    }
    
    public String getID() {
	return ds.findString(266544);
    }
    
    public File getDescriptorFile() {
	return objectToFile(ds.findValue(266561));
    }
    
    public Directory getRootDirectory() {
	return root;
    }
    
    private Record getRecordByOff(long l) throws DicomException {
	Record record = (Record) hm.get(new Long(l));
	if (record == null)
	    throw new DicomException("ProtocoleViolation",
				     "Invalid offset in dicom directory");
	return record;
    }
    
    public String toString() {
	return ("Dicom File Set\nId:" + getID() + "\nDescriptor file:"
		+ getDescriptorFile() + "\nRoot directory:\n" + root);
    }
    
}
