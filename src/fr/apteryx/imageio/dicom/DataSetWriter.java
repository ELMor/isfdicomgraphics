/*
 * DataSetWriter - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.SampleModel;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import com.sun.media.imageio.plugins.jpeg2000.J2KImageWriteParam;

class DataSetWriter extends DataSetReaderWriter {
    private final ImageOutputStream os;

    private final TransferSyntax TS;

    private final DataSet ds;

    private final boolean strict;

    private final DicomWriter writer;

    private static Hashtable writers = new Hashtable();

    static interface ValueWriter {
        public void write(DataSetWriter datasetwriter, int i, Object object)
                throws DicomException, IOException;
    }

    DataSetWriter(ImageOutputStream imageoutputstream,
            TransferSyntax transfersyntax, DataSet dataset, boolean bool,
            DicomWriter dicomwriter) {
        os = imageoutputstream;
        TS = transfersyntax;
        ds = dataset;
        strict = bool;
        writer = dicomwriter;
    }

    private void initByteOrder() {
        os.setByteOrder(TS.bigEndian ? ByteOrder.BIG_ENDIAN
                : ByteOrder.LITTLE_ENDIAN);
    }

    void write() throws DicomException, IOException {
        initByteOrder();
        Iterator iterator = ds.values().iterator();
        while (iterator.hasNext())
            write((DataElement) iterator.next());
    }

    private static String join(Object object, String string, int i, int i_0_,
            boolean bool) {
        if (Array.getLength(object) == 0)
            return "";
        StringBuffer stringbuffer = new StringBuffer();
        for (int i_1_ = 0; i_1_ < Array.getLength(object); i_1_++) {
            Object object_2_ = Array.get(object, i_1_);
            String string_3_ = (bool ? (object_2_ instanceof Number ? Integer
                    .toString(((Number) object_2_).intValue()) : "")
                    : object_2_ == null ? "" : object_2_.toString());
            if (i_1_ > 0)
                stringbuffer.append(string);
            stringbuffer.append(string_3_.length() > i_0_ ? string_3_
                    .substring(0, i_0_) : string_3_);
            for (int i_4_ = string_3_.length(); i_4_ < i; i_4_++)
                stringbuffer.append(' ');
        }
        return new String(stringbuffer);
    }

    private static String buildStringValue(Object object, int i, int i_5_,
            char c, boolean bool) {
        if (object == null)
            return "";
        if (object.getClass().isArray()) {
            String string = join(object, "\\", i, i_5_, bool);
            return (string.length() & 0x1) == 1 ? string + c : string;
        }
        String string;
        if (bool) {
            if (object instanceof Number)
                string = Integer.toString(((Number) object).intValue());
            else
                return "";
        } else
            string = object.toString();
        if (string.length() >= i) {
            string = string.length() <= i_5_ ? string : string.substring(0,
                    i_5_);
            return (string.length() & 0x1) == 1 ? string + c : string;
        }
        StringBuffer stringbuffer = new StringBuffer(string);
        for (int i_6_ = string.length(); i_6_ < i; i_6_++)
            stringbuffer.append(c);
        if ((stringbuffer.length() & 0x1) == 1)
            stringbuffer.append(c);
        return new String(stringbuffer);
    }

    private static int writeData(ImageOutputStream imageoutputstream,
            Object object, boolean bool, int i) throws IOException {
        if (object instanceof Collection) {
            int i_7_ = 0;
            Iterator iterator = ((Collection) object).iterator();
            while (iterator.hasNext())
                i_7_ += writeData(imageoutputstream, iterator.next(), bool, i);
            return i_7_;
        }
        return RawWriter
                .writeRaw(imageoutputstream, (IIOImage) object, bool, i);
    }

    private static void writeEncapsulatedImage(
            ImageOutputStream imageoutputstream, IIOImage iioimage,
            ImageWriter imagewriter, ImageWriteParam imagewriteparam,
            boolean bool) throws IOException {
        writeTag(imageoutputstream, -73728);
        long l = imageoutputstream.getStreamPosition();
        ByteOrder byteorder = imageoutputstream.getByteOrder();
        imageoutputstream.writeInt(0);
        imagewriter
                .setOutput(bool ? (ImageOutputStream) (new JPEGFilterOutputStream(
                        imageoutputstream))
                        : imageoutputstream);
        imagewriter.write(null, iioimage, imagewriteparam);
        imagewriter.setOutput(null);
        imageoutputstream.setByteOrder(byteorder);
        long l_8_ = imageoutputstream.getStreamPosition();
        if ((l_8_ - l & 0x1L) == 1L) {
            imageoutputstream.writeByte(0);
            l_8_++;
        }
        imageoutputstream.seek(l);
        imageoutputstream.writeInt((int) (l_8_ - l - 4L));
        imageoutputstream.seek(l_8_);
    }

    private static void writeImageData(ImageOutputStream imageoutputstream,
            Object object, boolean bool, int i) throws DicomException,
            IOException {
        Object object_9_ = object;
        int i_10_ = 1;
        for (/**/; object_9_ instanceof Collection; object_9_ = ((Collection) object_9_)
                .iterator().next())
            i_10_ *= ((Collection) object_9_).size();
        if (!(object_9_ instanceof IIOImage))
            throw new DicomException("ProtocoleViolation", "Invalid image data");
        IIOImage iioimage = (IIOImage) object_9_;
        SampleModel samplemodel;
        if (iioimage.hasRaster()) {
            Raster raster = iioimage.getRaster();
            samplemodel = raster.getSampleModel();
            i_10_ = i_10_
                    * (raster.getHeight() * raster.getWidth() * (i > 0 ? 1
                            + (samplemodel.getNumBands() - 1) / (2 * i)
                            : samplemodel.getNumBands()));
        } else {
            RenderedImage renderedimage = iioimage.getRenderedImage();
            samplemodel = renderedimage.getSampleModel();
            i_10_ = i_10_
                    * (renderedimage.getHeight() * renderedimage.getWidth() * (i > 0 ? 1
                            + (samplemodel.getNumBands() - 1) / (2 * i)
                            : samplemodel.getNumBands()));
        }
        int i_11_ = samplemodel.getDataType();
        if (i_11_ == 2 || i_11_ == 1)
            i_10_ *= 2;
        else if (i_11_ != 0)
            throw new DicomException("ProtocoleViolation", "Invalid pixel type");
        imageoutputstream.writeInt((i_10_ & 0x1) == 1 ? i_10_ + 1 : i_10_);
        if (writeData(imageoutputstream, object, bool, i) != i_10_)
            throw new DicomException("ProtocoleViolation",
                    "Not consistent image size");
        if ((i_10_ & 0x1) == 1)
            imageoutputstream.write(0);
    }

    private static void writeLen(DataSetWriter datasetwriter, int i)
            throws IOException {
        if (datasetwriter.TS.explicitVR)
            datasetwriter.os.writeShort(i);
        else
            datasetwriter.os.writeInt(i);
    }

    private void write(int i, String string, Object object) throws IOException,
            DicomException {
        if (string.equals("up"))
            string = "UL";
        else if (string.equals("xs"))
            string = ds.findInt(2621699) == 0 ? "US" : "SS";
        else if (string.equals("ox")) {
            if ((i & ~0xff0000) == 1342185484)
                string = ds.findInt(i & ~0xffff | 0x2002) == 0 ? "OW" : "OB";
            else if (TS.explicitVR) {
                if (i == 2145386512) {
                    if (ds.findInt(2621696) <= 8 || TS.encapsulated)
                        string = "OB";
                    else
                        string = "OW";
                } else
                    string = "OW";
            } else if (i == 2145386512 || (i & ~0xff0000) == 1610625024
                    || i == 1409290256 || i >= 2626049 && i <= 2626051
                    || i >= 2626081 && i <= 2626083)
                string = "OW";
            else if ((i & ~0xff0000) == 1342189568) {
                int i_12_ = ds.findInt(i & ~0xffff | 0x103);
                switch (i_12_) {
                case 1:
                    string = "SS";
                    break;
                case 2:
                    string = "FL";
                    break;
                case 3:
                    string = "FD";
                    break;
                case 4:
                    string = "SL";
                    break;
                default:
                    string = "US";
                }
            } else
                string = "OB";
        }
        if (TS.explicitVR)
            os.writeBytes(string);
        if (object instanceof byte[]) {
            byte[] is = (byte[]) object;
            if (TS.explicitVR) {
                if (string.equals("OB") || string.equals("UT")
                        || string.equals("OW") || string.equals("OF")
                        || string.equals("SQ") || string.equals("UN")) {
                    os.writeShort(0);
                    os.writeInt(is.length);
                } else
                    os.writeShort(is.length);
            } else
                os.writeInt(is.length);
            os.write(is);
        } else if (i == 2145386512 && object instanceof byte[][]) {
            if (TS.explicitVR)
                os.writeShort(0);
            os.writeInt(-1);
            byte[][] is = (byte[][]) object;
            for (int i_13_ = 0; i_13_ < is.length; i_13_++) {
                writeTag(os, -73728);
                os.writeInt(is[i_13_].length);
                os.write(is[i_13_]);
            }
            writeTag(os, -73507);
            os.writeInt(0);
        } else {
            ValueWriter valuewriter = (ValueWriter) writers.get(string);
            if (valuewriter == null)
                throw new DicomException("ProtocoleViolation", "Unknown VR:"
                        + string);
            valuewriter.write(this, i, object);
        }
    }

    private static void writeTag(ImageOutputStream imageoutputstream, int i)
            throws IOException {
        imageoutputstream.writeShort(Tag.getGroupNumber(i));
        imageoutputstream.writeShort(Tag.getElementNumber(i));
    }

    private void write(DataElement dataelement) throws DicomException,
            IOException {
        if(dataelement.value instanceof UnknownValue)
            return;
        writeTag(os, dataelement.tag);
        Attribute attribute = dataelement.getAttribute();
        if (attribute == null)
            throw new DicomException("Unsupported", ("Unknown tag " + Tag
                    .toString(dataelement.tag)));
        write(dataelement.tag, attribute.VR, dataelement.value);
    }

    static {
        writers.put("AE", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 16, ' ', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        writers.put("AS", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 4, 4, ' ', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        writers.put("AT", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else if (object instanceof Integer) {
                    writeLen(datasetwriter, 4);
                    writeTag(datasetwriter.os, ((Integer) object).intValue());
                } else if (object.getClass().isArray()) {
                    writeLen(datasetwriter, 4 * Array.getLength(object));
                    for (int i_15_ = 0; i_15_ < Array.getLength(object); i_15_++) {
                        Object object_16_ = Array.get(object, i_15_);
                        if (!(object_16_ instanceof Number))
                            throw new DicomException("ProtocoleViolation",
                                    "Incorrect value type for AT VR");
                        writeTag(datasetwriter.os, ((Number) object_16_)
                                .intValue());
                    }
                } else
                    throw new DicomException("ProtocoleViolation",
                            "Incorrect value type for VR AT");
            }
        });
        writers.put("CS", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 16, ' ', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        writers.put("DA", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else if (object instanceof Date) {
                    if (datasetwriter != null) {
                        /* empty */
                    }
                    String string = DataSetWriter.formats.dicom3DA
                            .format((Date) object);
                    writeLen(datasetwriter, 8);
                    datasetwriter.os.writeBytes(string);
                } else if (object instanceof Date[]) {
                    Date[] dates = (Date[]) object;
                    if (dates.length == 0)
                        writeLen(datasetwriter, 0);
                    else {
                        StringBuffer stringbuffer = new StringBuffer(
                                DataSetWriter.formats.dicom3DA.format(dates[0]));
                        if (datasetwriter != null) {
                            /* empty */
                        }
                        StringBuffer stringbuffer_17_ = stringbuffer;
                        for (int i_18_ = 1; i_18_ < dates.length; i_18_++) {
                            stringbuffer_17_.append("\\");
                            StringBuffer stringbuffer_19_ = stringbuffer_17_;
                            if (datasetwriter != null) {
                                /* empty */
                            }
                            stringbuffer_19_
                                    .append(DataSetWriter.formats.dicom3DA
                                            .format(dates[i_18_]));
                        }
                        if ((stringbuffer_17_.length() & 0x1) == 1)
                            stringbuffer_17_.append(' ');
                        writeLen(datasetwriter, stringbuffer_17_.length());
                        datasetwriter.os
                                .writeBytes(new String(stringbuffer_17_));
                    }
                } else if (!datasetwriter.strict) {
                    String string = buildStringValue(object, 0, 255, ' ', false);
                    writeLen(datasetwriter, string.length());
                    datasetwriter.os.writeBytes(string);
                } else
                    throw new DicomException("ProtocoleViolation",
                            "Incorrect value type for VR DA");
            }
        });
        writers.put("DS", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 16, ' ', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        writers.put("DT", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else if (object instanceof Date) {
                    if (datasetwriter != null) {
                        /* empty */
                    }
                    String string = DataSetWriter.formats.DTWrite.format(
                            (Date) object).trim();
                    if ((string.length() & 0x1) == 1)
                        string += ' ';
                    writeLen(datasetwriter, string.length());
                    datasetwriter.os.writeBytes(string);
                } else if (object instanceof Date[]) {
                    Date[] dates = (Date[]) object;
                    if (dates.length == 0)
                        writeLen(datasetwriter, 0);
                    else {
                        StringBuffer stringbuffer = new StringBuffer(
                                DataSetWriter.formats.DTWrite.format(dates[0])
                                        .trim());
                        StringBuffer stringbuffer_20_ = stringbuffer;
                        for (int i_21_ = 1; i_21_ < dates.length; i_21_++) {
                            stringbuffer_20_.append("\\");
                            StringBuffer stringbuffer_22_ = stringbuffer_20_;
                            if (datasetwriter != null) {
                                /* empty */
                            }
                            stringbuffer_22_
                                    .append(DataSetWriter.formats.dicom3DA
                                            .format(dates[i_21_]).trim());
                        }
                        if ((stringbuffer_20_.length() & 0x1) == 1)
                            stringbuffer_20_.append(' ');
                        writeLen(datasetwriter, stringbuffer_20_.length());
                        datasetwriter.os
                                .writeBytes(new String(stringbuffer_20_));
                    }
                } else if (!datasetwriter.strict) {
                    String string = buildStringValue(object, 0, 255, ' ', false);
                    writeLen(datasetwriter, string.length());
                    datasetwriter.os.writeBytes(string);
                } else
                    throw new DicomException("ProtocoleViolation",
                            "Incorrect value type for VR DT");
            }
        });
        writers.put("FL", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else if (object instanceof Number) {
                    writeLen(datasetwriter, 4);
                    datasetwriter.os.writeFloat(((Number) object).floatValue());
                } else if (object.getClass().isArray()) {
                    writeLen(datasetwriter, 4 * Array.getLength(object));
                    for (int i_23_ = 0; i_23_ < Array.getLength(object); i_23_++) {
                        Object object_24_ = Array.get(object, i_23_);
                        if (!(object_24_ instanceof Number))
                            throw new DicomException("ProtocoleViolation",
                                    "Incorrect value type for FL VR");
                        datasetwriter.os.writeFloat(((Number) object_24_)
                                .floatValue());
                    }
                } else
                    throw new DicomException("ProtocoleViolation",
                            "Incorrect value type for FL VR");
            }
        });
        writers.put("FD", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else if (object instanceof Number) {
                    writeLen(datasetwriter, 8);
                    datasetwriter.os.writeDouble(((Number) object)
                            .doubleValue());
                } else if (object.getClass().isArray()) {
                    writeLen(datasetwriter, 8 * Array.getLength(object));
                    for (int i_25_ = 0; i_25_ < Array.getLength(object); i_25_++) {
                        Object object_26_ = Array.get(object, i_25_);
                        if (!(object_26_ instanceof Number))
                            throw new DicomException("ProtocoleViolation",
                                    "Incorrect value type for FD VR");
                        datasetwriter.os.writeDouble(((Number) object_26_)
                                .doubleValue());
                    }
                } else
                    throw new DicomException("ProtocoleViolation",
                            "Incorrect value type for VR FD");
            }
        });
        writers.put("IS", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 12, ' ', true);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        writers.put("LO", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 64, ' ', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        writers.put("LT", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 10240, ' ', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        ValueWriter valuewriter = new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (datasetwriter.TS.explicitVR)
                    datasetwriter.os.writeShort(0);
                if (object == null)
                    datasetwriter.os.writeInt(0);
                else if (object instanceof byte[]) {
                    byte[] is = (byte[]) object;
                    if ((is.length & 0x1) == 1) {
                        datasetwriter.os.writeInt(is.length + 1);
                        datasetwriter.os.write(is);
                        datasetwriter.os.write(0);
                    } else {
                        datasetwriter.os.writeInt(is.length);
                        datasetwriter.os.write(is);
                    }
                } else if (object instanceof short[]) {
                    short[] is = (short[]) object;
                    datasetwriter.os.writeInt(is.length * 2);
                    for (int i_27_ = 0; i_27_ < is.length; i_27_++)
                        datasetwriter.os.writeShort(is[i_27_]);
                } else if (object instanceof Collection
                        || object instanceof IIOImage) {
                    if (datasetwriter.TS.encapsulated) {
                        ImageWriter imagewriter = datasetwriter.TS.getWriter();
                        ImageWriteParam imagewriteparam = imagewriter
                                .getDefaultWriteParam();
                        imagewriteparam.setCompressionMode(2);
                        boolean bool = false;
                        if (datasetwriter.TS == TransferSyntax.JPEG_BASELINE_1) {
                            imagewriteparam.setCompressionType("JPEG");
                            imagewriteparam
                                    .setCompressionQuality(datasetwriter.writer
                                            .getJPEGQuality());
                            bool = true;
                        } else if (datasetwriter.TS == TransferSyntax.JPEG_LOSSLESS_14SV1) {
                            imagewriteparam.setCompressionType("JPEG-LOSSLESS");
                            bool = true;
                        } else if (datasetwriter.TS == TransferSyntax.JPEG_LS_LOSSLESS) {
                            imagewriteparam.setCompressionType("JPEG-LS");
                            bool = true;
                        } else if ((datasetwriter.TS == TransferSyntax.JPEG2000_LOSSLESS)
                                || (datasetwriter.TS == TransferSyntax.JPEG2000)) {
                            J2KImageWriteParam j2kimagewriteparam = ((J2KImageWriteParam) (Object) imagewriteparam);
                            j2kimagewriteparam.setComponentTransformation(true);
                            j2kimagewriteparam.setWriteCodeStreamOnly(true);
                            if (datasetwriter.TS == TransferSyntax.JPEG2000_LOSSLESS) {
                                j2kimagewriteparam
                                        .setEncodingRate(1.7976931348623157E308);
                                j2kimagewriteparam.setLossless(true);
                                J2KImageWriteParam j2kimagewriteparam_28_ = j2kimagewriteparam;
                                if (j2kimagewriteparam != null) {
                                    /* empty */
                                }
                                j2kimagewriteparam_28_.setFilter("w5x3");
                            } else {
                                j2kimagewriteparam.setLossless(false);
                                J2KImageWriteParam j2kimagewriteparam_29_ = j2kimagewriteparam;
                                if (j2kimagewriteparam != null) {
                                    /* empty */
                                }
                                j2kimagewriteparam_29_.setFilter("w9x7");
                                j2kimagewriteparam
                                        .setEncodingRate(datasetwriter.writer
                                                .getJPEG2000EncodingRate());
                            }
                        }
                        datasetwriter.os.writeInt(-1);
                        writeTag(datasetwriter.os, -73728);
                        if (object instanceof Collection) {
                            Collection collection = (Collection) object;
                            long[] ls = new long[collection.size()];
                            datasetwriter.os.writeInt(ls.length * 4);
                            long l = datasetwriter.os.getStreamPosition();
                            datasetwriter.os.skipBytes(ls.length * 4);
                            int i_30_ = 0;
                            Iterator iterator = collection.iterator();
                            while (iterator.hasNext()) {
                                ls[i_30_++] = datasetwriter.os
                                        .getStreamPosition();
                                writeEncapsulatedImage(datasetwriter.os,
                                        ((IIOImage) iterator.next()),
                                        imagewriter, imagewriteparam, bool);
                            }
                            long l_31_ = datasetwriter.os.getStreamPosition();
                            datasetwriter.os.seek(l);
                            for (i_30_ = 0; i_30_ < ls.length; i_30_++)
                                datasetwriter.os
                                        .writeInt((int) (ls[i_30_] - ls[0]));
                            datasetwriter.os.seek(l_31_);
                        } else {
                            datasetwriter.os.writeInt(0);
                            writeEncapsulatedImage(datasetwriter.os,
                                    (IIOImage) object, imagewriter,
                                    imagewriteparam, bool);
                        }
                        imagewriter.dispose();
                        writeTag(datasetwriter.os, -73507);
                        datasetwriter.os.writeInt(0);
                    } else {
                        String string = datasetwriter.ds.findString(2621444);
                        int i_32_ = 0;
                        if (string != null) {
                            if (string.endsWith("_422"))
                                i_32_ = 1;
                            else if (string.endsWith("_420"))
                                i_32_ = 2;
                        }
                        writeImageData(
                                datasetwriter.os,
                                object,
                                (datasetwriter.ds.findInt(2621442) == 1 || (datasetwriter.ds
                                        .findInt(2621446) != 0)), i_32_);
                    }
                } else
                    throw new DicomException(
                            "ProtocoleViolation",
                            ("Incorrect value for VR Ox (" + object.getClass() + ")"));
            }
        };
        writers.put("OB", valuewriter);
        writers.put("OW", valuewriter);
        writers.put("OF", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (datasetwriter.TS.explicitVR)
                    datasetwriter.os.writeShort(0);
                if (object == null)
                    datasetwriter.os.writeInt(0);
                else if (object instanceof float[]) {
                    float[] fs = (float[]) object;
                    datasetwriter.os.writeInt(fs.length * 4);
                    for (int i_33_ = 0; i_33_ < fs.length; i_33_++)
                        datasetwriter.os.writeFloat(fs[i_33_]);
                } else
                    throw new DicomException(
                            "ProtocoleViolation",
                            ("Incorrect value for VR OF (" + object.getClass() + ")"));
            }
        });
        writers.put("PN", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else {
                    StringBuffer stringbuffer = new StringBuffer();
                    if (object instanceof PersonName)
                        ((PersonName) object).encode(stringbuffer);
                    else if (object instanceof PersonName[]) {
                        PersonName[] personnames = (PersonName[]) object;
                        for (int i_34_ = 0; i_34_ < personnames.length; i_34_++) {
                            if (i_34_ > 0)
                                stringbuffer.append("\\");
                            personnames[i_34_].encode(stringbuffer);
                        }
                    } else
                        stringbuffer.append(buildStringValue(object, 0, 64,
                                ' ', false));
                    if ((stringbuffer.length() & 0x1) == 1)
                        stringbuffer.append(' ');
                    writeLen(datasetwriter, stringbuffer.length());
                    datasetwriter.os.writeBytes(new String(stringbuffer));
                }
            }
        });
        writers.put("SH", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 16, ' ', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        writers.put("SQ", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (datasetwriter.TS.explicitVR)
                    datasetwriter.os.writeShort(0);
                if (object == null)
                    datasetwriter.os.writeInt(0);
                else {
                    if (object instanceof DataSet) {
                        Object object_35_ = object;
                        object = new ArrayList(1);
                        ((ArrayList) object).add(object_35_);
                    } else if (!(object instanceof Collection))
                        throw new DicomException("ProtocoleViolation",
                                "Incorrect value type for VR SQ");
                    datasetwriter.os.writeInt(-1);
                    Iterator iterator = ((Collection) object).iterator();
                    while (iterator.hasNext()) {
                        writeTag(datasetwriter.os, -73728);
                        datasetwriter.os.writeInt(-1);
                        Object object_36_ = iterator.next();
                        if (!(object_36_ instanceof DataSet))
                            throw new DicomException("ProtocoleViolation",
                                    "Incorrect value type for VR SQ");
                        ((DataSet) object_36_).write(datasetwriter.os,
                                datasetwriter.TS, true, datasetwriter.writer);
                        writeTag(datasetwriter.os, -73715);
                        datasetwriter.os.writeInt(0);
                    }
                    writeTag(datasetwriter.os, -73507);
                    datasetwriter.os.writeInt(0);
                }
            }
        });
        writers.put("ST", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 1024, ' ', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        writers.put("TM", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else if (object instanceof Date) {
                    if (datasetwriter != null) {
                        /* empty */
                    }
                    String string = DataSetWriter.formats.dicom3TM
                            .format((Date) object);
                    writeLen(datasetwriter, 10);
                    datasetwriter.os.writeBytes(string);
                } else if (object instanceof Date[]) {
                    Date[] dates = (Date[]) object;
                    if (dates.length == 0)
                        writeLen(datasetwriter, 0);
                    else {
                        StringBuffer stringbuffer = new StringBuffer(
                                DataSetWriter.formats.dicom3TM.format(dates[0]));
                        StringBuffer stringbuffer_37_ = stringbuffer;
                        for (int i_38_ = 1; i_38_ < dates.length; i_38_++) {
                            stringbuffer_37_.append("\\");
                            StringBuffer stringbuffer_39_ = stringbuffer_37_;
                            if (datasetwriter != null) {
                                /* empty */
                            }
                            stringbuffer_39_
                                    .append(DataSetWriter.formats.dicom3TM
                                            .format(dates[i_38_]));
                        }
                        if ((stringbuffer_37_.length() & 0x1) == 1)
                            stringbuffer_37_.append(' ');
                        writeLen(datasetwriter, stringbuffer_37_.length());
                        datasetwriter.os
                                .writeBytes(new String(stringbuffer_37_));
                    }
                } else if (!datasetwriter.strict) {
                    String string = buildStringValue(object, 0, 255, ' ', false);
                    writeLen(datasetwriter, string.length());
                    datasetwriter.os.writeBytes(string);
                } else
                    throw new DicomException("ProtocoleViolation",
                            "Incorrect value type for VR TM");
            }
        });
        writers.put("UI", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                String string = buildStringValue(object, 0, 64, '\0', false);
                writeLen(datasetwriter, string.length());
                datasetwriter.os.writeBytes(string);
            }
        });
        valuewriter = new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else if (object instanceof Number) {
                    writeLen(datasetwriter, 4);
                    datasetwriter.os.writeInt(((Number) object).intValue());
                } else if (object.getClass().isArray()) {
                    writeLen(datasetwriter, 4 * Array.getLength(object));
                    for (int i_40_ = 0; i_40_ < Array.getLength(object); i_40_++) {
                        Object object_41_ = Array.get(object, i_40_);
                        if (!(object_41_ instanceof Number))
                            throw new DicomException("ProtocoleViolation",
                                    "Incorrect value type for long VR");
                        datasetwriter.os.writeInt(((Number) object_41_)
                                .intValue());
                    }
                } else
                    throw new DicomException("ProtocoleViolation",
                            "Incorrect value type for integer VR");
            }
        };
        writers.put("UL", valuewriter);
        writers.put("SL", valuewriter);
        writers.put("UN", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (datasetwriter.TS.explicitVR)
                    datasetwriter.os.writeShort(0);
                datasetwriter.os.writeInt(0);
            }
        });
        valuewriter = new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (object == null)
                    writeLen(datasetwriter, 0);
                else if (object instanceof Number) {
                    writeLen(datasetwriter, 2);
                    datasetwriter.os.writeShort(((Number) object).intValue());
                } else if (object.getClass().isArray()) {
                    writeLen(datasetwriter, 2 * Array.getLength(object));
                    for (int i_42_ = 0; i_42_ < Array.getLength(object); i_42_++) {
                        Object object_43_ = Array.get(object, i_42_);
                        if (!(object_43_ instanceof Number))
                            throw new DicomException("ProtocoleViolation",
                                    "Incorrect value type for short VR");
                        datasetwriter.os.writeShort(((Number) object_43_)
                                .shortValue());
                    }
                } else
                    throw new DicomException("ProtocoleViolation",
                            "Incorrect value type for short VR");
            }
        };
        writers.put("US", valuewriter);
        writers.put("SS", valuewriter);
        writers.put("UT", new ValueWriter() {

            public void write(DataSetWriter datasetwriter, int i, Object object)
                    throws IOException, DicomException {
                if (datasetwriter.TS.explicitVR)
                    datasetwriter.os.writeShort(0);
                if (object == null)
                    datasetwriter.os.writeInt(0);
                else {
                    String string = object.toString();
                    if ((string.length() & 0x1) == 1) {
                        datasetwriter.os.writeInt(string.length() + 1);
                        datasetwriter.os.writeBytes(string);
                        datasetwriter.os.write(32);
                    } else {
                        datasetwriter.os.writeInt(string.length());
                        datasetwriter.os.writeBytes(string);
                    }
                }
            }
        });
    }
}
