/* VOIColorModel - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.color.ColorSpace;
import java.awt.image.ColorModel;
import java.awt.image.ComponentSampleModel;
import java.awt.image.Raster;
import java.awt.image.SampleModel;

abstract class VOIColorModel extends ColorModel
{
    private final int outbits;
    final int sample_min;
    final int sample_max;
    final int unnorm_max;
    private final boolean isMono1;
    
    VOIColorModel(int i, int i_0_, int i_1_, ColorSpace colorspace) {
	super(i_0_, new int[] { i_1_ }, colorspace, false, false, 1, i);
	outbits = i_1_;
	switch (i) {
	case 0:
	case 1:
	    sample_min = 0;
	    sample_max = (1 << i_0_) - 1;
	    break;
	default:
	    sample_max = (1 << i_0_ - 1) - 1;
	    sample_min = sample_max ^ 0xffffffff;
	}
	unnorm_max = (1 << i_1_) - 1;
	isMono1 = colorspace instanceof DicomColorSpace.Monochrome1;
    }
    
    public final int getAlpha(int i) {
	return 255;
    }
    
    public final int getAlpha(Object object) {
	return 255;
    }
    
    public final int getBlue(int i) {
	return isMono1 ? 255 - get255(i) : get255(i);
    }
    
    public final int getGreen(int i) {
	return isMono1 ? 255 - get255(i) : get255(i);
    }
    
    public final int getRed(int i) {
	return isMono1 ? 255 - get255(i) : get255(i);
    }
    
    public final int getBlue(Object object) {
	int i;
	switch (transferType) {
	case 0:
	    i = get255(((byte[]) object)[0] & 0xff);
	    break;
	case 2:
	    i = get255(((short[]) object)[0]);
	    break;
	case 1:
	    i = get255(((short[]) object)[0] & 0xffff);
	    break;
	default:
	    i = get255(((int[]) object)[0]);
	}
	return isMono1 ? 255 - i : i;
    }
    
    public final int getGreen(Object object) {
	int i;
	switch (transferType) {
	case 0:
	    i = get255(((byte[]) object)[0] & 0xff);
	    break;
	case 2:
	    i = get255(((short[]) object)[0]);
	    break;
	case 1:
	    i = get255(((short[]) object)[0] & 0xffff);
	    break;
	default:
	    i = get255(((int[]) object)[0]);
	}
	return isMono1 ? 255 - i : i;
    }
    
    public final int getRed(Object object) {
	int i;
	switch (transferType) {
	case 0:
	    i = get255(((byte[]) object)[0] & 0xff);
	    break;
	case 2:
	    i = get255(((short[]) object)[0]);
	    break;
	case 1:
	    i = get255(((short[]) object)[0] & 0xffff);
	    break;
	default:
	    i = get255(((int[]) object)[0]);
	}
	return isMono1 ? 255 - i : i;
    }
    
    abstract int getComp(int i);
    
    abstract int get255(int i);
    
    abstract float getNorm(int i);
    
    public final int[] getComponents(int i, int[] is, int i_2_) {
	if (is == null)
	    is = new int[i_2_ + 1];
	is[i_2_] = getComp(i);
	return is;
    }
    
    public final int[] getComponents(Object object, int[] is, int i) {
	if (is == null)
	    is = new int[i + 1];
	switch (transferType) {
	case 0:
	    is[i] = getComp(((byte[]) object)[0] & 0xff);
	    break;
	case 2:
	    is[i] = getComp(((short[]) object)[0]);
	    break;
	case 1:
	    is[i] = getComp(((short[]) object)[0] & 0xffff);
	    break;
	case 3:
	    is[i] = getComp(((int[]) object)[0]);
	    break;
	}
	return is;
    }
    
    public final float[] getNormalizedComponents(int[] is, int i, float[] fs,
						 int i_3_) {
	if (fs == null)
	    fs = new float[i_3_ + 1];
	fs[i_3_] = getNorm(is[i]);
	return fs;
    }
    
    public final float[] getNormalizedComponents(Object object, float[] fs,
						 int i) {
	if (fs == null)
	    fs = new float[i + 1];
	switch (transferType) {
	case 0:
	    fs[i] = getNorm(((byte[]) object)[0] & 0xff);
	    break;
	case 2:
	    fs[i] = getNorm(((short[]) object)[0]);
	    break;
	case 1:
	    fs[i] = getNorm(((short[]) object)[0] & 0xffff);
	    break;
	case 3:
	    fs[i] = getNorm(((int[]) object)[0]);
	    break;
	}
	return fs;
    }
    
    public boolean isCompatibleRaster(Raster raster) {
	return isCompatibleSampleModel(raster.getSampleModel());
    }
    
    public boolean isCompatibleSampleModel(SampleModel samplemodel) {
	return (samplemodel instanceof ComponentSampleModel
		&& samplemodel.getNumBands() == 1
		&& samplemodel.getDataType() == transferType);
    }
    
    public int[] getUnnormalizedComponents(float[] fs, int i, int[] is,
					   int i_4_) {
	if (is == null)
	    is = new int[i_4_ + 1];
	int i_5_ = (int) ((double) fs[i] * ((double) unnorm_max + 0.5));
	is[i_5_] = i_5_ > unnorm_max ? unnorm_max : i_5_;
	return is;
    }
    
    abstract int getDataElement(float f);
    
    public int getDataElement(float[] fs, int i) {
	return getDataElement(fs[i]);
    }
    
    public int getDataElement(int[] is, int i) {
	return getDataElement((float) is[i] / (float) unnorm_max);
    }
    
    public Object getDataElements(float[] fs, int i, Object object) {
	switch (transferType) {
	case 0:
	    if (object == null)
		object = new byte[1];
	    ((byte[]) object)[0] = (byte) getDataElement(fs[i]);
	    break;
	case 1:
	case 2:
	    if (object == null)
		object = new short[1];
	    ((short[]) object)[0] = (short) getDataElement(fs[i]);
	    break;
	case 3:
	    if (object == null)
		object = new int[1];
	    ((int[]) object)[0] = getDataElement(fs[i]);
	    break;
	}
	return object;
    }
    
    public Object getDataElements(int[] is, int i, Object object) {
	switch (transferType) {
	case 0:
	    if (object == null)
		object = new byte[1];
	    ((byte[]) object)[0]
		= (byte) getDataElement((float) is[i] / (float) unnorm_max);
	    break;
	case 1:
	case 2:
	    if (object == null)
		object = new short[1];
	    ((short[]) object)[0]
		= (short) getDataElement((float) is[i] / (float) unnorm_max);
	    break;
	case 3:
	    if (object == null)
		object = new int[1];
	    ((int[]) object)[0]
		= getDataElement((float) is[i] / (float) unnorm_max);
	    break;
	}
	return object;
    }
    
    public Object getDataElements(int i, Object object) {
	float f = ((float) ((i & 0xff) + (i >> 8 & 0xff) + (i >> 16 & 0xff))
		   / 765.0F);
	switch (transferType) {
	case 0:
	    if (object == null)
		object = new byte[1];
	    ((byte[]) object)[0] = (byte) getDataElement(f);
	    break;
	case 1:
	case 2:
	    if (object == null)
		object = new short[1];
	    ((short[]) object)[0] = (short) getDataElement(f);
	    break;
	case 3:
	    if (object == null)
		object = new int[1];
	    ((int[]) object)[0] = getDataElement(f);
	    break;
	}
	return object;
    }
}
