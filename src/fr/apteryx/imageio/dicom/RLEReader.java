/* RLEReader - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteOrder;

import javax.imageio.stream.ImageInputStream;

final class RLEReader
{
    private final byte[] buf = new byte[16384];
    int pos;
    int len;
    
    private final byte nextByte(ImageInputStream imageinputstream)
	throws IOException {
	for (/**/; pos >= len; pos = 0) {
	    len = imageinputstream.read(buf, 0, buf.length);
	    if (len < 0)
		throw new EOFException();
	}
	return buf[pos++];
    }
    
    private final void seek(ImageInputStream imageinputstream, long l)
	throws IOException {
	len = 0;
	imageinputstream.seek(l);
    }
    
    void readRLEImage(ImageInputStream imageinputstream, Raster raster, int i,
		      int i_0_) throws IOException {
	long l = imageinputstream.getStreamPosition();
	int i_1_ = raster.getWidth() * raster.getHeight();
	DataBuffer databuffer = raster.getDataBuffer();
	int i_2_ = raster.getNumBands();
	int i_3_ = databuffer.getSize();
	imageinputstream.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	int i_4_ = imageinputstream.readInt();
	long[] ls = new long[15];
	for (int i_5_ = 0; i_5_ < 15; i_5_++)
	    ls[i_5_] = (long) imageinputstream.readInt() & 0xffffffffL;
	if (i == 8) {
	    if (i_4_ != i_2_)
		throw new DicomException("ProtocoleViolation",
					 "Invalid number of RLE bands");
	    for (int i_6_ = 0; i_6_ < i_2_; i_6_++) {
		seek(imageinputstream, l + ls[i_6_]);
		if (i_0_ == 1 && i_6_ > 0) {
		    int i_7_ = i_6_;
		    while (i_7_ < i_3_) {
			int i_8_ = nextByte(imageinputstream);
			if (i_8_ >= 0) {
			    for (int i_9_ = i_8_; i_9_ >= 0; i_9_--) {
				byte i_10_ = nextByte(imageinputstream);
				databuffer.setElem(i_7_, i_10_);
				i_7_ += i_2_;
				databuffer.setElem(i_7_, i_10_);
				i_7_ += i_2_;
			    }
			} else if (i_8_ != -128) {
			    byte i_11_ = nextByte(imageinputstream);
			    for (int i_12_ = i_8_; i_12_ <= 0; i_12_++) {
				databuffer.setElem(i_7_, i_11_);
				i_7_ += i_2_;
				databuffer.setElem(i_7_, i_11_);
				i_7_ += i_2_;
			    }
			}
		    }
		} else {
		    int i_13_ = i_6_;
		    while (i_13_ < i_3_) {
			int i_14_ = nextByte(imageinputstream);
			if (i_14_ >= 0) {
			    for (int i_15_ = i_14_; i_15_ >= 0; i_15_--) {
				databuffer.setElem(i_13_,
						   nextByte(imageinputstream));
				i_13_ += i_2_;
			    }
			} else if (i_14_ != -128) {
			    byte i_16_ = nextByte(imageinputstream);
			    for (int i_17_ = i_14_; i_17_ <= 0; i_17_++) {
				databuffer.setElem(i_13_, i_16_);
				i_13_ += i_2_;
			    }
			}
		    }
		}
	    }
	} else if (i == 16) {
	    if (i_4_ != 2 * i_2_)
		throw new DicomException("ProtocoleViolation",
					 "Invalid number of RLE bands");
	    for (int i_18_ = 0; i_18_ < i_2_; i_18_++) {
		if (i_0_ == 1 && i_18_ > 0) {
		    seek(imageinputstream, l + ls[2 * i_18_]);
		    int i_19_ = i_18_;
		    while (i_19_ < i_3_) {
			int i_20_ = nextByte(imageinputstream);
			if (i_20_ >= 0) {
			    for (int i_21_ = i_20_; i_21_ >= 0; i_21_--) {
				int i_22_
				    = (nextByte(imageinputstream) & 0xff) << 8;
				databuffer.setElem(i_19_, i_22_);
				i_19_ += i_2_;
				databuffer.setElem(i_19_, i_22_);
				i_19_ += i_2_;
			    }
			} else if (i_20_ != -128) {
			    int i_23_ = nextByte(imageinputstream) & 0xff;
			    for (int i_24_ = i_20_; i_24_ <= 0; i_24_++) {
				databuffer.setElem(i_19_, i_23_ << 8);
				i_19_ += i_2_;
				databuffer.setElem(i_19_, i_23_ << 8);
				i_19_ += i_2_;
			    }
			}
		    }
		    seek(imageinputstream, l + ls[2 * i_18_ + 1]);
		    i_19_ = i_18_;
		    while (i_19_ < i_3_) {
			int i_25_ = nextByte(imageinputstream);
			if (i_25_ >= 0) {
			    for (int i_26_ = i_25_; i_26_ >= 0; i_26_--) {
				int i_27_ = databuffer.getElem(i_19_);
				int i_28_
				    = (i_27_
				       | nextByte(imageinputstream) & 0xff);
				databuffer.setElem(i_19_, i_28_);
				i_19_ += i_2_;
				databuffer.setElem(i_19_, i_28_);
				i_19_ += i_2_;
			    }
			} else if (i_25_ != -128) {
			    int i_29_ = nextByte(imageinputstream) & 0xff;
			    for (int i_30_ = i_25_; i_30_ <= 0; i_30_++) {
				int i_31_ = databuffer.getElem(i_19_) | i_29_;
				databuffer.setElem(i_19_, i_31_);
				i_19_ += i_2_;
				databuffer.setElem(i_19_, i_31_);
				i_19_ += i_2_;
			    }
			}
		    }
		} else {
		    seek(imageinputstream, l + ls[2 * i_18_]);
		    int i_32_ = i_18_;
		    while (i_32_ < i_3_) {
			int i_33_ = nextByte(imageinputstream);
			if (i_33_ >= 0) {
			    for (int i_34_ = i_33_; i_34_ >= 0; i_34_--) {
				databuffer.setElem(i_32_,
						   (nextByte(imageinputstream)
						    & 0xff) << 8);
				i_32_ += i_2_;
			    }
			} else if (i_33_ != -128) {
			    int i_35_ = nextByte(imageinputstream) & 0xff;
			    for (int i_36_ = i_33_; i_36_ <= 0; i_36_++) {
				databuffer.setElem(i_32_, i_35_ << 8);
				i_32_ += i_2_;
			    }
			}
		    }
		    seek(imageinputstream, l + ls[2 * i_18_ + 1]);
		    i_32_ = i_18_;
		    while (i_32_ < i_3_) {
			int i_37_ = nextByte(imageinputstream);
			if (i_37_ >= 0) {
			    for (int i_38_ = i_37_; i_38_ >= 0; i_38_--) {
				int i_39_ = databuffer.getElem(i_32_);
				databuffer.setElem
				    (i_32_, i_39_ | (nextByte(imageinputstream)
						     & 0xff));
				i_32_ += i_2_;
			    }
			} else if (i_37_ != -128) {
			    int i_40_ = nextByte(imageinputstream) & 0xff;
			    for (int i_41_ = i_37_; i_41_ <= 0; i_41_++) {
				int i_42_ = databuffer.getElem(i_32_);
				databuffer.setElem(i_32_, i_42_ | i_40_);
				i_32_ += i_2_;
			    }
			}
		    }
		}
	    }
	}
    }
}
