/* Modality - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.image.IndexColorModel;
import java.awt.image.SampleModel;
import java.util.Collection;
import java.util.HashMap;

import javax.imageio.IIOImage;

abstract class Modality
{
    private static final String[][] SOP_CLASSES
	= { { "1.2.840.10008.5.1.4.1.1.1", "CR" },
	    { "1.2.840.10008.5.1.4.1.1.2", "CT" },
	    { "1.2.840.10008.5.1.4.1.1.2.1", "CT" },
	    { "1.2.840.10008.5.1.4.1.1.4", "MR" },
	    { "1.2.840.10008.5.1.4.1.1.4.1", "MR" },
	    { "1.2.840.10008.5.1.4.1.1.20", "NM" },
	    { "1.2.840.10008.5.1.4.1.1.5", "NM" },
	    { "1.2.840.10008.5.1.4.1.1.6.1", "US" },
	    { "1.2.840.10008.5.1.4.1.1.3.1", "US" },
	    { "1.2.840.10008.5.1.4.1.1.3", "US" },
	    { "1.2.840.10008.5.1.4.1.1.7", null },
	    { "1.2.840.10008.5.1.4.1.1.7.2", null },
	    { "1.2.840.10008.5.1.4.1.1.7.3", null },
	    { "1.2.840.10008.5.1.4.1.1.7.4", null },
	    { "1.2.840.10008.5.1.4.1.1.12.1", "XA" },
	    { "1.2.840.10008.5.1.4.1.1.12.2", "RF" },
	    { "1.2.840.10008.5.1.4.1.1.12.3", "XA" },
	    { "1.2.840.10008.5.1.4.1.1.481.1", "RTIMAGE" },
	    { "1.2.840.10008.5.1.4.1.1.1.1", "DX" },
	    { "1.2.840.10008.5.1.4.1.1.1.1.1", "DX" },
	    { "1.2.840.10008.5.1.4.1.1.1.2", "MG" },
	    { "1.2.840.10008.5.1.4.1.1.1.2.1", "MG" },
	    { "1.2.840.10008.5.1.4.1.1.1.2", "IO" },
	    { "1.2.840.10008.5.1.4.1.1.1.2.1", "IO" },
	    { "1.2.840.10008.5.1.4.1.1.77.1.1", "ES" },
	    { "1.2.840.10008.5.1.4.1.1.77.1.2", "GM" },
	    { "1.2.840.10008.5.1.4.1.1.77.1.3", "SM" },
	    { "1.2.840.10008.5.1.4.1.1.77.1.4", "XC" },
	    { "1.2.840.10008.5.1.4.1.1.77.1.5.1", "OP" },
	    { "1.2.840.10008.5.1.4.1.1.77.1.5.2", "OP" },
	    { "1.2.840.10008.5.1.4.1.1.128", "PT" },
	    { "1.2.840.10008.5.1.1.30", "HC" },
	    { "1.2.840.10008.5.1.1.29", "HC" } };
    static HashMap sop2mod = new HashMap();
    static HashMap mod2sop = new HashMap();
    
    static String sopToModality(String string) {
	return (String) sop2mod.get(string);
    }
    
    static String modalityToSop(String string, Object object) {
	SampleModel samplemodel = Util.getSampleModel(object);
	if (string == null || string.equals("OT")) {
	    java.awt.image.ColorModel colormodel = Util.getColorModel(object);
	    if (colormodel instanceof IndexColorModel) {
		if (object instanceof IIOImage)
		    return "1.2.840.10008.5.1.4.1.1.6.1";
		return "1.2.840.10008.5.1.4.1.1.3.1";
	    }
	    if (object instanceof IIOImage)
		return "1.2.840.10008.5.1.4.1.1.7";
	    if (samplemodel.getNumBands() > 1)
		return "1.2.840.10008.5.1.4.1.1.7.4";
	    switch (samplemodel.getDataType()) {
	    case 0:
		return "1.2.840.10008.5.1.4.1.1.7.2";
	    case 1:
	    case 2:
		return "1.2.840.10008.5.1.4.1.1.7.3";
	    }
	}
	if (string.equals("US"))
	    return (object instanceof Collection
		    ? "1.2.840.10008.5.1.4.1.1.3.1"
		    : "1.2.840.10008.5.1.4.1.1.6.1");
	if (string.equals("HC"))
	    return (samplemodel.getNumBands() > 1 ? "1.2.840.10008.5.1.1.29"
		    : "1.2.840.10008.5.1.1.30");
	return (String) mod2sop.get(string);
    }
    
    static {
	for (int i = SOP_CLASSES.length - 1; i >= 0; i--) {
	    sop2mod.put(SOP_CLASSES[i][0], SOP_CLASSES[i][1]);
	    mod2sop.put(SOP_CLASSES[i][1], SOP_CLASSES[i][0]);
	}
    }
}
