/* Constants - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

interface Constants
{
    public static final String MONOCHROME2 = "MONOCHROME2";
    public static final String MONOCHROME1 = "MONOCHROME1";
    public static final String PALETTE_COLOR = "PALETTE COLOR";
    public static final String YBR_FULL = "YBR_FULL";
    public static final String YBR_FULL_422 = "YBR_FULL_422";
    public static final String YBR_PARTIAL_422 = "YBR_PARTIAL_422";
    public static final String YBR_PARTIAL_420 = "YBR_PARTIAL_420";
    public static final String YBR_ICT = "YBR_ICT";
    public static final String YBR_RCT = "YBR_RCT";
    public static final String RGB = "RGB";
    public static final String HSV = "HSV";
    public static final String CMYK = "CMYK";
    public static final String ARGB = "ARGB";
    public static final String COLOR = "COLOR";
    public static final String MONOCHROME = "MONOCHROME";
    public static final String MIXED = "MIXED";
}
