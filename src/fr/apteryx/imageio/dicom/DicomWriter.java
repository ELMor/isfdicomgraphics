/*
 * DicomWriter - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.stream.ImageOutputStream;

import org.w3c.dom.Node;

public class DicomWriter extends ImageWriter {
    private static final TransferSyntax[] DEFAULT_PREFERRED_TS = { TransferSyntax.EXPLICIT_LITTLEENDIAN };

    private DicomMetadata md;

    private ArrayList sequence;

    final DicomWarningListener wl;

    private boolean raw = false;

    private TransferSyntax[] preferredTS = DEFAULT_PREFERRED_TS;

    private float JPEGQuality = 0.75F;

    private double JPEG2000EncodingRate = 2.0;

    DicomWriter(ImageWriterSpi imagewriterspi) {
        super(imagewriterspi);
        wl = new DicomWarningListener() {

            public void warning(int i, String string, String string_1_) {
                processWarningOccurred(i, DicomException.localize(string) + ": " + string_1_);
            }

            public void warning(String string, String string_2_) {
                warning(0, string, string_2_);
            }
        };
    }

    public void setRawMode(boolean bool) {
        raw = bool;
    }

    public boolean canWriteRasters() {
        return true;
    }

    public IIOMetadata getDefaultStreamMetadata(ImageWriteParam imagewriteparam) {
        return new DicomMetadata();
    }

    public IIOMetadata getDefaultImageMetadata(
            ImageTypeSpecifier imagetypespecifier,
            ImageWriteParam imagewriteparam) {
        return null;
    }

    public IIOMetadata convertStreamMetadata(IIOMetadata iiometadata,
            ImageWriteParam imagewriteparam) {
        if (iiometadata == null)
            return null;
        if (raw)
            return iiometadata;
        if (iiometadata instanceof DicomMetadata) {
            DicomMetadata dicommetadata = (DicomMetadata) iiometadata;
            DataSet dataset = new DataSet();
            Iterator iterator = dicommetadata.obj.ds.values().iterator();
            while (iterator.hasNext()) {
                DataElement dataelement = (DataElement) iterator.next();
                if (dataelement.tag != 2145386512
                        && Tag.getElementNumber(dataelement.tag) != 0)
                    dataset.add(dataelement);
            }
            return new DicomMetadata(dataset, dicommetadata.obj.sopClass,
                    dicommetadata.obj.sopInstance,
                    dicommetadata.obj.transferSyntax);
        }
        do {
            if (iiometadata.isStandardMetadataFormatSupported()) {
                Node node = iiometadata.getAsTree("javax_imageio_1.0");
                if (node != null) {
                    DicomMetadata dicommetadata;
                    try {
                        DicomMetadata dicommetadata_3_ = new DicomMetadata(
                                new DataSet(), null, null, null);
                        dicommetadata_3_.mergeTree("javax_imageio_1.0", node);
                        dicommetadata = dicommetadata_3_;
                    } catch (IIOInvalidTreeException iioinvalidtreeexception) {
                        break;
                    }
                    return dicommetadata;
                }
            }
        } while (false);
        return null;
    }

    public IIOMetadata convertImageMetadata(IIOMetadata iiometadata,
            ImageTypeSpecifier imagetypespecifier,
            ImageWriteParam imagewriteparam) {
        return null;
    }

    private void updateFromImage(DicomMetadata dicommetadata, IIOImage iioimage)
            throws DicomException {
        if (iioimage.hasRaster()) {
            Raster raster = iioimage.getRaster();
            dicommetadata.updateFromImage(raster.getWidth(),
                    raster.getHeight(), raster.getSampleModel(), null);
        } else {
            RenderedImage renderedimage = iioimage.getRenderedImage();
            dicommetadata.updateFromImage(renderedimage.getWidth(),
                    renderedimage.getHeight(), renderedimage.getSampleModel(),
                    renderedimage.getColorModel());
        }
    }

    public void write(IIOMetadata iiometadata, IIOImage iioimage,
            ImageWriteParam imagewriteparam) throws IOException {
        if (raw && iioimage != null)
            throw new DicomException("ApplicationError",
                    "Cannot write IIOImage in raw mode");
        clearAbortRequest();
        DicomMetadata dicommetadata = getMD(iiometadata);
        if (iioimage != null) {
            updateFromImage(dicommetadata, iioimage);
            dicommetadata.obj.ds.add(2145386512, iioimage);
        }
        normalizeAndWrite(dicommetadata);
    }

    private void normalizeAndWrite(DicomMetadata dicommetadata)
            throws IOException {
        if (!raw)
            dicommetadata.normalize(wl);
        if (output instanceof ImageOutputStream) {
            TransferSyntax transfersyntax = dicommetadata.obj
                    .selectSuitableTS(preferredTS);
            if (transfersyntax == null)
                throw new DicomException("ApplicationError",
                        "The selected tranfer syntaxes cannot be used to encode this object");
            dicommetadata.obj.updateFromTransferSyntax(transfersyntax);
            dicommetadata.obj.write((ImageOutputStream) output, transfersyntax,
                    this);
        } 
        else
            throw new IllegalStateException("Output not set");
    }

    public boolean canWriteSequence() {
        return true;
    }

    public boolean canRemoveImage(int i) {
        return sequence != null && sequence.size() > i;
    }

    public boolean canInsertImage(int i) {
        return sequence != null && sequence.size() > i;
    }

    public void prepareWriteSequence(IIOMetadata iiometadata) {
        clearAbortRequest();
        md = getMD(iiometadata);
        sequence = new ArrayList();
    }

    public void abort() {
        super.abort();
        sequence = null;
        md = null;
    }

    public void writeToSequence(IIOImage iioimage,
            ImageWriteParam imagewriteparam) throws DicomException {
        if (raw)
            throw new DicomException("ApplicationError",
                    "Cannot writetoSequence in raw mode");
        if (sequence.isEmpty())
            updateFromImage(md, iioimage);
        sequence.add(iioimage);
    }

    public void removeImage(int i) {
        sequence.remove(i);
    }

    public void writeInsert(int i, IIOImage iioimage,
            ImageWriteParam imagewriteparam) throws DicomException {
        if (raw)
            throw new DicomException("ApplicationError",
                    "Cannot writeInsert in raw mode");
        if (sequence.isEmpty())
            updateFromImage(md, iioimage);
        if (i < 0)
            sequence.add(iioimage);
        else
            sequence.add(i, iioimage);
    }

    public void endWriteSequence() throws IOException {
        if (sequence.size() > 1)
            md.obj.ds.add(2145386512, sequence);
        else if (sequence.size() == 1)
            md.obj.ds.add(2145386512, sequence.get(0));
        normalizeAndWrite(md);
        sequence = null;
        md = null;
    }

    private DicomMetadata getMD(IIOMetadata iiometadata) {
        return ((DicomMetadata) (iiometadata == null ? getDefaultStreamMetadata(null)
                : convertStreamMetadata(iiometadata, null)));
    }

    public void dispose() {
        super.dispose();
        md = null;
        sequence = null;
    }

    public void setOutput(Object object) {
        super.setOutput(object);
    }

    public void setTransferSyntaxes(String[] strings) {
        preferredTS = (strings == null || strings.length == 0 ? DEFAULT_PREFERRED_TS
                : TransferSyntax.intersectWithSupported(strings));
    }

    public void setTransferSyntax(String string) {
        setTransferSyntaxes(string == null ? null : new String[] { string });
    }

    public void setJPEGQuality(float f) {
        JPEGQuality = f;
    }

    public float getJPEGQuality() {
        return JPEGQuality;
    }

    public void setJPEG2000EncodingRate(double d) {
        JPEG2000EncodingRate = d;
    }

    public double getJPEG2000EncodingRate() {
        return JPEG2000EncodingRate;
    }

}
