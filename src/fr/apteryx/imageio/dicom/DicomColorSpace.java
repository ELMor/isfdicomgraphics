/* DicomColorSpace - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.IndexColorModel;
import java.util.HashMap;

abstract class DicomColorSpace implements Constants
{
    private static HashMap hash = new HashMap();
    private static HashMap h;
    /*synthetic*/ static Class array$Ljava$lang$Number;
    
    static final class YbrRCT extends ColorSpace
    {
	YbrRCT() {
	    super(3, 3);
	}
	
	public float[] fromCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] fromRGB(float[] fs) {
	    float[] fs_0_ = new float[3];
	    fs_0_[0] = (fs[0] + 2.0F * fs[1] + fs[2]) / 4.0F;
	    fs_0_[1] = fs[2] - fs[1];
	    fs_0_[2] = fs[0] - fs[1];
	    clamp3(fs_0_);
	    return fs_0_;
	}
	
	public String getName(int i) {
	    switch (i) {
	    case 0:
		return "Y";
	    case 1:
		return "Cb";
	    default:
		return "Cr";
	    }
	}
	
	public float[] toCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] toRGB(float[] fs) {
	    float[] fs_1_ = new float[3];
	    fs_1_[1] = fs[0] - (fs[1] + fs[2]) / 4.0F;
	    fs_1_[0] = fs[2] + fs_1_[1];
	    fs_1_[2] = fs[1] + fs_1_[1];
	    clamp(fs_1_);
	    return fs_1_;
	}
	
	public float getMinValue(int i) {
	    return i == 0 ? 0.0F : -1.0F;
	}
    }
    
    static final class YbrICT extends ColorSpace
    {
	YbrICT() {
	    super(3, 3);
	}
	
	public float[] fromCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] fromRGB(float[] fs) {
	    float[] fs_2_ = new float[3];
	    fs_2_[0] = 0.299F * fs[0] + 0.587F * fs[1] + 0.114F * fs[2];
	    fs_2_[1] = -0.16875F * fs[0] - 0.33126F * fs[1] + 0.5F * fs[2];
	    fs_2_[2] = 0.5F * fs[0] - 0.41869F * fs[1] - 0.08131F * fs[2];
	    clamp2(fs_2_);
	    return fs_2_;
	}
	
	public String getName(int i) {
	    switch (i) {
	    case 0:
		return "Y";
	    case 1:
		return "Cb";
	    default:
		return "Cr";
	    }
	}
	
	public float[] toCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] toRGB(float[] fs) {
	    float[] fs_3_ = new float[3];
	    fs_3_[0] = fs[0] + 1.402F * fs[2];
	    fs_3_[1] = fs[0] - 0.34413F * fs[1] - 0.71414F * fs[2];
	    fs_3_[2] = fs[0] + 1.772F * fs[1];
	    clamp(fs_3_);
	    return fs_3_;
	}
	
	public float getMinValue(int i) {
	    return i == 0 ? 0.0F : -0.5F;
	}
	
	public float getMaxValue(int i) {
	    return i == 0 ? 1.0F : 0.5F;
	}
    }
    
    static final class YbrPartial extends ColorSpace
    {
	YbrPartial() {
	    super(3, 3);
	}
	
	public float[] fromCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] fromRGB(float[] fs) {
	    float[] fs_4_ = new float[3];
	    fs_4_[0] = (0.0625F + 0.2568F * fs[0] + 0.5041F * fs[1]
			+ 0.0979F * fs[2]);
	    fs_4_[1]
		= 0.5F - 0.1482F * fs[0] - 0.291F * fs[1] + 0.4392F * fs[2];
	    fs_4_[2]
		= 0.5F + 0.4392F * fs[0] - 0.3678F * fs[1] - 0.0714F * fs[2];
	    clamp(fs_4_);
	    return fs_4_;
	}
	
	public String getName(int i) {
	    switch (i) {
	    case 0:
		return "Y";
	    case 1:
		return "Cb";
	    default:
		return "Cr";
	    }
	}
	
	public float[] toCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] toRGB(float[] fs) {
	    float[] fs_5_ = new float[3];
	    fs_5_[0] = (1.16442F * fs[0] - 1.0E-4F * fs[1] + 1.596F * fs[2]
			- 0.8707262F);
	    fs_5_[1] = (1.16442F * fs[0] - 0.39172F * fs[1] - 0.81301F * fs[2]
			+ 0.52959F);
	    fs_5_[2] = (1.16442F * fs[0] + 2.01729F * fs[1] - 1.4E-4F * fs[2]
			- 1.0813513F);
	    clamp(fs_5_);
	    return fs_5_;
	}
    }
    
    static final class YbrFull extends ColorSpace
    {
	YbrFull() {
	    super(3, 3);
	}
	
	public float[] fromCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] fromRGB(float[] fs) {
	    float[] fs_6_ = new float[3];
	    fs_6_[0] = 0.299F * fs[0] + 0.587F * fs[1] + 0.114F * fs[2];
	    fs_6_[1] = 0.5F - 0.1687F * fs[0] - 0.3313F * fs[1] + 0.5F * fs[2];
	    fs_6_[2] = 0.5F + 0.5F * fs[0] - 0.4187F * fs[1] - 0.0813F * fs[2];
	    clamp(fs_6_);
	    return fs_6_;
	}
	
	public String getName(int i) {
	    switch (i) {
	    case 0:
		return "Y";
	    case 1:
		return "Cb";
	    default:
		return "Cr";
	    }
	}
	
	public float[] toCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] toRGB(float[] fs) {
	    float[] fs_7_ = new float[3];
	    fs_7_[0] = fs[0] + 1.40199F * fs[2] - 0.700995F;
	    fs_7_[1] = fs[0] - 0.34411F * fs[1] - 0.7141F * fs[2] + 0.529105F;
	    fs_7_[2] = fs[0] + 1.77198F * fs[1] - 0.88599F;
	    clamp(fs_7_);
	    return fs_7_;
	}
    }
    
    static final class PresentationLUT extends ColorSpace
    {
	private final short[] data;
	private final int out_max;
	private final int in_len;
	
	PresentationLUT(short[] is, int i) {
	    super(6, 1);
	    data = is;
	    out_max = (1 << i) - 1;
	    in_len = is.length;
	}
	
	public float[] fromCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] fromRGB(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public String getName(int i) {
	    return "Gray";
	}
	
	public float[] toCIEXYZ(float[] fs) {
	    throw new RuntimeException("Unsupported");
	}
	
	public float[] toRGB(float[] fs) {
	    int i = (int) (fs[0] * (float) in_len);
	    if (i >= in_len)
		i = in_len - 1;
	    float f = (float) (data[i] & 0xffff) / (float) out_max;
	    if (f > 1.0F)
		f = 1.0F;
	    return new float[] { f, f, f };
	}
    }
    
    static final class Monochrome1 extends ColorSpace
    {
	static final ColorSpace css = getInstance(1003);
	
	Monochrome1() {
	    super(6, 1);
	}
	
	public float[] fromCIEXYZ(float[] fs) {
	    float[] fs_8_ = css.fromCIEXYZ(fs);
	    fs_8_[0] = 1.0F - fs_8_[0];
	    return fs_8_;
	}
	
	public float[] fromRGB(float[] fs) {
	    float f = 3.0F - (fs[0] + fs[1] + fs[2]);
	    return (new float[]
		    { f > 3.0F ? 1.0F : f < 0.0F ? 0.0F : f / 3.0F });
	}
	
	public String getName(int i) {
	    return "Gray";
	}
	
	public float[] toCIEXYZ(float[] fs) {
	    float[] fs_9_ = { 1.0F - fs[0] };
	    return css.toCIEXYZ(fs_9_);
	}
	
	public float[] toRGB(float[] fs) {
	    float f = 1.0F - fs[0];
	    return new float[] { f, f, f };
	}
    }
    
    static String getDicomType(ColorSpace colorspace) throws DicomException {
	switch (colorspace.getType()) {
	case 5:
	    return "RGB";
	case 3:
	    return (colorspace instanceof YbrPartial ? "YBR_PARTIAL_422"
		    : colorspace instanceof YbrICT ? "YBR_ICT"
		    : colorspace instanceof YbrRCT ? "YBR_RCT" : "YBR_FULL");
	case 9:
	    return "CMYK";
	case 7:
	    return "HSV";
	default:
	    throw new DicomException("Unsupported", "Colorspace");
	}
    }
    
    static synchronized String tagToIIOStandard(String string) {
	if (h == null) {
	    h = new HashMap();
	    h.put("MONOCHROME1", "GRAY");
	    h.put("MONOCHROME2", "GRAY");
	    h.put("PALETTE COLOR", "RGB");
	    h.put("RGB", "RGB");
	    h.put("HSV", "HSV");
	    h.put("ARGB", "4CLR");
	    h.put("CMYK", "CMYK");
	    h.put("YBR_FULL", "YCbCr");
	    h.put("YBR_FULL_422", "YCbCr");
	    h.put("YBR_PARTIAL_422", "YCbCr");
	    h.put("YBR_PARTIAL_420", "YCbCr");
	    h.put("YBR_ICT", "YCbCr");
	    h.put("YBR_RCT", "YCbCr");
	}
	return (String) h.get(string);
    }
    
    static synchronized ColorSpace getInstance(String string, int i) {
	if (string == null) {
	    switch (i) {
	    case 1:
		string = "MONOCHROME2";
		break;
	    case 3:
		string = "RGB";
		break;
	    case 4:
		string = "CMYK";
		break;
	    }
	}
	ColorSpace colorspace = (ColorSpace) hash.get(string);
	if (colorspace != null)
	    return colorspace;
	if (string.equals("MONOCHROME2"))
	    colorspace = ColorSpace.getInstance(1003);
	else if (string.equals("MONOCHROME1"))
	    colorspace = new Monochrome1();
	else if (string.equals("YBR_FULL") || string.equals("YBR_FULL_422"))
	    colorspace = new YbrFull();
	else if (string.equals("YBR_PARTIAL_422")
		 || string.equals("YBR_PARTIAL_420"))
	    colorspace = new YbrPartial();
	else if (string.equals("YBR_ICT"))
	    colorspace = new YbrICT();
	else if (string.equals("YBR_RCT"))
	    colorspace = new YbrRCT();
	else if (string.equals("CMYK")) {
	    try {
		colorspace
		    = new ICC_ColorSpace(ICC_Profile.getInstance("CMYK.pf"));
	    } catch (Exception exception) {
		exception.printStackTrace();
	    }
	} else
	    colorspace = ColorSpace.getInstance(1000);
	hash.put(string, colorspace);
	return colorspace;
    }
    
    static ColorSpace getInstanceFromPresentationLUTShape(String string) {
	String string_10_
	    = "INVERSE".equals(string) ? "MONOCHROME1" : "MONOCHROME2";
	return getInstance(string_10_, 1);
    }
    
    static ColorSpace getInstanceFromPresentationLUTSequence(DataSet dataset) {
	Number[] numbers
	    = ((Number[])
	       dataset.findValue(2633730, java.lang.Number.class));
	if (numbers == null || numbers.length != 3)
	    return null;
	int i = numbers[2].intValue();
	short[] is = Util.toShortArray(dataset.findValue(2633734));
	if (is == null)
	    return null;
	return new PresentationLUT(is, i);
    }
    
    private static final void clamp(float[] fs) {
	if (fs[0] < 0.0F)
	    fs[0] = 0.0F;
	if (fs[1] < 0.0F)
	    fs[1] = 0.0F;
	if (fs[2] < 0.0F)
	    fs[2] = 0.0F;
	if (fs[0] > 1.0F)
	    fs[0] = 1.0F;
	if (fs[1] > 1.0F)
	    fs[1] = 1.0F;
	if (fs[2] > 1.0F)
	    fs[2] = 1.0F;
    }
    
    private static final void clamp2(float[] fs) {
	if (fs[0] < 0.0F)
	    fs[0] = 0.0F;
	if (fs[1] < -0.5F)
	    fs[1] = -0.5F;
	if (fs[2] < -0.5F)
	    fs[2] = -0.5F;
	if (fs[0] > 1.0F)
	    fs[0] = 1.0F;
	if (fs[1] > 0.5F)
	    fs[1] = 0.5F;
	if (fs[2] > 0.5F)
	    fs[2] = 0.5F;
    }
    
    private static final void clamp3(float[] fs) {
	if (fs[0] < 0.0F)
	    fs[0] = 0.0F;
	if (fs[1] < -1.0F)
	    fs[1] = -1.0F;
	if (fs[2] < -1.0F)
	    fs[2] = -1.0F;
	if (fs[0] > 1.0F)
	    fs[0] = 1.0F;
	if (fs[1] > 1.0F)
	    fs[1] = 1.0F;
	if (fs[2] > 1.0F)
	    fs[2] = 1.0F;
    }
    
    static IndexColorModel getIndexColorModel
	(int i, Number[][] numbers, short[][] is) throws DicomException {
	int[] is_11_ = new int[3];
	int[] is_12_ = new int[3];
	int[] is_13_ = new int[3];
	byte[][] is_14_ = new byte[3][];
	for (int i_15_ = 0; i_15_ < 3; i_15_++) {
	    if (numbers[i_15_] == null || is[i_15_] == null)
		throw new DicomException("ProtocoleViolation",
					 "Invalid palette data");
	    is_11_[i_15_] = numbers[i_15_][0].shortValue() & 0xffff;
	    if (is_11_[i_15_] == 0)
		is_11_[i_15_] = 65536;
	    is_12_[i_15_] = numbers[i_15_][1].shortValue() & 0xffff;
	    is_13_[i_15_] = numbers[i_15_][2].intValue();
	}
	int i_16_ = 1 << i;
	for (int i_17_ = 0; i_17_ < 3; i_17_++) {
	    short[] is_18_ = is[i_17_];
	    byte[] is_19_ = is_14_[i_17_] = new byte[i_16_];
	    int i_20_ = is_12_[i_17_];
	    if (i_20_ > i_16_)
		i_20_ = i_16_;
	    int i_21_ = is_18_.length;
	    if (i_20_ + i_21_ > i_16_)
		i_21_ = i_16_ - i_20_;
	    if (is_13_[i_17_] <= 8) {
		byte i_22_ = (byte) is_18_[0];
		for (int i_23_ = i_20_ - 1; i_23_ >= 0; i_23_--)
		    is_19_[i_23_] = i_22_;
		for (int i_24_ = i_21_ - 1; i_24_ >= 0; i_24_--)
		    is_19_[i_20_ + i_24_] = (byte) is_18_[i_24_];
		if (i_20_ + i_21_ < i_16_) {
		    byte i_25_ = (byte) is_18_[is_18_.length - 1];
		    for (int i_26_ = i_20_ + i_21_; i_26_ < i_16_; i_26_++)
			is_19_[i_26_] = i_25_;
		}
	    } else {
		byte i_27_ = (byte) (is_18_[0] >> 8);
		for (int i_28_ = i_20_ - 1; i_28_ >= 0; i_28_--)
		    is_19_[i_28_] = i_27_;
		for (int i_29_ = i_21_ - 1; i_29_ >= 0; i_29_--)
		    is_19_[i_20_ + i_29_] = (byte) (is_18_[i_29_] >> 8);
		if (i_20_ + i_21_ < i_16_) {
		    byte i_30_ = (byte) (is_18_[is_18_.length - 1] >> 8);
		    for (int i_31_ = i_20_ + i_21_; i_31_ < i_16_; i_31_++)
			is_19_[i_31_] = i_30_;
		}
	    }
	}
	return new IndexColorModel(i, i_16_, is_14_[0], is_14_[1], is_14_[2]);
    }
    
}
