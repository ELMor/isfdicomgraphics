/* Identifier - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

public class Identifier
{
    public static final String LEVEL_PATIENT = "PATIENT";
    public static final String LEVEL_STUDY = "STUDY";
    public static final String LEVEL_SERIES = "SERIES";
    public static final String LEVEL_IMAGE = "IMAGE";
    public static final int MODEL_PATIENT_ROOT = 0;
    public static final int MODEL_STUDY_ROOT = 1;
    public static final int MODEL_PATIENT_STUDY_ONLY = 2;
    static final int MODEL_NUMBERS = 3;
    static final String[] FIND_UID
	= { "1.2.840.10008.5.1.4.1.2.1.1", "1.2.840.10008.5.1.4.1.2.2.1",
	    "1.2.840.10008.5.1.4.1.2.3.1" };
    static final String[] GET_UID
	= { "1.2.840.10008.5.1.4.1.2.1.3", "1.2.840.10008.5.1.4.1.2.2.3",
	    "1.2.840.10008.5.1.4.1.2.3.3" };
    static final String[] MOVE_UID
	= { "1.2.840.10008.5.1.4.1.2.1.2", "1.2.840.10008.5.1.4.1.2.2.2",
	    "1.2.840.10008.5.1.4.1.2.3.2" };
    final DataSet ds;
    final int model;
    /*synthetic*/ static Class class$java$lang$Object;
    
    public Identifier(int i, String string) {
	this(i, new DataSet());
	setLevel(string);
    }
    
    Identifier(int i, DataSet dataset) {
	model = i;
	ds = dataset;
    }
    
    public void setLevel(String string) {
	ds.add(524370, string);
    }
    
    public String getLevel() {
	return ds.findString(524370);
    }
    
    public int getModel() {
	return model;
    }
    
    public void requestAttribute(int i) {
	ds.add(i, (Object) null);
    }
    
    public void setAttribute(int i, Object object) {
	ds.add(i, object);
    }
    
    public Object getAttribute(int i) {
	return ds.findValue(i, java.lang.Object.class);
    }
    
    public String toString() {
	return ds.toString();
    }
    
    public DataSet getDataSet() {
	return ds;
    }
    
}
