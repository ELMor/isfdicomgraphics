/* JPEGFilterOutputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.IOException;

import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.ImageOutputStreamImpl;

class JPEGFilterOutputStream extends ImageOutputStreamImpl
{
    private int APP0len;
    private boolean APP0present;
    ImageOutputStream os;
    private byte[] header = new byte[6];
    private final byte[] buf1 = new byte[1];
    
    JPEGFilterOutputStream(ImageOutputStream imageoutputstream) {
	os = imageoutputstream;
    }
    
    public void write(byte[] is, int i, int i_0_) throws IOException {
	if (i_0_ != 0) {
	    int i_1_ = i;
	    int i_2_ = i + i_0_;
	    if (streamPos < 6L) {
		for (/**/; i_1_ < i + i_0_ && streamPos < 6L; i_1_++) {
		    header[(int) streamPos] = is[i_1_];
		    streamPos++;
		}
		if (streamPos < 6L)
		    return;
		if (header[0] != -1 || header[1] != -40)
		    throw new IOException
			      ("JPEG stream being written does not start with SOI");
		if (header[2] != -1 || header[3] != -32) {
		    os.write(header, 0, 6);
		    APP0present = false;
		} else {
		    APP0len = (header[4] & 0xff) << 8 | header[5] & 0xff;
		    os.write(header, 0, 2);
		    APP0present = true;
		}
	    }
	    if (APP0present) {
		if (streamPos >= (long) (APP0len + 4))
		    os.write(is, i_1_, i_2_ - i_1_);
		else if (streamPos + (long) i_2_ - (long) i_1_
			 > (long) (APP0len + 4)) {
		    int i_3_ = (int) ((long) i_1_
				      + ((long) (APP0len + 4) - streamPos));
		    os.write(is, i_3_, i_2_ - i_3_);
		}
	    } else
		os.write(is, i_1_, i_2_ - i_1_);
	    streamPos += (long) (i_2_ - i_1_);
	}
    }
    
    public void write(int i) throws IOException {
	buf1[0] = (byte) i;
	write(buf1, 0, 1);
    }
    
    public void seek(long l) {
	throw new Error("Unsupported seek");
    }
    
    public int read(byte[] is, int i, int i_4_) {
	throw new Error("Unsupported read");
    }
    
    public int read() {
	throw new Error("Unsupported read");
    }
}
