/* PeerAE - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.net.InetAddress;

public class PeerAE
{
    InetAddress addr;
    int port;
    String callingAE;
    String calledAE;
    
    public PeerAE(InetAddress inetaddress, String string, String string_0_) {
	this(inetaddress, 104, string, string_0_);
    }
    
    public PeerAE(InetAddress inetaddress, String string) {
	this(inetaddress, 104, Plugin.getApplicationTitle(), string);
    }
    
    public PeerAE(InetAddress inetaddress, int i, String string) {
	this(inetaddress, i, Plugin.getApplicationTitle(), string);
    }
    
    public PeerAE(InetAddress inetaddress, int i, String string,
		  String string_1_) {
	addr = inetaddress;
	port = i;
	callingAE = string;
	calledAE = string_1_;
    }
}
