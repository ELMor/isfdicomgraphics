/* RawWriter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.GlyphVector;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.ComponentSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;

import javax.imageio.IIOImage;
import javax.imageio.stream.ImageOutputStream;

abstract class RawWriter
{
    static int writeRaw(ImageOutputStream imageoutputstream, IIOImage iioimage,
			boolean bool, int i) throws IOException {
	Raster raster;
	int i_0_;
	int i_1_;
	if (!iioimage.hasRaster()) {
	    RenderedImage renderedimage = iioimage.getRenderedImage();
	    raster = renderedimage.getData();
	    i_0_ = renderedimage.getWidth();
	    i_1_ = renderedimage.getHeight();
	} else {
	    raster = iioimage.getRaster();
	    i_0_ = raster.getWidth();
	    i_1_ = raster.getHeight();
	}
	if (!(raster.getSampleModel() instanceof ComponentSampleModel))
	    throw new DicomException("Unsupported",
				     ("SampleModel "
				      + raster.getSampleModel().getClass()));
	WritableRaster writableraster
	    = raster.createCompatibleWritableRaster();
	writableraster.setRect(raster);
	BufferedImage bufferedimage;
	if (!iioimage.hasRaster()
	    && iioimage.getRenderedImage().getColorModel() != null)
	    bufferedimage = new BufferedImage(iioimage.getRenderedImage()
						  .getColorModel(),
					      writableraster, false, null);
	else {
	    ComponentColorModel componentcolormodel
		= new ComponentColorModel(DicomColorSpace.getInstance(null, 1),
					  false, false, 0,
					  writableraster.getSampleModel()
					      .getTransferType());
	    bufferedimage = new BufferedImage(componentcolormodel,
					      writableraster, false, null);
	}
	Graphics2D graphics2d = bufferedimage.createGraphics();
	String string = new String(new char[] { 'D' });
	string += 'E';
	string += "MO";
	Font font
	    = new Font("SansSerif", 0, Math.min(bufferedimage.getHeight(),
						bufferedimage.getWidth()) / 3);
	int i_2_ = graphics2d.getFontMetrics(font).stringWidth(string);
	int i_3_ = graphics2d.getFontMetrics(font).getAscent();
	GlyphVector glyphvector
	    = font.createGlyphVector(graphics2d.getFontRenderContext(),
				     string);
	int i_4_ = (bufferedimage.getWidth() - i_2_) / 2;
	int i_5_ = (bufferedimage.getHeight()
		    - (bufferedimage.getHeight() - i_3_) / 2);
	Rectangle rectangle
	    = glyphvector.getPixelBounds(graphics2d.getFontRenderContext(),
					 (float) i_4_, (float) i_5_);
	graphics2d.setColor(Color.BLACK);
	graphics2d.fillRect(0, rectangle.y - rectangle.height / 10,
			    bufferedimage.getWidth(),
			    rectangle.height + rectangle.height / 5);
	graphics2d.setColor(Color.WHITE);
	graphics2d.drawGlyphVector(glyphvector, (float) i_4_, (float) i_5_);
	WritableRaster writableraster_6_ = writableraster;
	ComponentSampleModel componentsamplemodel
	    = (ComponentSampleModel) writableraster_6_.getSampleModel();
	int i_7_ = componentsamplemodel.getPixelStride();
	int i_8_ = componentsamplemodel.getScanlineStride();
	int[] is = componentsamplemodel.getBankIndices();
	if (bool) {
	    for (int i_9_ = 0; i_9_ < writableraster_6_.getNumBands();
		 i_9_++) {
		i_2_ = (componentsamplemodel.getOffset
			((writableraster_6_.getMinX()
			  - writableraster_6_.getSampleModelTranslateX()),
			 (writableraster_6_.getMinY()
			  - writableraster_6_.getSampleModelTranslateY()),
			 i_9_));
		switch (componentsamplemodel.getDataType()) {
		case 0: {
		    DataBufferByte databufferbyte
			= (DataBufferByte) writableraster_6_.getDataBuffer();
		    byte[] is_10_ = databufferbyte.getData(is[i_9_]);
		    if (i_8_ == i_0_ && i_7_ == 1)
			imageoutputstream.write(is_10_, i_2_, i_0_ * i_1_);
		    else if (i_7_ == 1) {
			for (i_4_ = 0; i_4_ < i_1_; i_4_++) {
			    imageoutputstream.write(is_10_, i_2_, i_0_);
			    i_2_ += i_8_;
			}
		    } else {
			byte[] is_11_ = new byte[i_0_];
			for (i_5_ = 0; i_5_ < i_1_; i_5_++) {
			    int i_12_ = i_2_;
			    for (int i_13_ = 0; i_13_ < i_0_; i_13_++) {
				is_11_[i_13_] = is_10_[i_12_];
				i_12_ += i_7_;
			    }
			    imageoutputstream.write(is_11_, 0, i_0_);
			    i_2_ += i_8_;
			}
		    }
		    break;
		}
		case 1:
		case 2: {
		    DataBuffer databuffer = writableraster_6_.getDataBuffer();
		    short[] is_14_
			= (componentsamplemodel.getDataType() == 2
			   ? ((DataBufferShort) databuffer).getData(is[i_9_])
			   : ((DataBufferUShort) databuffer)
				 .getData(is[i_9_]));
		    if (i_8_ == i_0_ && i_7_ == 1)
			imageoutputstream.writeShorts(is_14_, i_2_,
						      i_0_ * i_1_);
		    else if (i_7_ == 1) {
			for (i_4_ = 0; i_4_ < i_1_; i_4_++) {
			    imageoutputstream.writeShorts(is_14_, i_2_, i_0_);
			    i_2_ += i_8_;
			}
		    } else {
			short[] is_15_ = new short[i_0_];
			for (i_5_ = 0; i_5_ < i_1_; i_5_++) {
			    int i_16_ = i_2_;
			    for (int i_17_ = 0; i_17_ < i_0_; i_17_++) {
				is_15_[i_17_] = is_14_[i_16_];
				i_16_ += i_7_;
			    }
			    imageoutputstream.writeShorts(is_15_, 0, i_0_);
			    i_2_ += i_8_;
			}
		    }
		    break;
		}
		}
	    }
	} else {
	    int i_18_ = writableraster_6_.getNumBands();
	    int[] is_19_ = new int[i_18_];
	    for (i_3_ = 0; i_3_ < i_18_; i_3_++)
		is_19_[i_3_]
		    = (componentsamplemodel.getOffset
		       ((writableraster_6_.getMinX()
			 - writableraster_6_.getSampleModelTranslateX()),
			(writableraster_6_.getMinY()
			 - writableraster_6_.getSampleModelTranslateY()),
			i_3_));
	    switch (i) {
	    case 0: {
		boolean bool_20_ = true;
		for (int i_21_ = 1; i_21_ < i_18_; i_21_++) {
		    if (is_19_[i_21_] != is_19_[i_21_ - 1] + 1) {
			bool_20_ = false;
			break;
		    }
		}
		switch (componentsamplemodel.getDataType()) {
		case 0: {
		    DataBufferByte databufferbyte
			= (DataBufferByte) writableraster_6_.getDataBuffer();
		    byte[][] is_22_ = databufferbyte.getBankData();
		    if (databufferbyte.getNumBanks() == 1 && bool_20_
			&& i_8_ == i_0_ * i_18_ && i_7_ == i_18_)
			imageoutputstream.write(is_22_[0], is_19_[0],
						i_0_ * i_1_ * i_18_);
		    else if (databufferbyte.getNumBanks() == 1 && bool_20_
			     && i_7_ == i_18_) {
			for (i_5_ = 0; i_5_ < i_1_; i_5_++) {
			    imageoutputstream.write(is_22_[0], is_19_[0],
						    i_0_ * i_18_);
			    is_19_[0] += i_8_;
			}
		    } else {
			byte[] is_23_ = new byte[i_0_ * i_18_];
			for (int i_24_ = 0; i_24_ < i_1_; i_24_++) {
			    for (int i_25_ = 0; i_25_ < i_18_; i_25_++) {
				int i_26_ = is_19_[i_25_];
				byte[] is_27_ = is_22_[is[i_25_]];
				for (int i_28_ = 0; i_28_ < i_0_; i_28_++) {
				    is_23_[i_28_ * i_18_ + i_25_]
					= is_27_[i_26_];
				    i_26_ += i_7_;
				}
				is_19_[i_25_] += i_8_;
			    }
			    imageoutputstream.write(is_23_, 0, i_0_ * i_18_);
			}
		    }
		    break;
		}
		case 1:
		case 2: {
		    DataBuffer databuffer = writableraster_6_.getDataBuffer();
		    short[][] is_29_
			= (componentsamplemodel.getDataType() == 2
			   ? ((DataBufferShort) databuffer).getBankData()
			   : ((DataBufferUShort) databuffer).getBankData());
		    if (databuffer.getNumBanks() == 1 && bool_20_
			&& i_8_ == i_0_ * i_18_ && i_7_ == i_18_)
			imageoutputstream.writeShorts(is_29_[0], is_19_[0],
						      i_0_ * i_1_ * i_18_);
		    else if (databuffer.getNumBanks() == 1 && bool_20_
			     && i_7_ == i_18_) {
			for (i_5_ = 0; i_5_ < i_1_; i_5_++) {
			    imageoutputstream.writeShorts(is_29_[0], is_19_[0],
							  i_0_ * i_18_);
			    is_19_[0] += i_8_;
			}
		    } else {
			short[] is_30_ = new short[i_0_ * i_18_];
			for (int i_31_ = 0; i_31_ < i_1_; i_31_++) {
			    for (int i_32_ = 0; i_32_ < i_18_; i_32_++) {
				int i_33_ = is_19_[i_32_];
				short[] is_34_ = is_29_[is[i_32_]];
				for (int i_35_ = 0; i_35_ < i_0_; i_35_++) {
				    is_30_[i_35_ * i_18_ + i_32_]
					= is_34_[i_33_];
				    i_33_ += i_7_;
				}
				is_19_[i_32_] += i_8_;
			    }
			    imageoutputstream.writeShorts(is_30_, 0,
							  i_0_ * i_18_);
			}
		    }
		    break;
		}
		}
		break;
	    }
	    case 1:
		if ((i_0_ & 0x1) == 1)
		    throw new DicomException
			      ("ProtocoleViolation",
			       "Image width must be even when using chrominance-subsampled photometric interpretation");
		switch (componentsamplemodel.getDataType()) {
		case 0: {
		    DataBufferByte databufferbyte
			= (DataBufferByte) writableraster_6_.getDataBuffer();
		    byte[][] is_36_ = databufferbyte.getBankData();
		    byte[] is_37_ = new byte[i_0_ * (1 + (i_18_ - 1) / 2)];
		    for (i_5_ = 0; i_5_ < i_1_; i_5_++) {
			int i_38_ = 0;
			int[] is_39_ = new int[i_18_];
			for (int i_40_ = 0; i_40_ < i_18_; i_40_++)
			    is_39_[i_40_] = is_19_[i_40_];
			for (int i_41_ = 0; i_41_ < i_0_; i_41_ += 2) {
			    is_37_[i_38_++] = is_36_[is[0]][is_39_[0]];
			    is_37_[i_38_++] = is_36_[is[0]][is_39_[0] + i_7_];
			    is_39_[0] += 2 * i_7_;
			    for (int i_42_ = 1; i_42_ < i_18_; i_42_++) {
				is_37_[i_38_++]
				    = is_36_[is[i_42_]][is_39_[i_42_]];
				is_39_[i_42_] += 2 * i_7_;
			    }
			}
			for (int i_43_ = 0; i_43_ < i_18_; i_43_++)
			    is_19_[i_43_] += i_8_;
			imageoutputstream.write(is_37_);
		    }
		    break;
		}
		case 1:
		case 2: {
		    DataBuffer databuffer = writableraster_6_.getDataBuffer();
		    short[][] is_44_
			= (componentsamplemodel.getDataType() == 2
			   ? ((DataBufferShort) databuffer).getBankData()
			   : ((DataBufferUShort) databuffer).getBankData());
		    short[] is_45_ = new short[i_0_ * (1 + (i_18_ - 1) / 2)];
		    for (i_5_ = 0; i_5_ < i_1_; i_5_++) {
			int i_46_ = 0;
			int[] is_47_ = new int[i_18_];
			for (int i_48_ = 0; i_48_ < i_18_; i_48_++)
			    is_47_[i_48_] = is_19_[i_48_];
			for (int i_49_ = 0; i_49_ < i_0_; i_49_++) {
			    is_45_[i_46_++] = is_44_[is[0]][is_47_[0]];
			    is_45_[i_46_++] = is_44_[is[0]][is_47_[0] + i_7_];
			    is_47_[0] += 2 * i_7_;
			    for (int i_50_ = 1; i_50_ < i_18_; i_50_++) {
				is_45_[i_46_++]
				    = is_44_[is[i_50_]][is_47_[i_50_]];
				is_47_[i_50_] += 2 * i_7_;
			    }
			}
			for (int i_51_ = 0; i_51_ < i_18_; i_51_++)
			    is_19_[i_51_] += i_8_;
			imageoutputstream.writeShorts(is_45_, 0,
						      is_45_.length);
		    }
		    break;
		}
		}
		break;
	    case 2:
		if ((i_0_ & 0x1) == 1)
		    throw new DicomException
			      ("ProtocoleViolation",
			       "Image width must be even when using chrominance-subsampled photometric interpretation");
		switch (componentsamplemodel.getDataType()) {
		case 0: {
		    DataBufferByte databufferbyte
			= (DataBufferByte) writableraster_6_.getDataBuffer();
		    byte[][] is_52_ = databufferbyte.getBankData();
		    byte[] is_53_ = new byte[i_0_ * (1 + (i_18_ - 1) / 2)];
		    for (i_5_ = 0; i_5_ < i_1_; i_5_ += 2) {
			int i_54_ = 0;
			int[] is_55_ = new int[i_18_];
			for (int i_56_ = 0; i_56_ < i_18_; i_56_++)
			    is_55_[i_56_] = is_19_[i_56_];
			for (int i_57_ = 0; i_57_ < i_0_; i_57_ += 2) {
			    is_53_[i_54_++] = is_52_[is[0]][is_55_[0]];
			    is_53_[i_54_++] = is_52_[is[0]][is_55_[0] + i_7_];
			    is_55_[0] += 2 * i_7_;
			    for (int i_58_ = 1; i_58_ < i_18_; i_58_++) {
				is_53_[i_54_++]
				    = is_52_[is[i_58_]][is_55_[i_58_]];
				is_55_[i_58_] += 2 * i_7_;
			    }
			}
			for (int i_59_ = 0; i_59_ < i_18_; i_59_++)
			    is_19_[i_59_] += i_8_;
			imageoutputstream.write(is_53_);
			if (i_5_ == i_1_ - 1)
			    break;
			i_54_ = 0;
			is_55_[0] = is_19_[0];
			for (int i_60_ = 0; i_60_ < i_0_; i_60_++) {
			    is_53_[i_54_++] = is_52_[is[0]][is_55_[0]];
			    is_55_[0] += i_7_;
			}
			imageoutputstream.write(is_53_, 0, i_0_);
		    }
		    break;
		}
		case 1:
		case 2: {
		    DataBuffer databuffer = writableraster_6_.getDataBuffer();
		    short[][] is_61_
			= (componentsamplemodel.getDataType() == 2
			   ? ((DataBufferShort) databuffer).getBankData()
			   : ((DataBufferUShort) databuffer).getBankData());
		    short[] is_62_ = new short[i_0_ * (1 + (i_18_ - 1) / 2)];
		    for (i_5_ = 0; i_5_ < i_1_; i_5_ += 2) {
			int i_63_ = 0;
			int[] is_64_ = new int[i_18_];
			for (int i_65_ = 0; i_65_ < i_18_; i_65_++)
			    is_64_[i_65_] = is_19_[i_65_];
			for (int i_66_ = 0; i_66_ < i_0_; i_66_++) {
			    is_62_[i_63_++] = is_61_[is[0]][is_64_[0]];
			    is_62_[i_63_++] = is_61_[is[0]][is_64_[0] + i_7_];
			    is_64_[0] += 2 * i_7_;
			    for (int i_67_ = 1; i_67_ < i_18_; i_67_++) {
				is_62_[i_63_++]
				    = is_61_[is[i_67_]][is_64_[i_67_]];
				is_64_[i_67_] += 2 * i_7_;
			    }
			}
			for (int i_68_ = 0; i_68_ < i_18_; i_68_++)
			    is_19_[i_68_] += i_8_;
			imageoutputstream.writeShorts(is_62_, 0,
						      is_62_.length);
			if (i_5_ == i_1_ - 1)
			    break;
			i_63_ = 0;
			is_64_[0] = is_19_[0];
			for (int i_69_ = 0; i_69_ < i_0_; i_69_++) {
			    is_62_[i_63_++] = is_61_[is[0]][is_64_[0]];
			    is_64_[0] += i_7_;
			}
			imageoutputstream.writeShorts(is_62_, 0, i_0_);
		    }
		    break;
		}
		default:
		    break;
		}
		break;
	    }
	}
	if (i > 0)
	    return (componentsamplemodel.getDataType() == 0
		    ? ((1 + (writableraster_6_.getNumBands() - 1) / (2 * i))
		       * i_0_ * i_1_)
		    : ((1 + (writableraster_6_.getNumBands() - 1) / (2 * i))
		       * i_0_ * i_1_ * 2));
	return (componentsamplemodel.getDataType() == 0
		? writableraster_6_.getNumBands() * i_0_ * i_1_
		: writableraster_6_.getNumBands() * i_0_ * i_1_ * 2);
    }
}
