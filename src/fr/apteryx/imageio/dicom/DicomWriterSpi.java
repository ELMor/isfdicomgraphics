/*
 * DicomWriterSpi - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

import java.awt.image.SampleModel;
import java.util.Locale;

import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriter;
import javax.imageio.spi.ImageWriterSpi;

public final class DicomWriterSpi extends ImageWriterSpi {

    public DicomWriterSpi() {
        vendorName = "Apteryx";
        version = "1.08pre";
        names = new String[] { "dicom" };
        MIMETypes = new String[] { "application/dicom" };
        nativeStreamMetadataFormatClassName = "DicomStreamMetadataFormat";
        nativeStreamMetadataFormatName = "fr_apteryx_imageio_dicom_1.0";
        pluginClassName = "fr.apteryx.imageio.dicom.DicomWriter";
        suffixes = new String[] { "dcm" };
        supportsStandardStreamMetadataFormat = true;
        outputTypes = (new Class[] {
                javax.imageio.stream.ImageOutputStream.class,
                PeerAE.class,

        });
        readerSpiNames = new String[] { "fr.apteryx.imageio.dicom.DicomReaderSpi" };
    }

    public String getDescription(Locale locale) {
        return "DICOM V3.0 writer";
    }

    public boolean canEncodeImage(ImageTypeSpecifier imagetypespecifier) {
        SampleModel samplemodel = imagetypespecifier.getSampleModel();
        int i = samplemodel.getNumBands();
        if (i != 1 && i != 3 && i != 4)
            return false;
        i = samplemodel.getDataType();
        if (i != 0 && i != 2 && i != 1)
            return false;
        return true;
    }

    public ImageWriter createWriterInstance(Object object) {
        return new DicomWriter(this);
    }

}
