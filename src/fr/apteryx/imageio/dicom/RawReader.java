/* RawReader - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.io.IOException;
import java.nio.ByteOrder;

import javax.imageio.stream.ImageInputStream;

abstract class RawReader
{
    static void readRaw
	(ImageInputStream imageinputstream, Raster raster, boolean bool, int i,
	 int i_0_, int i_1_, boolean bool_2_, boolean bool_3_, int i_4_)
	throws IOException {
	DataBuffer databuffer = raster.getDataBuffer();
	int i_5_ = databuffer.getSize();
	int i_6_ = (1 << i_0_) - 1;
	int i_7_ = 0xffffffff ^ i_6_ >> 1;
	imageinputstream.setByteOrder(bool ? ByteOrder.BIG_ENDIAN
				      : ByteOrder.LITTLE_ENDIAN);
	if (i_4_ == 2) {
	    int i_8_ = raster.getWidth();
	    int i_9_ = raster.getHeight();
	    int i_10_ = 0;
	    int i_11_ = 0;
	    if (i == 8) {
		byte[] is = new byte[(i_9_ / 2 + (i_9_ + 1) / 2 * 2) * i_8_];
		imageinputstream.readFully(is, 0, is.length);
		for (int i_12_ = 0; i_12_ < i_9_; i_12_ += 2) {
		    for (int i_13_ = 0; i_13_ < i_8_; i_13_ += 2) {
			databuffer.setElem(0, i_11_, is[i_10_] & 0xff);
			databuffer.setElem(0, i_11_ + 3, is[i_10_ + 1] & 0xff);
			databuffer.setElem(0, i_11_ + 1, is[i_10_ + 2] & 0xff);
			databuffer.setElem(0, i_11_ + 4, is[i_10_ + 2] & 0xff);
			databuffer.setElem(0, i_11_ + 2, is[i_10_ + 3] & 0xff);
			databuffer.setElem(0, i_11_ + 5, is[i_10_ + 3] & 0xff);
			i_11_ += 6;
			i_10_ += 4;
		    }
		    if (i_12_ == i_9_ - 1)
			break;
		    for (int i_14_ = 0; i_14_ < i_8_; i_14_ += 2) {
			int i_15_ = i_10_ - i_14_ - (i_8_ - i_14_) * 2;
			databuffer.setElem(0, i_11_, is[i_10_] & 0xff);
			databuffer.setElem(0, i_11_ + 3, is[i_10_ + 1] & 0xff);
			databuffer.setElem(0, i_11_ + 1, is[i_15_ + 2] & 0xff);
			databuffer.setElem(0, i_11_ + 4, is[i_15_ + 2] & 0xff);
			databuffer.setElem(0, i_11_ + 2, is[i_15_ + 3] & 0xff);
			databuffer.setElem(0, i_11_ + 5, is[i_15_ + 3] & 0xff);
			i_11_ += 6;
			i_10_ += 2;
		    }
		}
	    } else {
		short[] is = new short[(i_9_ / 2 + (i_9_ + 1) / 2 * 2) * i_8_];
		imageinputstream.readFully(is, 0, is.length);
		for (int i_16_ = 0; i_16_ < i_9_; i_16_ += 2) {
		    for (int i_17_ = 0; i_17_ < i_8_; i_17_ += 2) {
			databuffer.setElem(0, i_11_, is[i_10_] & 0xffff);
			databuffer.setElem(0, i_11_ + 3,
					   is[i_10_ + 1] & 0xffff);
			databuffer.setElem(0, i_11_ + 1,
					   is[i_10_ + 2] & 0xffff);
			databuffer.setElem(0, i_11_ + 4,
					   is[i_10_ + 2] & 0xffff);
			databuffer.setElem(0, i_11_ + 2,
					   is[i_10_ + 3] & 0xffff);
			databuffer.setElem(0, i_11_ + 5,
					   is[i_10_ + 3] & 0xffff);
			i_11_ += 6;
			i_10_ += 4;
		    }
		    if (i_16_ == i_9_ - 1)
			break;
		    for (int i_18_ = 0; i_18_ < i_8_; i_18_ += 2) {
			int i_19_ = i_10_ - i_18_ - (i_8_ - i_18_) * 2;
			databuffer.setElem(0, i_11_, is[i_10_] & 0xffff);
			databuffer.setElem(0, i_11_ + 3,
					   is[i_10_ + 1] & 0xffff);
			databuffer.setElem(0, i_11_ + 1,
					   is[i_19_ + 2] & 0xffff);
			databuffer.setElem(0, i_11_ + 4,
					   is[i_19_ + 2] & 0xffff);
			databuffer.setElem(0, i_11_ + 2,
					   is[i_19_ + 3] & 0xffff);
			databuffer.setElem(0, i_11_ + 5,
					   is[i_19_ + 3] & 0xffff);
			i_11_ += 6;
			i_10_ += 2;
		    }
		}
	    }
	} else if (i_4_ == 1) {
	    int i_20_ = 0;
	    if (i == 8) {
		byte[] is = new byte[2 * i_5_ / 3];
		imageinputstream.readFully(is, 0, is.length);
		for (int i_21_ = 0; i_21_ < i_5_; i_21_ += 6) {
		    databuffer.setElem(0, i_21_, is[i_20_] & 0xff);
		    databuffer.setElem(0, i_21_ + 3, is[i_20_ + 1] & 0xff);
		    databuffer.setElem(0, i_21_ + 1, is[i_20_ + 2] & 0xff);
		    databuffer.setElem(0, i_21_ + 4, is[i_20_ + 2] & 0xff);
		    databuffer.setElem(0, i_21_ + 2, is[i_20_ + 3] & 0xff);
		    databuffer.setElem(0, i_21_ + 5, is[i_20_ + 3] & 0xff);
		    i_20_ += 4;
		}
	    } else {
		short[] is = new short[2 * i_5_ / 3];
		imageinputstream.readFully(is, 0, is.length);
		for (int i_22_ = 0; i_22_ < i_5_; i_22_ += 6) {
		    databuffer.setElem(0, i_22_, is[i_20_] & 0xffff);
		    databuffer.setElem(0, i_22_ + 3, is[i_20_ + 1] & 0xffff);
		    databuffer.setElem(0, i_22_ + 1, is[i_20_ + 2] & 0xffff);
		    databuffer.setElem(0, i_22_ + 4, is[i_20_ + 2] & 0xffff);
		    databuffer.setElem(0, i_22_ + 2, is[i_20_ + 3] & 0xffff);
		    databuffer.setElem(0, i_22_ + 5, is[i_20_ + 3] & 0xffff);
		    i_20_ += 4;
		}
	    }
	} else if (i_1_ <= 0) {
	    if (i == 8) {
		if (!bool_2_ && i_0_ == 8
		    && databuffer instanceof DataBufferByte) {
		    for (int i_23_ = 0; i_23_ < databuffer.getNumBanks();
			 i_23_++)
			imageinputstream.readFully(((DataBufferByte)
						    databuffer)
						       .getData(i_23_),
						   0, i_5_);
		} else if (bool_3_) {
		    if (bool_2_ && bool) {
			int i_24_ = i_5_ * databuffer.getNumBanks() / 2;
			short[] is = new short[i_24_];
			int i_25_ = 0;
			int i_26_ = 0;
			imageinputstream.readFully(is, 0, i_24_);
			for (int i_27_ = 0; i_27_ < i_24_; i_27_++) {
			    int i_28_ = is[i_27_];
			    int i_29_ = i_28_ & i_6_;
			    if ((i_29_ & i_7_) != 0)
				i_29_ |= i_7_;
			    databuffer.setElem(i_25_, i_26_++, i_29_);
			    if (i_26_ >= i_5_) {
				i_26_ = 0;
				i_25_++;
			    }
			    i_29_ = i_28_ >> 8 & i_6_;
			    if ((i_29_ & i_7_) != 0)
				i_29_ |= i_7_;
			    databuffer.setElem(i_25_, i_26_++, i_29_);
			    if (i_26_ >= i_5_) {
				i_26_ = 0;
				i_25_++;
			    }
			}
			if (i_26_ > 0) {
			    int i_30_ = imageinputstream.readShort() & i_6_;
			    if ((i_30_ & i_7_) != 0)
				i_30_ |= i_7_;
			    databuffer.setElem(i_25_, i_26_, i_30_);
			}
		    } else {
			byte[] is = new byte[i_5_];
			for (int i_31_ = 0; i_31_ < databuffer.getNumBanks();
			     i_31_++) {
			    imageinputstream.readFully(is, 0, i_5_);
			    for (int i_32_ = 0; i_32_ < i_5_; i_32_++) {
				int i_33_ = is[i_32_] & i_6_;
				if ((i_33_ & i_7_) != 0)
				    i_33_ |= i_7_;
				databuffer.setElem(i_31_, i_32_, i_33_);
			    }
			}
		    }
		} else if (bool_2_ && bool) {
		    int i_34_ = i_5_ * databuffer.getNumBanks() / 2;
		    int i_35_ = 0;
		    int i_36_ = 0;
		    short[] is = new short[i_34_];
		    imageinputstream.readFully(is, 0, i_34_);
		    for (int i_37_ = 0; i_37_ < i_34_; i_37_++) {
			int i_38_ = is[i_37_];
			databuffer.setElem(i_35_, i_36_++, i_38_ & i_6_);
			if (i_36_ >= i_5_) {
			    i_36_ = 0;
			    i_35_++;
			}
			databuffer.setElem(i_35_, i_36_++, i_38_ >> 8 & i_6_);
			if (i_36_ >= i_5_) {
			    i_36_ = 0;
			    i_35_++;
			}
		    }
		    if (i_36_ > 0) {
			int i_39_ = imageinputstream.readShort();
			databuffer.setElem(i_35_, i_36_, i_39_ & i_6_);
		    }
		} else {
		    byte[] is = new byte[i_5_];
		    for (int i_40_ = 0; i_40_ < databuffer.getNumBanks();
			 i_40_++) {
			imageinputstream.readFully(is, 0, i_5_);
			for (int i_41_ = 0; i_41_ < i_5_; i_41_++)
			    databuffer.setElem(i_40_, i_41_, is[i_41_] & i_6_);
		    }
		}
	    } else if (i_0_ == 16 && databuffer instanceof DataBufferShort) {
		for (int i_42_ = 0; i_42_ < databuffer.getNumBanks(); i_42_++)
		    imageinputstream.readFully(((DataBufferShort) databuffer)
						   .getData(i_42_),
					       0, i_5_);
	    } else if (i_0_ == 16 && databuffer instanceof DataBufferUShort) {
		for (int i_43_ = 0; i_43_ < databuffer.getNumBanks(); i_43_++)
		    imageinputstream.readFully(((DataBufferUShort) databuffer)
						   .getData(i_43_),
					       0, i_5_);
	    } else {
		short[] is = new short[i_5_];
		if (bool_3_) {
		    for (int i_44_ = 0; i_44_ < databuffer.getNumBanks();
			 i_44_++) {
			imageinputstream.readFully(is, 0, i_5_);
			for (int i_45_ = 0; i_45_ < i_5_; i_45_++) {
			    int i_46_ = is[i_45_] & i_6_;
			    if ((i_46_ & i_7_) != 0)
				i_46_ |= i_7_;
			    databuffer.setElem(i_44_, i_45_, i_46_);
			}
		    }
		} else {
		    for (int i_47_ = 0; i_47_ < databuffer.getNumBanks();
			 i_47_++) {
			imageinputstream.readFully(is, 0, i_5_);
			for (int i_48_ = 0; i_48_ < i_5_; i_48_++)
			    databuffer.setElem(i_47_, i_48_, is[i_48_] & i_6_);
		    }
		}
	    }
	} else if (i == 8) {
	    if (bool_3_) {
		if (bool_2_ && bool) {
		    int i_49_ = i_5_ * databuffer.getNumBanks() / 2;
		    int i_50_ = 0;
		    int i_51_ = 0;
		    short[] is = new short[i_49_];
		    imageinputstream.readFully(is, 0, i_49_);
		    for (int i_52_ = 0; i_52_ < i_49_; i_52_++) {
			int i_53_ = is[i_52_];
			int i_54_ = i_53_ >> i_1_ & i_6_;
			if ((i_54_ & i_7_) != 0)
			    i_54_ |= i_7_;
			databuffer.setElem(i_50_, i_51_++, i_54_);
			if (i_51_ >= i_5_) {
			    i_51_ = 0;
			    i_50_++;
			}
			i_54_ = i_53_ >> 8 + i_1_ & i_6_;
			if ((i_54_ & i_7_) != 0)
			    i_54_ |= i_7_;
			databuffer.setElem(i_50_, i_51_++, i_54_);
			if (i_51_ >= i_5_) {
			    i_51_ = 0;
			    i_50_++;
			}
		    }
		    if (i_51_ > 0) {
			int i_55_
			    = imageinputstream.readShort() >> i_1_ & i_6_;
			if ((i_55_ & i_7_) != 0)
			    i_55_ |= i_7_;
			databuffer.setElem(i_50_, i_51_, i_55_);
		    }
		} else {
		    byte[] is = new byte[i_5_];
		    for (int i_56_ = 0; i_56_ < databuffer.getNumBanks();
			 i_56_++) {
			imageinputstream.readFully(is, 0, i_5_);
			for (int i_57_ = 0; i_57_ < i_5_; i_57_++) {
			    int i_58_ = is[i_57_] >> i_1_ & i_6_;
			    if ((i_58_ & i_7_) != 0)
				i_58_ |= i_7_;
			    databuffer.setElem(i_56_, i_57_, i_58_);
			}
		    }
		}
	    } else if (bool_2_ && bool) {
		int i_59_ = i_5_ * databuffer.getNumBanks() / 2;
		int i_60_ = 0;
		int i_61_ = 0;
		short[] is = new short[i_59_];
		imageinputstream.readFully(is, 0, i_59_);
		for (int i_62_ = 0; i_62_ < i_59_; i_62_++) {
		    int i_63_ = is[i_62_];
		    databuffer.setElem(i_60_, i_61_++, i_63_ >> i_1_ & i_6_);
		    if (i_61_ >= i_5_) {
			i_61_ = 0;
			i_60_++;
		    }
		    databuffer.setElem(i_60_, i_61_++,
				       i_63_ >> 8 + i_1_ & i_6_);
		    if (i_61_ >= i_5_) {
			i_61_ = 0;
			i_60_++;
		    }
		}
		if (i_61_ > 0)
		    databuffer.setElem(i_60_, i_61_,
				       (imageinputstream.readShort() >> i_1_
					& i_6_));
	    } else {
		byte[] is = new byte[i_5_];
		for (int i_64_ = 0; i_64_ < databuffer.getNumBanks();
		     i_64_++) {
		    imageinputstream.readFully(is, 0, i_5_);
		    for (int i_65_ = 0; i_65_ < i_5_; i_65_++)
			databuffer.setElem(i_64_, i_65_,
					   is[i_65_] >> i_1_ & i_6_);
		}
	    }
	} else if (bool_3_) {
	    short[] is = new short[i_5_];
	    for (int i_66_ = 0; i_66_ < databuffer.getNumBanks(); i_66_++) {
		imageinputstream.readFully(is, 0, i_5_);
		for (int i_67_ = 0; i_67_ < i_5_; i_67_++) {
		    int i_68_ = is[i_67_] >> i_1_ & i_6_;
		    if ((i_68_ & i_7_) != 0)
			i_68_ |= i_7_;
		    databuffer.setElem(i_66_, i_67_, i_68_);
		}
	    }
	} else {
	    short[] is = new short[i_5_];
	    for (int i_69_ = 0; i_69_ < databuffer.getNumBanks(); i_69_++) {
		imageinputstream.readFully(is, 0, i_5_);
		for (int i_70_ = 0; i_70_ < i_5_; i_70_++)
		    databuffer.setElem(i_69_, i_70_, is[i_70_] >> i_1_ & i_6_);
	    }
	}
    }
}
