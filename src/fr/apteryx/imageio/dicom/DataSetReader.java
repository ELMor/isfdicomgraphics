/*
 * DataSetReader - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;

import java.io.EOFException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.imageio.stream.ImageInputStream;

class DataSetReader extends DataSetReaderWriter {
    private final DicomWarningListener wl;

    private final ImageInputStream is;

    private final TransferSyntax TS;

    private final String enc;

    private final boolean raw;

    private final Stack dss;

    private int ctag;

    private final byte[] b;

    private static Hashtable readers;

    static interface ValueReader {
        public Object read(DataSetReader datasetreader, long l, int i)
                throws DicomException, IOException;
    }

    DataSetReader(ImageInputStream imageinputstream,
            DicomWarningListener dicomwarninglistener, boolean bool) {
        dss = new Stack();
        b = new byte[2];
        readers = new Hashtable();
        readers.put("AE", new ValueReader() {

            public Object read(DataSetReader datasetreader_1_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_1_.readString(l).trim());
            }
        });
        readers.put("AS", new ValueReader() {

            public Object read(DataSetReader datasetreader_3_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_3_.readString(l));
            }
        });
        readers.put("AT", new ValueReader() {

            public Object read(DataSetReader datasetreader_5_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x3L) != 0L)
                    datasetreader_5_.warning("ProtocoleViolation",
                            "AT VR value length");
                if (l == 4L)
                    return new Integer(datasetreader_5_.readTag());
                l /= 4L;
                Integer[] integers = new Integer[(int) l];
                for (int i_6_ = 0; (long) i_6_ < l; i_6_++)
                    integers[i_6_] = new Integer(datasetreader_5_.readTag());
                return integers;
            }
        });
        readers.put("CS", new ValueReader() {

            public Object read(DataSetReader datasetreader_8_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_8_.readString(l).trim());
            }
        });
        readers.put("DA", new ValueReader() {

            public Object read(DataSetReader datasetreader_10_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_10_.readString(l).trim(), "\\");
                int i_11_ = stringtokenizer.countTokens();
                if (i_11_ == 0)
                    return null;
                if (i_11_ == 1)
                    return datasetreader_10_.parseDA(stringtokenizer
                            .nextToken());
                Day[] days = new Day[i_11_];
                for (int i_12_ = 0; i_12_ < i_11_; i_12_++)
                    days[i_12_] = datasetreader_10_.parseDA(stringtokenizer
                            .nextToken());
                return days;
            }
        });
        readers.put("DS", new ValueReader() {

            public Object read(DataSetReader datasetreader_14_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_14_.readString(l).trim(), "\\");
                int i_15_ = stringtokenizer.countTokens();
                if (i_15_ == 0)
                    return null;
                if (i_15_ == 1)
                    return datasetreader_14_.parseDS(stringtokenizer
                            .nextToken());
                Float[] var_floats = new Float[i_15_];
                for (int i_16_ = 0; i_16_ < i_15_; i_16_++)
                    var_floats[i_16_] = datasetreader_14_
                            .parseDS(stringtokenizer.nextToken());
                return var_floats;
            }
        });
        readers.put("DT", new ValueReader() {

            public Object read(DataSetReader datasetreader_18_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_18_.readString(l).trim(), "\\");
                int i_19_ = stringtokenizer.countTokens();
                if (i_19_ == 0)
                    return null;
                if (i_19_ == 1)
                    return datasetreader_18_.parseDT(stringtokenizer
                            .nextToken());
                Date[] dates = new Date[i_19_];
                for (int i_20_ = 0; i_20_ < i_19_; i_20_++)
                    dates[i_20_] = datasetreader_18_.parseDT(stringtokenizer
                            .nextToken());
                return dates;
            }
        });
        readers.put("FL", new ValueReader() {

            public Object read(DataSetReader datasetreader_22_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x3L) != 0L) {
                    datasetreader_22_.warning("ProtocoleViolation",
                            "FL VR value length");
                    return null;
                }
                if (l == 4L)
                    return new Float(Float.intBitsToFloat(datasetreader_22_.is
                            .readInt()));
                l /= 4L;
                Float[] var_floats = new Float[(int) l];
                for (int i_23_ = 0; (long) i_23_ < l; i_23_++)
                    var_floats[i_23_] = new Float(Float
                            .intBitsToFloat(datasetreader_22_.is.readInt()));
                return var_floats;
            }
        });
        readers.put("FD", new ValueReader() {

            public Object read(DataSetReader datasetreader_25_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x7L) != 0L) {
                    datasetreader_25_.warning("ProtocoleViolation",
                            "FD VR value length");
                    return null;
                }
                if (l == 8L)
                    return new Double(Double
                            .longBitsToDouble(datasetreader_25_.is.readLong()));
                l /= 8L;
                Double[] var_doubles = new Double[(int) l];
                for (int i_26_ = 0; (long) i_26_ < l; i_26_++)
                    var_doubles[i_26_] = new Double(Double
                            .longBitsToDouble(datasetreader_25_.is.readLong()));
                return var_doubles;
            }
        });
        readers.put("IS", new ValueReader() {

            public Object read(DataSetReader datasetreader_28_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_28_.readString(l).trim(), "\\");
                int i_29_ = stringtokenizer.countTokens();
                if (i_29_ == 0)
                    return null;
                if (i_29_ == 1)
                    return datasetreader_28_.parseIS(stringtokenizer
                            .nextToken());
                Integer[] integers = new Integer[i_29_];
                for (int i_30_ = 0; i_30_ < i_29_; i_30_++)
                    integers[i_30_] = datasetreader_28_.parseIS(stringtokenizer
                            .nextToken());
                return integers;
            }
        });
        readers.put("LO", new ValueReader() {

            public Object read(DataSetReader datasetreader_32_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_32_.readString(l).trim());
            }
        });
        readers.put("LT", new ValueReader() {

            public Object read(DataSetReader datasetreader_34_, long l, int i)
                    throws IOException, DicomException {
                if (l > 10240L)
                    datasetreader_34_.warning("ProtocoleViolation",
                            "LT VR value length");
                return datasetreader_34_.readString(l).trim();
            }
        });
        readers.put("OB", new ValueReader() {

            public Object read(DataSetReader datasetreader_36_, long l, int i)
                    throws IOException, DicomException {
                if (i == 2145386512) {
                    PixelData pixeldata = new PixelData(datasetreader_36_.TS,
                            datasetreader_36_.getPos(), l, "OB");
                    if (l == 4294967295L) {
                        for (;;) {
                            int i_37_ = datasetreader_36_.readTag();
                            if (i_37_ == -73507)
                                break;
                            if (i_37_ == -73728) {
                                l = ((long) datasetreader_36_.is.readInt() & 0xffffffffL);
                                datasetreader_36_.skip(l);
                            } else
                                throw new DicomException(
                                        "ProtocoleViolation",
                                        (Tag.toString(i) + ": Expected Item or Sequence Delimitation Item"));
                        }
                        datasetreader_36_.skip(4L);
                    } else
                        datasetreader_36_.skip(l);
                    return pixeldata;
                }
                byte[] is = new byte[(int) l];
                datasetreader_36_.is.readFully(is);
                return is;
            }
        });
        readers.put("OF", new ValueReader() {

            public Object read(DataSetReader datasetreader_39_, long l, int i)
                    throws IOException, DicomException {
                float[] fs = new float[(int) l / 4];
                datasetreader_39_.is.readFully(fs, 0, fs.length);
                return fs;
            }
        });
        readers.put("OW", new ValueReader() {

            public Object read(DataSetReader datasetreader_41_, long l, int i)
                    throws IOException, DicomException {
                if (i == 2145386512) {
                    PixelData pixeldata = new PixelData(datasetreader_41_.TS,
                            datasetreader_41_.getPos(), l, "OW");
                    datasetreader_41_.skip(l);
                    return pixeldata;
                }
                short[] is = new short[(int) l / 2];
                datasetreader_41_.is.readFully(is, 0, is.length);
                return is;
            }
        });
        readers.put("PN", new ValueReader() {

            public Object read(DataSetReader datasetreader_43_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_43_.readString(l), "\\");
                int i_44_ = stringtokenizer.countTokens();
                if (i_44_ == 0)
                    return null;
                if (i_44_ == 1)
                    return datasetreader_43_.parsePN(stringtokenizer
                            .nextToken());
                PersonName[] personnames = new PersonName[i_44_];
                for (int i_45_ = 0; i_45_ < i_44_; i_45_++)
                    personnames[i_45_] = datasetreader_43_
                            .parsePN(stringtokenizer.nextToken());
                return personnames;
            }
        });
        readers.put("SH", new ValueReader() {

            public Object read(DataSetReader datasetreader_47_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_47_.readString(l).trim());
            }
        });
        readers.put("SL", new ValueReader() {

            public Object read(DataSetReader datasetreader_49_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x3L) != 0L) {
                    datasetreader_49_.warning("ProtocoleViolation",
                            "SL VR value length");
                    return null;
                }
                if (l == 4L)
                    return new Integer(datasetreader_49_.is.readInt());
                l /= 4L;
                Integer[] integers = new Integer[(int) l];
                for (int i_50_ = 0; (long) i_50_ < l; i_50_++)
                    integers[i_50_] = new Integer(datasetreader_49_.is
                            .readInt());
                return integers;
            }
        });
        readers.put("SQ", new ValueReader() {

            public Object read(DataSetReader datasetreader_52_, long l, int i)
                    throws IOException, DicomException {
                Vector vector = new Vector();
                long l_53_ = datasetreader_52_.getPos();
                while (datasetreader_52_.getPos() - l_53_ < l) {
                    DataElement dataelement = datasetreader_52_
                            .readDataElement();
                    if (dataelement.tag == -73507)
                        break;
                    if (dataelement.tag != -73728)
                        throw new DicomException("ProtocoleViolation", (Tag
                                .toString(i) + ": Expected Sequence Item"));
                    vector.add(dataelement.value);
                }
                return vector.size() > 0 ? vector : null;
            }
        });
        readers.put("SS", new ValueReader() {

            public Object read(DataSetReader datasetreader_55_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x1L) != 0L) {
                    datasetreader_55_.warning("ProtocoleViolation",
                            "SS VR value length");
                    return null;
                }
                if (l == 2L)
                    return new Short(datasetreader_55_.is.readShort());
                l /= 2L;
                Short[] var_shorts = new Short[(int) l];
                for (int i_56_ = 0; (long) i_56_ < l; i_56_++)
                    var_shorts[i_56_] = new Short(datasetreader_55_.is
                            .readShort());
                return var_shorts;
            }
        });
        readers.put("ST", new ValueReader() {

            public Object read(DataSetReader datasetreader_58_, long l, int i)
                    throws IOException, DicomException {
                if (l > 1024L)
                    datasetreader_58_.warning("ProtocoleViolation",
                            "ST VR value length");
                return datasetreader_58_.readString(l).trim();
            }
        });
        readers.put("TM", new ValueReader() {

            public Object read(DataSetReader datasetreader_60_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_60_.readString(l).trim(), "\\");
                int i_61_ = stringtokenizer.countTokens();
                if (i_61_ == 0)
                    return null;
                if (i_61_ == 1)
                    return datasetreader_60_.parseTM(stringtokenizer
                            .nextToken());
                Time[] times = new Time[i_61_];
                for (int i_62_ = 0; i_62_ < i_61_; i_62_++)
                    times[i_62_] = datasetreader_60_.parseTM(stringtokenizer
                            .nextToken());
                return times;
            }
        });
        readers.put("UI", new ValueReader() {

            public Object read(DataSetReader datasetreader_64_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_64_.readString(l).trim());
            }
        });
        readers.put("UL", new ValueReader() {

            public Object read(DataSetReader datasetreader_66_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x3L) != 0L) {
                    datasetreader_66_.warning("ProtocoleViolation",
                            "UL VR value length");
                    return null;
                }
                if (l == 4L)
                    return new Long(
                            (long) datasetreader_66_.is.readInt() & 0xffffffffL);
                l /= 4L;
                Long[] var_longs = new Long[(int) l];
                for (int i_67_ = 0; (long) i_67_ < l; i_67_++)
                    var_longs[i_67_] = new Long((long) datasetreader_66_.is
                            .readInt() & 0xffffffffL);
                return var_longs;
            }
        });
        readers.put("UN", new ValueReader() {

            public Object read(DataSetReader datasetreader_69_, long l, int i)
                    throws IOException, DicomException {
                byte[] is = new byte[(int) l];
                datasetreader_69_.read(is);
                return is;
            }
        });
        readers.put("US", new ValueReader() {

            public Object read(DataSetReader datasetreader_71_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x1L) != 0L) {
                    datasetreader_71_.warning("ProtocoleViolation",
                            "US VR value length");
                    return null;
                }
                if (l == 2L)
                    return new Integer(
                            datasetreader_71_.is.readShort() & 0xffff);
                l /= 2L;
                Integer[] integers = new Integer[(int) l];
                for (int i_72_ = 0; (long) i_72_ < l; i_72_++)
                    integers[i_72_] = new Integer(datasetreader_71_.is
                            .readShort() & 0xffff);
                return integers;
            }
        });
        readers.put("UT", new ValueReader() {

            public Object read(DataSetReader datasetreader_74_, long l, int i)
                    throws IOException, DicomException {
                byte[] is = new byte[(int) l];
                datasetreader_74_.read(is);
                if (is[(int) l - 1] == 32)
                    l--;
                return new String(is, 0, (int) l, datasetreader_74_.enc);
            }
        });
        is = imageinputstream;
        TS = TransferSyntax.IMPLICIT_LITTLEENDIAN;
        wl = dicomwarninglistener;
        enc = "ISO-8859-1";
        raw = bool;
    }

    DataSetReader(ImageInputStream imageinputstream,
            TransferSyntax transfersyntax,
            DicomWarningListener dicomwarninglistener, boolean bool)
            throws IOException {
        dss = new Stack();
        b = new byte[2];
        readers = new Hashtable();
        readers.put("AE", new ValueReader() {

            public Object read(DataSetReader datasetreader_76_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_76_.readString(l).trim());
            }
        });
        readers.put("AS", new ValueReader() {

            public Object read(DataSetReader datasetreader_78_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_78_.readString(l));
            }
        });
        readers.put("AT", new ValueReader() {

            public Object read(DataSetReader datasetreader_80_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x3L) != 0L)
                    datasetreader_80_.warning("ProtocoleViolation",
                            "AT VR value length");
                if (l == 4L)
                    return new Integer(datasetreader_80_.readTag());
                l /= 4L;
                Integer[] integers = new Integer[(int) l];
                for (int i_81_ = 0; (long) i_81_ < l; i_81_++)
                    integers[i_81_] = new Integer(datasetreader_80_.readTag());
                return integers;
            }
        });
        readers.put("CS", new ValueReader() {

            public Object read(DataSetReader datasetreader_83_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_83_.readString(l).trim());
            }
        });
        readers.put("DA", new ValueReader() {

            public Object read(DataSetReader datasetreader_85_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_85_.readString(l).trim(), "\\");
                int i_86_ = stringtokenizer.countTokens();
                if (i_86_ == 0)
                    return null;
                if (i_86_ == 1)
                    return datasetreader_85_.parseDA(stringtokenizer
                            .nextToken());
                Day[] days = new Day[i_86_];
                for (int i_87_ = 0; i_87_ < i_86_; i_87_++)
                    days[i_87_] = datasetreader_85_.parseDA(stringtokenizer
                            .nextToken());
                return days;
            }
        });
        readers.put("DS", new ValueReader() {

            public Object read(DataSetReader datasetreader_89_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_89_.readString(l).trim(), "\\");
                int i_90_ = stringtokenizer.countTokens();
                if (i_90_ == 0)
                    return null;
                if (i_90_ == 1)
                    return datasetreader_89_.parseDS(stringtokenizer
                            .nextToken());
                Float[] var_floats = new Float[i_90_];
                for (int i_91_ = 0; i_91_ < i_90_; i_91_++)
                    var_floats[i_91_] = datasetreader_89_
                            .parseDS(stringtokenizer.nextToken());
                return var_floats;
            }
        });
        readers.put("DT", new ValueReader() {

            public Object read(DataSetReader datasetreader_93_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_93_.readString(l).trim(), "\\");
                int i_94_ = stringtokenizer.countTokens();
                if (i_94_ == 0)
                    return null;
                if (i_94_ == 1)
                    return datasetreader_93_.parseDT(stringtokenizer
                            .nextToken());
                Date[] dates = new Date[i_94_];
                for (int i_95_ = 0; i_95_ < i_94_; i_95_++)
                    dates[i_95_] = datasetreader_93_.parseDT(stringtokenizer
                            .nextToken());
                return dates;
            }
        });
        readers.put("FL", new ValueReader() {

            public Object read(DataSetReader datasetreader_97_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x3L) != 0L) {
                    datasetreader_97_.warning("ProtocoleViolation",
                            "FL VR value length");
                    return null;
                }
                if (l == 4L)
                    return new Float(Float.intBitsToFloat(datasetreader_97_.is
                            .readInt()));
                l /= 4L;
                Float[] var_floats = new Float[(int) l];
                for (int i_98_ = 0; (long) i_98_ < l; i_98_++)
                    var_floats[i_98_] = new Float(Float
                            .intBitsToFloat(datasetreader_97_.is.readInt()));
                return var_floats;
            }
        });
        readers.put("FD", new ValueReader() {

            public Object read(DataSetReader datasetreader_100_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x7L) != 0L) {
                    datasetreader_100_.warning("ProtocoleViolation",
                            "FD VR value length");
                    return null;
                }
                if (l == 8L)
                    return new Double(Double
                            .longBitsToDouble(datasetreader_100_.is.readLong()));
                l /= 8L;
                Double[] var_doubles = new Double[(int) l];
                for (int i_101_ = 0; (long) i_101_ < l; i_101_++)
                    var_doubles[i_101_] = new Double(Double
                            .longBitsToDouble(datasetreader_100_.is.readLong()));
                return var_doubles;
            }
        });
        readers.put("IS", new ValueReader() {

            public Object read(DataSetReader datasetreader_103_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_103_.readString(l).trim(), "\\");
                int i_104_ = stringtokenizer.countTokens();
                if (i_104_ == 0)
                    return null;
                if (i_104_ == 1)
                    return datasetreader_103_.parseIS(stringtokenizer
                            .nextToken());
                Integer[] integers = new Integer[i_104_];
                for (int i_105_ = 0; i_105_ < i_104_; i_105_++)
                    integers[i_105_] = datasetreader_103_
                            .parseIS(stringtokenizer.nextToken());
                return integers;
            }
        });
        readers.put("LO", new ValueReader() {

            public Object read(DataSetReader datasetreader_107_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_107_.readString(l).trim());
            }
        });
        readers.put("LT", new ValueReader() {

            public Object read(DataSetReader datasetreader_109_, long l, int i)
                    throws IOException, DicomException {
                if (l > 10240L)
                    datasetreader_109_.warning("ProtocoleViolation",
                            "LT VR value length");
                return datasetreader_109_.readString(l).trim();
            }
        });
        readers.put("OB", new ValueReader() {

            public Object read(DataSetReader datasetreader_111_, long l, int i)
                    throws IOException, DicomException {
                if (i == 2145386512) {
                    PixelData pixeldata = new PixelData(datasetreader_111_.TS,
                            datasetreader_111_.getPos(), l, "OB");
                    if (l == 4294967295L) {
                        for (;;) {
                            int i_112_ = datasetreader_111_.readTag();
                            if (i_112_ == -73507)
                                break;
                            if (i_112_ == -73728) {
                                l = ((long) datasetreader_111_.is.readInt() & 0xffffffffL);
                                datasetreader_111_.skip(l);
                            } else
                                throw new DicomException(
                                        "ProtocoleViolation",
                                        (Tag.toString(i) + ": Expected Item or Sequence Delimitation Item"));
                        }
                        datasetreader_111_.skip(4L);
                    } else
                        datasetreader_111_.skip(l);
                    return pixeldata;
                }
                byte[] is = new byte[(int) l];
                datasetreader_111_.is.readFully(is);
                return is;
            }
        });
        readers.put("OF", new ValueReader() {

            public Object read(DataSetReader datasetreader_114_, long l, int i)
                    throws IOException, DicomException {
                float[] fs = new float[(int) l / 4];
                datasetreader_114_.is.readFully(fs, 0, fs.length);
                return fs;
            }
        });
        readers.put("OW", new ValueReader() {

            public Object read(DataSetReader datasetreader_116_, long l, int i)
                    throws IOException, DicomException {
                if (i == 2145386512) {
                    PixelData pixeldata = new PixelData(datasetreader_116_.TS,
                            datasetreader_116_.getPos(), l, "OW");
                    datasetreader_116_.skip(l);
                    return pixeldata;
                }
                short[] is = new short[(int) l / 2];
                datasetreader_116_.is.readFully(is, 0, is.length);
                return is;
            }
        });
        readers.put("PN", new ValueReader() {

            public Object read(DataSetReader datasetreader_118_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_118_.readString(l), "\\");
                int i_119_ = stringtokenizer.countTokens();
                if (i_119_ == 0)
                    return null;
                if (i_119_ == 1)
                    return datasetreader_118_.parsePN(stringtokenizer
                            .nextToken());
                PersonName[] personnames = new PersonName[i_119_];
                for (int i_120_ = 0; i_120_ < i_119_; i_120_++)
                    personnames[i_120_] = datasetreader_118_
                            .parsePN(stringtokenizer.nextToken());
                return personnames;
            }
        });
        readers.put("SH", new ValueReader() {

            public Object read(DataSetReader datasetreader_122_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_122_.readString(l).trim());
            }
        });
        readers.put("SL", new ValueReader() {

            public Object read(DataSetReader datasetreader_124_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x3L) != 0L) {
                    datasetreader_124_.warning("ProtocoleViolation",
                            "SL VR value length");
                    return null;
                }
                if (l == 4L)
                    return new Integer(datasetreader_124_.is.readInt());
                l /= 4L;
                Integer[] integers = new Integer[(int) l];
                for (int i_125_ = 0; (long) i_125_ < l; i_125_++)
                    integers[i_125_] = new Integer(datasetreader_124_.is
                            .readInt());
                return integers;
            }
        });
        readers.put("SQ", new ValueReader() {

            public Object read(DataSetReader datasetreader_127_, long l, int i)
                    throws IOException, DicomException {
                Vector vector = new Vector();
                long l_128_ = datasetreader_127_.getPos();
                while (datasetreader_127_.getPos() - l_128_ < l) {
                    DataElement dataelement = datasetreader_127_
                            .readDataElement();
                    if (dataelement.tag == -73507)
                        break;
                    if (dataelement.tag != -73728)
                        throw new DicomException("ProtocoleViolation", (Tag
                                .toString(i) + ": Expected Sequence Item"));
                    vector.add(dataelement.value);
                }
                return vector.size() > 0 ? vector : null;
            }
        });
        readers.put("SS", new ValueReader() {

            public Object read(DataSetReader datasetreader_130_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x1L) != 0L) {
                    datasetreader_130_.warning("ProtocoleViolation",
                            "SS VR value length");
                    return null;
                }
                if (l == 2L)
                    return new Short(datasetreader_130_.is.readShort());
                l /= 2L;
                Short[] var_shorts = new Short[(int) l];
                for (int i_131_ = 0; (long) i_131_ < l; i_131_++)
                    var_shorts[i_131_] = new Short(datasetreader_130_.is
                            .readShort());
                return var_shorts;
            }
        });
        readers.put("ST", new ValueReader() {

            public Object read(DataSetReader datasetreader_133_, long l, int i)
                    throws IOException, DicomException {
                if (l > 1024L)
                    datasetreader_133_.warning("ProtocoleViolation",
                            "ST VR value length");
                return datasetreader_133_.readString(l).trim();
            }
        });
        readers.put("TM", new ValueReader() {

            public Object read(DataSetReader datasetreader_135_, long l, int i)
                    throws IOException, DicomException {
                StringTokenizer stringtokenizer = new StringTokenizer(
                        datasetreader_135_.readString(l).trim(), "\\");
                int i_136_ = stringtokenizer.countTokens();
                if (i_136_ == 0)
                    return null;
                if (i_136_ == 1)
                    return datasetreader_135_.parseTM(stringtokenizer
                            .nextToken());
                Time[] times = new Time[i_136_];
                for (int i_137_ = 0; i_137_ < i_136_; i_137_++)
                    times[i_137_] = datasetreader_135_.parseTM(stringtokenizer
                            .nextToken());
                return times;
            }
        });
        readers.put("UI", new ValueReader() {

            public Object read(DataSetReader datasetreader_139_, long l, int i)
                    throws IOException, DicomException {
                return checkStringVM(datasetreader_139_.readString(l).trim());
            }
        });
        readers.put("UL", new ValueReader() {

            public Object read(DataSetReader datasetreader_141_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x3L) != 0L) {
                    datasetreader_141_.warning("ProtocoleViolation",
                            "UL VR value length");
                    return null;
                }
                if (l == 4L)
                    return new Long(
                            (long) datasetreader_141_.is.readInt() & 0xffffffffL);
                l /= 4L;
                Long[] var_longs = new Long[(int) l];
                for (int i_142_ = 0; (long) i_142_ < l; i_142_++)
                    var_longs[i_142_] = new Long((long) datasetreader_141_.is
                            .readInt() & 0xffffffffL);
                return var_longs;
            }
        });
        readers.put("UN", new ValueReader() {

            public Object read(DataSetReader datasetreader_144_, long l, int i)
                    throws IOException, DicomException {
                byte[] is = new byte[(int) l];
                datasetreader_144_.read(is);
                return is;
            }
        });
        readers.put("US", new ValueReader() {

            public Object read(DataSetReader datasetreader_146_, long l, int i)
                    throws IOException, DicomException {
                if ((l & 0x1L) != 0L) {
                    datasetreader_146_.warning("ProtocoleViolation",
                            "US VR value length");
                    return null;
                }
                if (l == 2L)
                    return new Integer(
                            datasetreader_146_.is.readShort() & 0xffff);
                l /= 2L;
                Integer[] integers = new Integer[(int) l];
                for (int i_147_ = 0; (long) i_147_ < l; i_147_++)
                    integers[i_147_] = new Integer(datasetreader_146_.is
                            .readShort() & 0xffff);
                return integers;
            }
        });
        readers.put("UT", new ValueReader() {

            public Object read(DataSetReader datasetreader_149_, long l, int i)
                    throws IOException, DicomException {
                byte[] is = new byte[(int) l];
                datasetreader_149_.read(is);
                if (is[(int) l - 1] == 32)
                    l--;
                return new String(is, 0, (int) l, datasetreader_149_.enc);
            }
        });
        is = (transfersyntax == TransferSyntax.DEFLATED_EXPLICIT_LITTLEENDIAN ? (ImageInputStream) new ZipImageInputStream(
                imageinputstream)
                : imageinputstream);
        TS = transfersyntax;
        wl = dicomwarninglistener;
        raw = bool;
        enc = "ISO-8859-1";
    }

    private int read() throws IOException {
        int i = is.read();
        if (i == -1)
            throw new EOFException();
        return i;
    }

    private int read(byte[] is) throws IOException {
        int i = this.is.read(is);
        if (i == -1)
            throw new EOFException();
        return i;
    }

    private Day parseDA(String s) throws DicomException {
        int i;
        i = s.length();
        if (i != 8 && i != 10) {
            warning("ProtocoleViolation", "DA VR value length");
            return null;
        }
        try {
            if (i == 8)
                return new Day(formats.dicom3DA.parse(s));
            return new Day(formats.compatDA.parse(s));
        } catch (ParseException e) {
            throw new DicomException("ProtocoleViolation", "DA VR value format");
        }
    }

    private Float parseDS(String string) throws DicomException {
        int i = string.length();
        if (i > 16)
            warning("ProtocoleViolation", "DS VR value length");
        Float var_float;
        try {
            var_float = new Float(string);
        } catch (NumberFormatException numberformatexception) {
            warning("ProtocoleViolation", "DS VR value format");
            return null;
        }
        return var_float;
    }

    private Date parseDT(String string) throws DicomException {
        int i = string.length();
        if (i > 26)
            warning("ProtocoleViolation", "DT VR value length");
        String string_153_;
        if (i == 26)
            string_153_ = string.substring(0, 18) + string.substring(21);
        else if (i == 21)
            string_153_ = string.substring(0, 18) + "+0000";
        else if (i == 18)
            string_153_ = string + "+0000";
        else if (i == 14)
            string_153_ = string + ".000+0000";
        else if (i == 12)
            string_153_ = string + "00.000+0000";
        else if (i == 10)
            string_153_ = string + "0000.000+0000";
        else if (i == 8)
            string_153_ = string + "000000.000+0000";
        else if (i == 6)
            string_153_ = string + "00000000.000+0000";
        else if (i == 4)
            string_153_ = string + "0000000000.000+0000";
        else {
            warning("ProtocoleViolation", "DT VR value format");
            return null;
        }
        Date date;
        try {
            date = formats.DTRead.parse(string_153_);
        } catch (ParseException parseexception) {
            warning("ProtocoleViolation", "DT VR value format");
            return null;
        }
        return date;
    }

    private Integer parseIS(String string) throws DicomException {
        int i = string.length();
        if (i > 12)
            warning("ProtocoleViolation", "IS VR value length");
        Integer integer;
        try {
            integer = new Integer(string);
        } catch (NumberFormatException numberformatexception) {
            warning("ProtocoleViolation", "IS VR value format");
            return null;
        }
        return integer;
    }

    private PersonName parsePN(String string) throws DicomException {
        int i = string.length();
        if (i > 194)
            warning("ProtocoleViolation", "PN VR value length");
        string = string.trim();
        PersonName personname = new PersonName();
        StringTokenizer stringtokenizer = new StringTokenizer(string, "=");
        while (stringtokenizer.hasMoreTokens()) {
            StringTokenizer stringtokenizer_154_ = new StringTokenizer(
                    stringtokenizer.nextToken(), "^");
            String[] strings = new String[5];
            for (int i_155_ = 0; i_155_ < 5
                    && stringtokenizer_154_.hasMoreTokens(); i_155_++)
                strings[i_155_] = stringtokenizer_154_.nextToken();
            if (stringtokenizer_154_.hasMoreTokens())
                warning("ProtocoleViolation", "Too many components in PN");
            personname.add(strings);
        }
        return personname;
    }

    private Time parseTM(String s) throws DicomException {
        int i = s.length();
        if (i > 16)
            warning("ProtocoleViolation", "TM VR value length");
        if (!(s.indexOf(':') >= 0)) {
            if (s.length() > 10)
                s = s.substring(0, 10);
            switch (s.length()) {
            case 9:
                s = s + "0";
                break;
            case 8:
                s = s + "00";
                break;
            case 7:
                s = s + "000";
                break;
            case 6:
                s = s + ".000";
                break;
            case 4:
                s = s + "00.000";
                break;
            case 2:
                s = s + "0000.000";
                break;
            }
            try {
                return new Time(formats.dicom3TM.parse(s));
            } catch (ParseException e) {
                throw new DicomException("ProtocoleViolation", "TM VR value format");
            }
        }
        if (s.length() > 12)
            s = s.substring(0, 12);
            switch (s.length()) {
            case 11: // '\013'
                s = s + "0";
                break;
            case 10: // '\n'
                s = s + "00";
                break;
            case 9: // '\t'
                s = s + "000";
                break;
            case 8: // '\b'
                s = s + ".000";
                break;
            case 6: // '\006'
                s = s + "00.000";
                break;
            case 5: // '\005'
                s = s + ":00.000";
                break;
            case 3: // '\003'
                s = s + "00:00.000";
                break;
            case 2: // '\002'
                s = s + ":00:00.000";
                break;
            }
        try {
            return new Time(formats.compatTM.parse(s));
        } catch (ParseException e) {
            throw new DicomException("ProtocoleViolation", "TM VR value format");
        }
    }

    private static Object checkStringVM(String string) {
        StringTokenizer stringtokenizer = new StringTokenizer(string, "\\");
        int i = stringtokenizer.countTokens();
        if (i == 0)
            return null;
        if (i == 1)
            return string;
        String[] strings = new String[i];
        for (int i_159_ = 0; i_159_ < i; i_159_++)
            strings[i_159_] = stringtokenizer.nextToken();
        return strings;
    }

    private String readString(long l) throws IOException, DicomException {
        if (l == 0L)
            return "";
        byte[] is = new byte[(int) l];
        read(is);
        if (is[(int) l - 1] == 0)
            l--;
        String string;
        try {
            string = new String(is, 0, (int) l, enc);
        } catch (UnsupportedEncodingException unsupportedencodingexception) {
            throw new DicomException("Unsupported",
                    (Tag.toString(ctag) + ": Character encoding"));
        }
        return string;
    }

    String readVR() throws IOException {
        read(b);
        return new String(b, enc);
    }

    private final int readTag() throws IOException {
        return Tag.create(is.readUnsignedShort(), is.readUnsignedShort());
    }

    private long skip(long l) throws IOException {
        long l_160_;
        long l_161_;
        for (l_161_ = 0L; l_161_ < l; l_161_ += l_160_) {
            l_160_ = is.skipBytes(l - l_161_);
            if (l_160_ == 0L)
                break;
        }
        return l_161_;
    }

    private Object readValue(int i, String string) throws IOException,
            DicomException {
        ctag = i;
        long l;
        if (TS.explicitVR) {
            if (string.equals("OB") || string.equals("OW")
                    || string.equals("OF") || string.equals("SQ")
                    || string.equals("UN")) {
                skip(2L);
                l = (long) this.is.readInt() & 0xffffffffL;
                if (l != 4294967295L && (l & 0x1L) == 1L)
                    warning("ProtocoleViolation",
                            "OB, OW, OF, SQ or UN value length is not even");
            } else if (string.equals("UT")) {
                l = (long) this.is.readInt() & 0xffffffffL;
                if ((l & 0x1L) == 1L)
                    warning("ProtocoleViolation", "UT value length is not even");
            } else {
                l = (long) this.is.readShort() & 0xffffL;
                if ((l & 0x1L) == 1L)
                    warning("ProtocoleViolation", "Value length is not even");
            }
        } else {
            l = (long) this.is.readInt() & 0xffffffffL;
            if (l != 4294967295L && (l & 0x1L) == 1L)
                warning("ProtocoleViolation", "Value length is not even");
        }
        if (raw && !"SQ".equals(string)) {
            if (i == 2145386512 && TS.encapsulated) {
                ArrayList arraylist = new ArrayList();
                for (;;) {
                    int i_162_ = readTag();
                    if (i_162_ == -73507)
                        break;
                    if (i_162_ == -73728) {
                        int i_163_ = this.is.readInt();
                        byte[] is = new byte[i_163_];
                        read(is);
                        arraylist.add(is);
                    } else
                        throw new DicomException(
                                "ProtocoleViolation",
                                (Tag.toString(i) + ": Expected Sequence Delimitation Item"));
                }
                skip(4L);
                byte[][] is = new byte[arraylist.size()][];
                arraylist.toArray(is);
                return is;
            }
            byte[] is = new byte[(int) l];
            read(is);
            return is;
        }
        if (string == null) {
            skip(l);
            return UnknownValue.instance;
        }
        if (string.equals("ox")) {
            if ((i & ~0xff0000) == 1342185484) {
                DataSet dataset = getCurrentDataSet();
                string = dataset != null
                        && (dataset.findInt(i & ~0xffff | 0x2002) == 0) ? "OW"
                        : "OB";
            } else if ((i & ~0xff0000) == 1342189568) {
                DataSet dataset = getCurrentDataSet();
                int i_164_ = (dataset == null ? 0 : dataset.findInt(i & ~0xffff
                        | 0x103));
                switch (i_164_) {
                case 1:
                    string = "SS";
                    break;
                case 2:
                    string = "FL";
                    break;
                case 3:
                    string = "FD";
                    break;
                case 4:
                    string = "SL";
                    break;
                default:
                    string = "US";
                }
            } else
                string = "OW";
        } else if (string.equals("xs")) {
            DataSet dataset = getCurrentDataSet();
            DataSet dataset_165_ = getParentDataSet(1);
            string = (dataset != null && dataset.findInt(2621699) == 0 || dataset_165_ != null
                    && (dataset_165_.findInt(2621699) == 0)) ? "US" : "SS";
        }
        ValueReader valuereader = (ValueReader) readers.get(string);
        if (valuereader == null)
            throw new DicomException("ProtocoleViolation", (Tag.toString(i)
                    + ": Unknown VR:" + string));
        return valuereader.read(this, l, i);
    }

    private DataElement readDataElement() throws IOException, DicomException {
        int i = readTag();
        if (i == -73507 || i == -73715) {
            skip(4L);
            return new DataElement(i, (Object) null);
        }
        if (i == -73728) {
            long l = getPos() - 4L;
            DataSet dataset = readDataSet((long) is.readInt() & 0xffffffffL);
            dataset.offset = l;
            return new DataElement(i, dataset);
        }
        Attribute attribute;
        String string;
        if (TS.explicitVR) {
            string = readVR();
            Attribute attribute_166_ = Dictionary.getAttribute(i);
            if (attribute_166_ != null && attribute_166_.VR.equals(string))
                attribute = attribute_166_;
            else {
                attribute = new Attribute();
                attribute.tag = i;
                attribute.VR = string;
                attribute.name = attribute_166_ == null ? null
                        : attribute_166_.name;
            }
        } else {
            attribute = Dictionary.getAttribute(i);
            if (attribute == null) {
                warning("Unsupported", "Unknown tag");
                string = null;
            } else
                string = attribute.VR;
        }
        return new DataElement(i, readValue(i, string), attribute);
    }

    DataSet readDataSet(long l) throws IOException, DicomException {
        DataSet dataset = new DataSet();
        dss.push(dataset);
        try {
            initByteOrder();
            long l_167_ = getPos();
            while (getPos() - l_167_ < l) {
                DataElement dataelement = readDataElement();
                if (dataelement.tag == -73715)
                    break;
                dataset.add(dataelement);
            }
        } catch (DicomException object) {
            dss.pop();
            throw object;
        }
        dss.pop();
        return dataset;
    }

    DataSet readDataSet() throws IOException, DicomException {
        DataSet dataset = new DataSet();
        dss.push(dataset);
        try {
            initByteOrder();
            try {
                for (;;) {
                    DataElement dataelement = readDataElement();
                    dataset.add(dataelement);
                }
            } catch (EOFException eofexception) {
                /* empty */
            }
        } catch (DicomException object) {
            dss.pop();
            throw object;
        }
        dss.pop();
        return dataset;
    }

    DataSet readGroup() throws IOException, DicomException {
        DataSet dataset = new DataSet();
        dss.push(dataset);
        try {
            initByteOrder();
            int i = -1;
            long l = 9223372036854775807L;
            for (;;) {
                long l_168_ = getPos();
                if (l_168_ >= l)
                    break;
                DataElement dataelement = readDataElement();
                int i_169_ = Tag.getGroupNumber(dataelement.tag);
                if (i == -1)
                    i = i_169_;
                else if (i != i_169_) {
                    is.seek(l_168_);
                    break;
                }
                if (Tag.getElementNumber(dataelement.tag) == 0) {
                    if (!(dataelement.value instanceof Long))
                        throw new DicomException(
                                "ProtocoleViolation",
                                (Tag.toString(dataelement.tag) + ": Group length VR is not UL"));
                    l = getPos() + ((Long) dataelement.value).longValue();
                }
                dataset.add(dataelement);
            }
        } catch (DicomException object) {
            dss.pop();
            throw object;
        }
        dss.pop();
        return dataset;
    }

    public String toString() {
        StringBuffer stringbuffer = new StringBuffer("STREAM ");
        stringbuffer.append(TS);
        stringbuffer.append(" enc=");
        stringbuffer.append(enc);
        if (raw)
            stringbuffer.append(" raw");
        return stringbuffer.toString();
    }

    private void initByteOrder() {
        is.setByteOrder(TS.bigEndian ? ByteOrder.BIG_ENDIAN
                : ByteOrder.LITTLE_ENDIAN);
    }

    private void warning(String string, String string_170_) {
        if (wl != null)
            wl.warning(string, Tag.toString(ctag) + ": " + string_170_);
    }

    private long getPos() throws IOException {
        return is.getStreamPosition();
    }

    private DataSet getCurrentDataSet() {
        return getParentDataSet(0);
    }

    private DataSet getParentDataSet(int i) {
        return dss.size() <= i ? null : (DataSet) dss.get(dss.size() - 1 - i);
    }
}
