/* ByteArrayImageInputStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import javax.imageio.stream.ImageInputStreamImpl;

final class ByteArrayImageInputStream extends ImageInputStreamImpl
{
    final byte[] buf;
    int pos;
    
    ByteArrayImageInputStream(byte[] is) {
	buf = is;
    }
    
    public int read(byte[] is, int i, int i_0_) {
	if (pos >= buf.length)
	    return -1;
	if (pos + i_0_ > buf.length)
	    i_0_ = buf.length - pos;
	System.arraycopy(buf, pos, is, i, i_0_);
	pos += i_0_;
	return i_0_;
    }
    
    public int read() {
	if (pos >= buf.length)
	    return -1;
	return buf[pos++] & 0xff;
    }
    
    public void seek(long l) {
	throw new Error("Unsupported seek");
    }
}
