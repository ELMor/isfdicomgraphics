/* PixelData - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package fr.apteryx.imageio.dicom;
import java.io.IOException;
import java.io.Serializable;

final class PixelData implements Serializable
{
    final long pos;
    final long l;
    final String VR;
    final TransferSyntax TS;
    
    PixelData
	(TransferSyntax transfersyntax, long l, long l_0_, String string)
	throws IOException {
	TS = transfersyntax;
	pos = l;
	this.l = l_0_;
	VR = string;
    }
    
    public String toString() {
	StringBuffer stringbuffer
	    = new StringBuffer("<Pixel data encoded in ");
	stringbuffer.append(TS.bigEndian ? "big endian " : "little endian ");
	stringbuffer.append(TS.encapsulated ? "encapsulated " : "native ");
	stringbuffer.append(VR.equals("OB") ? "byte " : "word ");
	stringbuffer.append("stream, offset ");
	stringbuffer.append(pos);
	stringbuffer.append(", length ");
	stringbuffer.append(l);
	stringbuffer.append('>');
	return new String(stringbuffer);
    }
}
